<!DOCTYPE html>

<html>

<head>
    <title>
       Socialseller Account - Profile
    </title>
  @include ('website/head')
  <style type="text/css">
    .profile-userpic img {
    max-width: 3rem;
    border-radius: 50%;
}
.abs-center-x {
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
}
.profiledd {
    width: 10rem;
    right: 0rem;
    left: auto;
    float: right;
    border: none;
    border-radius: 0.5rem;
    box-shadow: 0 5px 30px rgba(0,0,0,0.1);
    margin-top: 1rem;
    -webkit-animation-duration: 0.5s;
    animation-duration: 0.5s;
    padding: 0;
    color: #000;
    font-size: 0.875rem;
    z-index: 9999;
}
.authpagecontent {
    background-color: #f5f7fc !important;
    padding: 10rem 0 5rem;
}
.fancynav .nav-link.active {
    background-color: #0054d1;
    color: #fff !important;
}
.form-control {
    display: block;
    width: 100%;
    padding: 31px;
    margin-top: 5px;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #6c757d !important;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
   border-radius: 10px !important;
}
.accountprofile .profilesidebar img {
    margin: 0 auto 2rem;
    width: 70%;
    border-radius: 50%;
    border: solid 8px rgba(0, 84, 209, 0.1);
    display: block;
}
.accountprofile .profilesidebar {
    border-right: solid 1px rgba(0, 84, 209, 0.07);
}
.card-account {
    border: none !important;
    background-color: #fff;
    border-radius: 0.5rem;
    min-height: 80vh;
    box-shadow: 0 0px 20px rgba(36, 102, 178, 0.1);
    padding: 2rem;
}
.accountprofile .btn-success {
    background-color: #00e133 !important;
    border-color: #00e133 !important;
    border: 1px solid #ccc !important;
    margin-top: 1rem;
}
  </style>
    </head>

<body >
@include ('website.dashboard_header')
<div class="authpagecontent">
  <div class="container">
    <div class="row text-center">
     @include ('website.users_dashboard_tabs')

    </div>
    <div class="row">
        <div class="col-12">
    </div>
            <div class="card card-account">
                <div class="accountprofile">
                    <form action="{{ url('profile_update')}}" method="post" enctype="multipart/form-data">
                     <div class="row mb-5">

            <div class="col-12 col-md-3 profilesidebar">
                @if(isset($student->profile_image))
                <img id="mainContent_imgProfilePic" class="img-fluid" src="{{ $student->profile_image }}">
                @else
                <img id="mainContent_imgProfilePic" class="img-fluid" src="http://via.placeholder.com/600x600/d4dbec/ffffff?text=S">
                @endif
                <small class="d-none d-md-block font-weight-bold">Q. Does setting a password replace old password?</small>
                <small class="d-none d-md-block">Yes.</small>
                <small class="d-none d-md-block font-weight-bold mt-2">Q. Is country code required for Indian Mobile?</small>
                <small class="d-none d-md-block">No. Only for other than India.</small>
            </div>
            @csrf
            <div class="col-12 col-md-9 pl-md-5">
          @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
                <div class="form-group row">
                    <label class="col-12 col-md-3 col-form-label">Email ID:</label>
                    <div class="col-12 col-md-8">
                        <input name="email" disabled type="email" value="{{ $student->email }}" maxlength="100" id="mainContent_txtEmail" class="form-control" required="" placeholder="Ex: john@example.com">
                     @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-12 col-md-3 col-form-label mtm1">
                        Mobile:<br>
                        <!-- <small class="text-muted">With Country Code (Ex: +91)</small> -->
                    </label>
                    <div class="col-12 col-md-8">
                        <input name="mobile" type="text" disabled value="{{ $student->mobile }}" maxlength="20" id="mainContent_txtMobile" class="form-control" required="" placeholder="Ex: +91xxxxxxxxxx">
                        @error('mobile')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12 col-md-3 col-form-label">Full Name:</label>
                    <div class="col-12 col-md-8">
                        <input name="name" type="text" value="{{$student->name}}" maxlength="100" id="mainContent_txtFullName" class="form-control" required="" placeholder="Ex: John Doe">
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12 col-md-3 col-form-label">Gender:</label>
                    <div class="col-12 col-md-8">
                        <label class="radio">
                            Male
                            <input id="mainContent_rbmale" type="radio" name="gender" value="Male" @if($student->gender=='Male')  checked @endif class="color-1">
                            <span class="checkround"></span>
                        </label>
                        <label class="radio">
                            Female
                            <input id="mainContent_rbFemale" type="radio" name="gender" value="Female" @if($student->gender=='Female') checked @endif class="color-2">
                            <span class="checkround"></span>
                        </label>
                         @error('gender')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12 col-md-3 col-form-label">Update Photo:</label>
                    <div class="col-12 col-md-8">
                        <input type="file" name="image" id="mainContent_fileProfilePic" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-12 col-md-3 col-form-label mtm1">
                        Password<small>optional</small><br>
                        <small class="text-muted">(Type New password)</small></label>
                    <div class="col-12 col-md-8">

                        <input name="password" type="password" maxlength="100" id="mainContent_txtPassword"  class="form-control" placeholder="Set/Change Your Password here">
                     @error('password')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
                <hr>
                <input type="submit" name="ctl00$mainContent$btnUpdateProfile" value="Update Profile" id="mainContent_btnUpdateProfile" class=" btn-success btn-lg">
            </div>
        </div>
</form>
    </div>


                        </div>
                    </div>
                </div>
  </div>
</div>
@include ('website.footer')

<div class="bsnav-mobile">
         <div class="bsnav-mobile-overlay"></div>
         <div class="navbar">
             <div class="masterbarmobile"></div>
         </div>
        </div>


        @include ('website.footer-scripts')

   </body>
</html>
