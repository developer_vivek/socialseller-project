   <section  style="padding:4rem 0 4rem;">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col-md-12 text-center">
                  <h1 class="dark-blue-text">
                     Socialseller Helping people to Start their own<br/> Online Business
                  </h1>
                     <p>Resellers and Online seller Trust Us</p>
               </div>
            </div>
            <div class="row pt-5">
               <div class="col-md-2 col-6">
                  <div class="text-center  border-radius " >
                     <img src="{{ asset('socialseller/images/Reseller.png')}} "
                     class="" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500" style="width:50% !important;">
                     <br/>
                     <div><br/>
                        <h6><b>{{ $count->resellers }}</b></h6>
                        <h6 style="color:#0f54b2;">Reseller</h6>
                     </div>
                  </div>
               </div>
               <div class="col-md-2 col-6">
                  <div class="text-center  border-radius " >
                     <img src="{{ asset('socialseller/images/connect11.png')}} "
                     class="" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500" style="width:50% !important;">
                     <br/><br/>
                     <h6><b>{{ $count->connects }}</b></h6>
                     <h6 style="color:#0f54b2;">Connects</h6>
                     
                  </div>
               </div>
               <div class="col-md-2 col-6">
                  <div class="text-center  border-radius " >
                    <img src="{{ asset('socialseller/images/wholesaler.png')}} "
                     class="" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500" style="width:40% !important;">
                     <br/><br/>
                     <h6 ><b>{{ $count->suppliers }}</b></h6>
                        <h6  style="color:#0f54b2;">Suppliers</h6>
                  </div>
               </div>
               <div class="col-md-2 col-6">
                  <div class="text-center  border-radius" >
                    <img src="{{ asset('socialseller/images/Studentd.png')}} "
                     class="" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500" style="width:40% !important;">
                      <br/><br/>
                     <h6><b>{{ $count->students }}</b></h6>
                        <h6  style="color:#0f54b2;">Students</h6>
                     
                  </div>
               </div>
                  <div class="col-md-2 col-6">
                  <div class="text-center  border-radius " >
                     <img src="{{ asset('socialseller/images/Views.png')}} "
                     class="" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500" style="width:50% !important;">
                     <br/>
                     <div><br/>
                        <h6><b>{{ $count->views }}</b></h6>
                        <h6 style="color:#0f54b2;">Views</h6>
                     </div>
                  </div>
               </div>
               <div class="col-md-2 col-6">
                  <div class="text-center  border-radius " >
                     <img src="{{ asset('socialseller/images/Sunscriber.png')}} "
                     class="" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500" style="width:40% !important;">
                     <br/><br/>
                     <h6><b>{{ $count->subscriber  }}</b></h6>
                     <h6 style="color:#0f54b2;">Subscribers</h6>
                     
                  </div>
               </div>
            </div>

               
            <div class="row pt-5">
            
              
            </div>
         </div>
      </section>
