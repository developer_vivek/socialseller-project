<!DOCTYPE html>
<html>
<head><title>
	Socialseller Account - Signup
</title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600|Questrial" rel="stylesheet" />
    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.2/aos.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css" />
    <link href="{{ asset('socialseller/css/socialsellermain.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/fitodac/bsnav@master/dist/bsnav.min.css" />
</head>
<body class="loginpage">
  @include('website.partial.header');


    <form method="post" action="" onsubmit="" id="frmLogin">

        <div class="row no-gutters">
            <div class="col-12">
                <div class="loginform loginformlg">
                    <div class="row no-gutters" style="background: #fff;">
                        <div class="col-12 col-md-5 signupoffer">
                            <img src="{{ asset('socialseller/images/undraw_mobile_login_ikmv.svg')}}" class="img-fluid mx-auto d-block mb-4 mt-2" style="width:70%;" />
                            <h2 class="signupheading">Get Started</h2>
                            <p class="small text-center">Create your Socialseller Account and Get access to Our FREE Offerings.</p>
                            <p class="small pl-4">
                                <i class="fas fa-check-circle text-primary"></i>&nbsp; Get Free Online Courses <br />
                                <i class="fas fa-check-circle text-primary"></i>&nbsp; Paid Courses for Sellers <br />
                                <i class="fas fa-check-circle text-primary"></i>&nbsp; Online Suppliers Details<br />
                                <i class="fas fa-check-circle text-primary"></i>&nbsp; Personal Business Consultation<br />


                                <br />
                                <br/>
                            </p>
                            <p></p>
                        </div>
                        <div class="col-12 col-md-7">
                            <div class="signupform">
                                <div id="pnlSignup">

                                    <h3 class="mb-4">Signup with Socialseller</h3>
                                    <form action="{{ url('signup') }}" method="post" >
                                        @csrf
                                    <div class="form-group">
                                        <label class="text-left">Your Full Name:&nbsp;<small class="text-danger">*</small></label>
                                        <input name="full_name" type="text" id="txtFullName" class="form-control" value="{{old('full_name')}}" required="" placeholder="Enter your Full Name" />
                                    </div>
                                    <div class="form-group">
                                        <label class="text-left">Email ID:&nbsp;<small class="text-danger">*</small></label>
                                        <input name="email" maxlength="100" id="txtEmailS" class="form-control" value="{{old('email')}}" required="" type="email" placeholder="Enter your Email ID" />
                                                @error('email')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="text-left">Mobile Number:&nbsp;<small class="text-danger">*</small></label>
                                        <input name="mobile" maxlength="10" value="{{old('mobile')}}" id="txtMobileS" class="form-control" required="" type="tel" placeholder="Enter your Mobile Number" />
                                        <!-- <small class="loginhelptext"><i class="fas fa-exclamation-circle text-danger"></i>&nbsp;Country Code With + for outside India. (Ex: +61405802xxx)</small> -->
                                         @error('mobile')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                    </div>
                                     <div class="form-group">
                                        <label class="text-left">Password &nbsp;<small class="text-danger">*</small></label>
                                        <input name="password" maxlength="50" value="{{old('password')}}" class="form-control" required="" type="password" placeholder="Enter your Password" />
                                    </div>
                                    </form>
                                    <span id="rqmobilesignup" class="text-danger font-weight-bold" style="display:none;"></span>
                                    <span id="rqemailsignup" class="text-danger font-weight-bold" style="display:none;"></span>
                                    <span id="validateEmail" style="display:none;"></span>
                                    <div id="valsummsignup" style="display:none;">

	</div>
                                    <span id="validatePhone" style="display:none;"></span>

                                    <input type="submit" name="btnSignup" value="Signup &amp; Get Started"  id="btnSignup" class="btn btn-primary btncheck mt-4" />

</div>

                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="hfReturnURL" id="hfReturnURL" />
                </div>
            </div>
        </div>
        <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar">
            </div>
        </div>


</form>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.2/aos.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.min.js"></script>
    <script src="js/bsnav.min.js"></script>

    <script>
        AOS.init();
        $('.navbar-toggler').click(function () {
            $('.bsnav-sticky').toggleClass("bg-transparent");
        });

    </script>

</body>
</html>
