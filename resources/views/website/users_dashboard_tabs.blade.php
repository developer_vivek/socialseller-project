 <div class="col-md-12">
        <div class="mobileuinav">
                    <ul class="nav nav-tabs fancynav justify-content-center">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('dashboard')}}"><i data-feather="home"></i>Courses</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" href="{{ url('user_course_details/1') }}"><i data-feather="bar-chart">Courses</i></a>
                        </li> -->
                         <li class="nav-item">
                            <a class="nav-link" href="{{ url('suppliers') }}"><i data-feather="user"></i>Suppliers</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link active" href="{{ url('profile') }} "><i data-feather="user"></i>Profile</a>
                        </li>
                    </ul>
                </div>
      </div>
