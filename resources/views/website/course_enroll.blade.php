<!DOCTYPE html>
<html>
   <head>
      <title>Socialseller</title>
      @include('website.head')
<style>
   .im-checkout-btn.btn--default {
color: #fff;
background: #75c26a;
letter-spacing: 1px;
padding: 12px 8px;
opacity: 0.9;
border-radius: 4px;
margin-top:10px;
}
</style>
   </head>
   <body>

      @include('website.partial.header')
<div class="pt-5"></div>
<div class="pt-5"></div>
  <div class="container-fluid">
        <div class="row p-md-5">
            <div class="col-12">
                <div id="code_prod_list1">
                    <article class="card card-product">
                        <div class="card-body">
                            <div class="row">
                                <aside class="col-sm-3">
                                    <div class="img-wrap">
                                        <img id="mainContent_imgCourse" src="https://d3po6s2ufk88fh.cloudfront.net/525x350/courses/e7059adf2b4843d181a349e2b63a51fe.jpg">
                                    </div>
                                </aside>
                                <article class="col-sm-6">
                                    <span class="badge badge-primary">
                                        Finance
                                    </span>
                                    <div class="clearfix"></div>
                                    <h4 class="title">
                                        Academy of Value Investing
                                    </h4>
                                    <p>
                                        The authoritative course on stock picking based on the principles of fundamental research opted by legends of stock market.
                                    </p>
                                    <dl class="dlist-align">
                                        <dt>Modules</dt>
                                        <dd>
                                            13
                                        </dd>
                                    </dl>
                                    <dl class="dlist-align">
                                        <dt>Certification</dt>
                                        <dd>
                                            Yes
                                        </dd>
                                    </dl>

                                    <dl class="dlist-align">
                                        <dt>Assessment</dt>
                                        <dd>
                                            Yes
                                        </dd>
                                    </dl>
                                </article>
                                <aside class="col-sm-3 border-left">

                                    
                                    <div id="mainContent_pnlEnroll">
	
                                        <div class="action-wrap">
                                            <div class="price-wrap h4">
                                                <del class="price-old">
                                                    ₹ 11999.00
                                                </del>
                                                <span class="price">
                                                    ₹ 9999.00
                                                </span>
                                            </div>
                                            <h6 class="pt-2 pt-md-4">Pay to Enroll</h6><br/>
                                            <div class="im-checkout btn-7">
      <a href="https://www.instamojo.com/@FinologyStore/d1ea0a5e1e9649acb33a02c3b98943b5?embed=form" class="im-checkout-btn btn--default" target="_blank" rel="modal" style="null">Click here to Pay</a></div>
                                            <script src="https://d2xwmjc4uy2hr5.cloudfront.net/im-embed/im-embed.min.js"></script>
                                            
                                            <br>
                                        </div>
                                    
</div>
                                </aside>
                                <!-- col.// -->
                            </div>

                            <input type="hidden" name="ctl00$mainContent$hfOrderNum" id="mainContent_hfOrderNum" value="ENR02022021fd09f593-cd82-47f4-9f66-5d855deac12d">
                            <input type="hidden" name="ctl00$mainContent$hfCourseID" id="mainContent_hfCourseID" value="000c9592-f29c-4cf0-940a-3b9017c3a535">
                            <!-- row.// -->
                        </div>
                        <!-- card-body .// -->
                    </article>
                    <!-- product-list.// -->
                </div>
            </div>
        </div>
    </div>

   @include ('website.footer')
      <div class="bsnav-mobile">
         <div class="bsnav-mobile-overlay">
         <div class="navbar">
            <div class="masterbarmobile"></div>
         </div>
      </div>

      @include ('website.footer-scripts')
   </body>
</html>
