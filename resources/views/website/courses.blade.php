<!DOCTYPE html>

<html>

<head>
    <title>
       Lesson
    </title>
   @include('website.head')
  <style type="text/css">
    .profile-userpic img {
    max-width: 3rem;
    border-radius: 50%;
}
.abs-center-x {
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
}
.profiledd {
    width: 10rem;
    right: 0rem;
    left: auto;
    float: right;
    border: none;
    border-radius: 0.5rem;
    box-shadow: 0 5px 30px rgba(0,0,0,0.1);
    margin-top: 1rem;
    -webkit-animation-duration: 0.5s;
    animation-duration: 0.5s;
    padding: 0;
    color: #000;
    font-size: 0.875rem;
    z-index: 9999;
}
.authpagecontent {
    background-color: #f5f7fc !important;
    padding: 10rem 0 5rem;
}
.fancynav .nav-link.active {
    background-color: #0054d1;
    color: #fff !important;
}
.form-control {
    display: block;
    width: 100%;
    padding: 31px;
    margin-top: 5px;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #6c757d !important;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
   border-radius: 10px !important;
}
.accountprofile .profilesidebar img {
    margin: 0 auto 2rem;
    width: 70%;
    border-radius: 50%;
    border: solid 8px rgba(0, 84, 209, 0.1);
    display: block;
}
.accountprofile .profilesidebar {
    border-right: solid 1px rgba(0, 84, 209, 0.07);
}
.card-account {
    border: none !important;
    background-color: #fff;
    border-radius: 0.5rem;
    min-height: 80vh;
    box-shadow: 0 0px 20px rgba(36, 102, 178, 0.1);
    padding: 1rem;
}
.accountprofile .btn-success {
    background-color: #00e133 !important;
    border-color: #00e133 !important;
    border: 1px solid #ccc !important;
    margin-top: 1rem;
}
.owl-carousel .item {
    width: 100%;

}
.btns{
  display: table;
  margin: 30px auto;
}
.customNextBtn, .customPreviousBtn{
      float: right;
    background: #2d9070;
    color: #fff;
    padding: 10px;
    margin-left: 5px;
    cursor: pointer;
}
.owl-theme .owl-dots .owl-dot span {
    width: 10px;
    height: 10px;
    margin: 5px 7px;
    background: #d6d6d600;
    display: block;
    -webkit-backface-visibility: visible;
    transition: opacity .2s ease;
    border-radius: 30px;
}
.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
    background: #86979100;
}
.mySlides {display:none}

#vid-gallery ul.gallery-items li .gallery-item-desc {
	padding:10px;
}
#vid-gallery ul.gallery-items li .gallery-item-desc p{
    font-size: 16px;
    color: #000;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    line-height: 25px;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
}
#vid-gallery .gallery-title{
	width: 30%;
    float: right;
	color:#fff;
    padding: 10px 10px;
    font-weight: 400;
    font-size: 18px;
    margin-bottom: 10px;
    background:#111;
}
#vid-gallery ul.gallery-items li.active .gallery-item-desc p{
	color:#fff;
}
#vid-gallery ul.gallery-items li.active{
	background: #297acb;
}
#vid-gallery ul::-webkit-scrollbar-thumb {
        background-color: #dadada;
    height: 100px;
}
#vid-gallery ul::-webkit-scrollbar {
    width: 3px;
    background-color: #f1f1f1;
}
#vid-gallery ul.gallery-items li{
	    margin-bottom: 5px;
    border: 1px solid gainsboro;
    background: #f9f9f9;
    overflow: hidden;
}
#vid-gallery ul.gallery-items{
	list-style: none;
    width: 30%;
    float: right;
    padding: 0;
    padding-left: 10px;
    height: 377px;
    overflow-y: auto;
}
#vid-gallery .gallery-main iframe{
	width: 100%;
    height: 400px;
}
@media only screen and (max-width: 600px) {
  #vid-gallery .gallery-main iframe{
    width: 100%;
    height: 100%;
  }
}
#vid-gallery .gallery-main{
	width: 70%;
    float: left;
    height: 100%;
    background: #111;
    padding: 10px 10px;
}
@media only screen and (max-width: 991px) {
	#vid-gallery .gallery-main{
		width: 100%;
		float: none;
	}
	#vid-gallery .gallery-title {
		width: 100%;
		float: none;
		padding:10px 0px 0;
	}
  #vid-gallery ul.gallery-items{
		width: 100%;
		float: none;
		padding: 0;
		padding-left: 0px;
		height: 100%;
		overflow-y: auto;
	}
}

  </style>
    </head>

<body >
       @include('website.dashboard_header')
<div class="authpagecontent">
  <div class="container">
    <div class="row text-center">
      @include('website.users_dashboard_tabs')

    </div>

<div class="row">
<div class="col-12">
<div class="card card-account">
	<div>
		<!-- <h3><strong>The Only Stock Research Tool You'll Ever Need!</strong></h3> -->
		<h3><strong>{{ $course->title }}</strong></h3>
		<p class="mb-2">{!! $course->description  !!}</p>
	</div>
	<hr/>
	<div id="vid-gallery" class=" gallery ">
    <ul class="gallery-items">
      @foreach($lesson as $row)
			<li class="gallery-item">

        <a href="https://player.vimeo.com/video/{{ $row->url}}" img-link="{{ $row->image }}" class="gallery-item-link">{{ $row->title }} </a>
      </li>
      @endforeach
      <li><a href="{{ url('download_certificate').'/'.Request::segment(2)  }} ">Download Certificate</a></li>
			  <!-- <li class="gallery-item"><a href="https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0" class="gallery-item-link">When in full screen mode, you can scroll down to access comments and check what videos are up next. Scroll back up to hide everything but the video. </a></li>
			 <li class="gallery-item"><a href="https://www.youtube.com/watch?v=NDPRpuc-OT4" class="gallery-item-link">Clicks-and-mortar solutions without functional solutions</a></li>
			<li class="gallery-item"><a href="https://www.youtube.com/watch?v=nCtivKcLejo" class="gallery-item-link">Unleash cross-media information</a></li>
			<li class="gallery-item"><a href="https://www.youtube.com/watch?v=kJaMATuaWOs" class="gallery-item-link">Convergence without revolutionary ROI</a></li>
       <li class="gallery-item"><a href="https://www.youtube.com/watch?v=EjRulFDClqI" class="gallery-item-link">Convergence without revolutionary ROI</a></li> -->
		</ul>
	</div>
	<div class="mt-3">
		<!-- <h4><strong>About this course</strong></h4>
		<hr/> -->
		<!-- <p>Learn the Javascript essentials for web development or any type of programming. Learn all the basics of Javascript including primitive types, arrays, functions, assignment operators, the window object and much more.</p> -->
		<h5>What you’ll learn</h5>
		<!-- <ul class="checklist mt-0 pl-3"> -->
      {!! $course->what_you_learn !!}

		 <!-- <li class="text-black">Drafted By Lakshit Sethiya.</li>
		 <li class="text-black">Explained in Hindi + English</li>
		 <li class="text-black">24X7 Lifetime access </li> -->
	  <!-- </ul> -->
	</div>
</div>
<!--div class="card card-account">
    <div class="accountprofile">

        <div class="row">
            <div class="col-12 text-center">
                <div class="w3-content" style="width: 100%">
                @foreach($lesson as $key=>$row)
                 <div class="mySlides">
                     <h3>{{ $row->title }}</h3>
                     <iframe src="{{ $row->url }}" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="" width="100%" height="315"></iframe>
                        <h1>Lesson {{ ++$key }}</h1><hr>
                        <h3>{{ $row->title }}</h3>
                        <p>{{  $row->description    }}</p>
                  </div>
                @endforeach
                  < <div class="mySlides">
                     <iframe src="https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="" width="100%" height="315"></iframe>
                        <h1>Lession 1</h1><hr>
                        <h3>Take Professional Consultation</h3>
                        <p>Are you caught up in the dilemma of what you like and what you dislike as a career option? Are you having difficulty in understanding what you should or shouldn't do? Our experienced career counsellors will show you the way to your dreams. Get in touch with us and clear this dilemma.</p>
                  </div>
                  <div class="mySlides">
                     <iframe src="https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen=""  id="fitvid6"
                        width="100%" height="315"></iframe>
                      <h1>Lession 2</h1>
                      <hr>
                        <h3>Take Professional Consultation</h3>
                        <p>Are you caught up in the dilemma of what you like and what you dislike as a career option? Are you having difficulty in understanding what you should or shouldn't do? Our experienced career counsellors will show you the way to your dreams. Get in touch with us and clear this dilemma.</p>
                  </div>
                  <div class="mySlides">
                     <iframe src="https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen=""
                        width="100%" height="315"></iframe>
                        <h1>Lession 3</h1>
                        <hr>
                        <h3>Take Professional Consultation</h3>
                        <p>Are you caught up in the dilemma of what you like and what you dislike as a career option? Are you having difficulty in understanding what you should or shouldn't do? Our experienced career counsellors will show you the way to your dreams. Get in touch with us and clear this dilemma.</p>
                  </div>
                </div>

                <div class="w3-center">
                  <div class="w3-section">
                    <button class="w3-button w3-light-grey" onclick="plusDivs(-1)" style="background: #0054d1;
    color: #fff;    border: 1px solid #fff;      border-radius: 5px;
">❮ Prev</button>
                    <button class="w3-button w3-light-grey" onclick="plusDivs(1)" style="background: #0054d1;
    color: #fff;    border: 1px solid #fff;      border-radius: 5px;
">Next ❯</button>
                  </div>

                </div>

             </div>
            </div>
        </div>
    </div-->

</div>
</div>
</div>

</div>
@include('website.footer')
      <div class="bsnav-mobile">
         <div class="bsnav-mobile-overlay"></div>
         <div class="navbar">
            <div class="masterbarmobile"></div>
         </div>
      </div>

      @include('website.footer-scripts')
<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function currentDiv(n) {
  showDivs(slideIndex = n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace("w3-red", "");
  }
  x[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += "w3-red";
}
</script>


<script>

(function($) {

	var pluginName	= 'vidGallery',
		dataKey 	= 'plugin_' + pluginName,
		defaults	= {
			galleryMainClass:	'gallery-main',
			galleryItemsClass:	'gallery-items',
			galleryItemClass:	'gallery-item',
			galleryTitleText: 	'Course content',
			// Valid sizes: default (default), medium (mqdefault),
			// high (hqdefault), standard (sqdefault), max (maxresdefault)
			thumbSize: 			'default'
		};

	function Plugin (element, options) {

		this.element 	= element;
		this.$element 	= $(element);
		this.options 	= $.extend({}, defaults, options);

		this._defaults = defaults;
		this._name = pluginName;

		this.init(options);

	}

	Plugin.prototype = {

		init: function () {

			this._getVideos();
			this._getMainVid();
			this._getEvents();

			return this;

		},
		_getVideos: function () {

			var self			= this,
				thumbSize 		= self.options.thumbSize;

			videoList = [];

			switch ( thumbSize ) {

				case 'default':
					thumbSize = 'default.jpg';
					break;
				case 'mqdefault':
				case 'medium':
					thumbSize = 'mqdefault.jpg';
					break;
				case 'hqdefault':
				case 'high':
					thumbSize = 'hqdefault.jpg';
					break;
				case 'sddefault':
				case 'standard':
					thumbSize = 'sddefault.jpg';
					break;
				case 'maxresdefault':
				case 'max':
					thumbSize = 'maxresdefault.jpg';
					break;
				default:
					throw new Error( '`' + self.options.thumbSize + '`' + ' is not a valid thumbnail size. Valid sizes: default (default), medium (mqdefault), high (hqdefault), standard (sqdefault), max (maxresdefault)');

			}
      var imgLink;

		 	$('.' + self.options.galleryItemClass).each(function (index_) {

		 		var $vidLink = $(this).find('a'),
					listItem = [];

			 	videoList.push({
					reference: this,
					videoId: $vidLink.attr('href'),
					vidDesc: $vidLink.text(),
          imgLink:$vidLink.attr('img-link')
				});
console.log(videoList[index_].videoId);
				$vidLink.html('');

				listItem 	+= '<div class=\"media media-left\">';
				listItem 	+= 		'<div class=\"media-img gallery-item-thumb\">';
				listItem 	+=			'<img src="'+videoList[index_].imgLink+'" class="img-thumbnail" style="width:150px;">';
				listItem	+= 		'</div>';
				listItem	+= 		'<div class=\"media-body gallery-item-desc\"> <p>';
				listItem	+= 			videoList[index_].vidDesc;
				listItem	+= 		'</p></div>';
				listItem	+= 	'</div>';

			 	console.log(listItem);

				$vidLink.append(listItem);

			});

		},
		_getMainVid: function () {

			var self 		= this,
				mainVid		= [];

				mainVid 	+= '<div class=\"' + self.options.galleryMainClass + '\">';
				mainVid 	+= 		'<div class=\"flex-media\">';
				mainVid 	+= 			'<iframe src=\"' + videoList[0].videoId + '' + '\" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen seamless>';
				mainVid		+= 		'</div>';
				mainVid 	+= '</div>';

				$('.gallery').prepend(mainVid);
				$('<div class=\"gallery-title\">' + self.options.galleryTitleText + '</div>').insertBefore('.gallery-items');
				$('.' + self.options.galleryItemClass).eq(0).addClass('active');

		},
		_getEvents: function () {

			var self = this;

			$('.' + self.options.galleryItemClass).on('click', function (e) {

				e.preventDefault();

				var $iframe			= $('.' + self.options.galleryMainClass).find('iframe'),
					currentIndex	= $(this).index(),
					newSrc 			=  videoList[currentIndex].videoId + '?autoplay=1&rel=0';


				if ( !$(this).hasClass('active') ) {

					$(this).siblings().removeClass('active');
					$(this).addClass('active');
					$iframe.attr('src', newSrc);

				}

			});

		}
	};

	$.fn[pluginName] = function (options) {

		var plugin = this.data(dataKey);

		if ( plugin instanceof Plugin ) {
			if (typeof options !== 'undefined') {
				plugin.init(options);
			}
		} else{
			plugin = new Plugin(this, options);
			this.data(dataKey, plugin);
		}

		return plugin;

	};

})(jQuery);


$(function() {

	$('.gallery').vidGallery();


});



</script>

   </body>
</html>
