<!DOCTYPE html>

<html>

<head>
    <title>
       Socialseller Account - Dashboard
    </title>
 @include ('website/head')
  <style type="text/css">
    .profile-userpic img {
    max-width: 3rem;
    border-radius: 50%;
}
.abs-center-x {
    position: absolute;
    left: 50%;
    transform: translateX(-50%);
}
.profiledd {
    width: 10rem;
    right: 0rem;
    left: auto;
    float: right;
    border: none;
    border-radius: 0.5rem;
    box-shadow: 0 5px 30px rgba(0,0,0,0.1);
    margin-top: 1rem;
    -webkit-animation-duration: 0.5s;
    animation-duration: 0.5s;
    padding: 0;
    color: #000;
    font-size: 0.875rem;
    z-index: 9999;
}
.authpagecontent {
    background-color: #f5f7fc !important;
    padding: 10rem 0 5rem;
}
.fancynav .nav-link.active {
    background-color: #0054d1;
    color: #fff !important;
}
.form-control {
    display: block;
    width: 100%;
    padding: 31px;
    margin-top: 5px;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #6c757d !important;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
   border-radius: 10px !important;
}
.accountprofile .profilesidebar img {
    margin: 0 auto 2rem;
    width: 70%;
    border-radius: 50%;
    border: solid 8px rgba(0, 84, 209, 0.1);
    display: block;
}
.accountprofile .profilesidebar {
    border-right: solid 1px rgba(0, 84, 209, 0.07);
}
.card-account {
    border: none !important;
    background-color: #fff;
    border-radius: 0.5rem;
    min-height: 80vh;
    box-shadow: 0 0px 20px rgba(36, 102, 178, 0.1);
    padding: 2rem;
}
.accountprofile .btn-success {
    background-color: #00e133 !important;
    border-color: #00e133 !important;
    border: 1px solid #ccc !important;
    margin-top: 1rem;
}
.card11{
        border: 1px solid;
    border-radius: 2px;
    height: 150px;
    padding: 5px;
    margin-top: 10px;
    box-shadow: 0px 0px 5px 5px #cccccc47;
}
.courseprice{
    background-color: #fff653;
    display: inline-block;
    color: #000 !important;
    font-weight: 500;
    font-size: 1rem;
    margin-top: 0.5rem;
    float: left;
    padding:10px 10px;
}

  </style>
    </head>

<body>
@include('website.dashboard_header');
<div class="authpagecontent">
  <div class="container">
    <div class="row text-center">

      <div class="col-md-12">
        <div class="mobileuinav">
                    <ul class="nav nav-tabs fancynav justify-content-center">
                        <li class="nav-item active">
                            <a class="nav-link active" href="{{url('dashboard')}}"><i data-feather="home"></i>Courses</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" href="{{ url('user_course_details/1') }}"><i data-feather="bar-chart">Courses</i></a>
                        </li> -->
                         <li class="nav-item">
                            <a class="nav-link" href="{{ url('suppliers') }}"><i data-feather="user"></i>Suppliers</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('profile') }} "><i data-feather="user"></i>Profile</a>
                        </li>
                    </ul>
                </div>
      </div>
    </div>
<div class="card card-account">
	<div class="row">
		<div class="col-12">
			<h4><strong>My Courses</strong></h4>
		<hr/>
         @if(count($courses)==0)
			<p style="color:red;float:left">You have not enrolled to any Course yet. </p>
         @endif
      </div>
      @foreach($courses as $row)
		<div class="col-12 col-md-3">
			<div class="coursecard aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">
				<div class="courseimg">
					<img class="img-fluid" src="{{ $row->image }}" />
					<span class="badge badge-warning">{{ $row->category_name }}</span>
				</div>
				<div class="p-2 coursedetails">
					<span class="coursename">{{ $row->title }}</span>
					<div class="clearfix"></div>
					<div class="coursehighlights">
						<img src="{{ asset('socialseller/images/Icon_5.png') }}" style="width: 20px;" />
						<p>{{ $row->total_lesson }} Lessons</p>
					</div>
					<div class="coursehighlights">
						<!-- <img src="http://gyankhajana.com/socialseller/images/Icon_6.png" style="width: 20px;"> -->
						<svg class="svg-inline--fa fa-certificate fa-w-16" style="" aria-hidden="true" data-prefix="fas" data-icon="certificate" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
							<path
								fill="currentColor"
								d="M458.622 255.92l45.985-45.005c13.708-12.977 7.316-36.039-10.664-40.339l-62.65-15.99 17.661-62.015c4.991-17.838-11.829-34.663-29.661-29.671l-61.994 17.667-15.984-62.671C337.085.197 313.765-6.276 300.99 7.228L256 53.57 211.011 7.229c-12.63-13.351-36.047-7.234-40.325 10.668l-15.984 62.671-61.995-17.667C74.87 57.907 58.056 74.738 63.046 92.572l17.661 62.015-62.65 15.99C.069 174.878-6.31 197.944 7.392 210.915l45.985 45.005-45.985 45.004c-13.708 12.977-7.316 36.039 10.664 40.339l62.65 15.99-17.661 62.015c-4.991 17.838 11.829 34.663 29.661 29.671l61.994-17.667 15.984 62.671c4.439 18.575 27.696 24.018 40.325 10.668L256 458.61l44.989 46.001c12.5 13.488 35.987 7.486 40.325-10.668l15.984-62.671 61.994 17.667c17.836 4.994 34.651-11.837 29.661-29.671l-17.661-62.015 62.65-15.99c17.987-4.302 24.366-27.367 10.664-40.339l-45.984-45.004z"
							></path>
						</svg>
						<!-- <i class="fas fa-certificate" style=""></i> -->
						<p>Certificate</p>
					</div>
					<div class="coursehighlights">
						<img src="{{ asset('socialseller/images/Icon_7.png') }}" style="width: 20px;" />
						<p>{{ $row->total_hours }} Hours</p>
					</div>
					<div class="clearfix"></div>
					<div class="progress">
					  <div class="progress-bar" role="progressbar" aria-valuenow="70"
					  aria-valuemin="0" aria-valuemax="100" style="width:70%">
						<span class="sr-only">70% Complete</span>
					  </div>
					</div>
				</div>
				<a href="{{ url('user_course_details/').'/'.$row->id  }}" class="enrollbtn enrollbtn_inner">
					Enrolled
				</a>
			</div>
      </div>
      @endforeach
		<!-- <div class="col-12 col-md-3">
			<div class="coursecard aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">
				<div class="courseimg">
					<img class="img-fluid" src="http://gyankhajana.com/socialseller/images/c1.jpg" />
					<span class="badge badge-warning">Business</span>
				</div>
				<div class="p-2 coursedetails">
					<span class="coursename">Reselling bussiness mastery course</span>
					<div class="clearfix"></div>
					<div class="coursehighlights">
						<img src="http://gyankhajana.com/socialseller/images/Icon_5.png" style="width: 20px;" />
						<p>0 Lessons</p>
					</div>
					<div class="coursehighlights">

						<svg class="svg-inline--fa fa-certificate fa-w-16" style="" aria-hidden="true" data-prefix="fas" data-icon="certificate" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
							<path
								fill="currentColor"
								d="M458.622 255.92l45.985-45.005c13.708-12.977 7.316-36.039-10.664-40.339l-62.65-15.99 17.661-62.015c4.991-17.838-11.829-34.663-29.661-29.671l-61.994 17.667-15.984-62.671C337.085.197 313.765-6.276 300.99 7.228L256 53.57 211.011 7.229c-12.63-13.351-36.047-7.234-40.325 10.668l-15.984 62.671-61.995-17.667C74.87 57.907 58.056 74.738 63.046 92.572l17.661 62.015-62.65 15.99C.069 174.878-6.31 197.944 7.392 210.915l45.985 45.005-45.985 45.004c-13.708 12.977-7.316 36.039 10.664 40.339l62.65 15.99-17.661 62.015c-4.991 17.838 11.829 34.663 29.661 29.671l61.994-17.667 15.984 62.671c4.439 18.575 27.696 24.018 40.325 10.668L256 458.61l44.989 46.001c12.5 13.488 35.987 7.486 40.325-10.668l15.984-62.671 61.994 17.667c17.836 4.994 34.651-11.837 29.661-29.671l-17.661-62.015 62.65-15.99c17.987-4.302 24.366-27.367 10.664-40.339l-45.984-45.004z"
							></path>
						</svg>

						<p>Certificate</p>
					</div>
					<div class="coursehighlights">
						<img src="http://gyankhajana.com/socialseller/images/Icon_7.png" style="width: 20px;" />
						<p>Hours</p>
					</div>
					<div class="clearfix"></div>
					<div class="progress">
					  <div class="progress-bar" role="progressbar" aria-valuenow="70"
					  aria-valuemin="0" aria-valuemax="100" style="width:70%">
						<span class="sr-only">70% Complete</span>
					  </div>
					</div>
				</div>
				<a href="http://gyankhajana.com/course_details/1" class="enrollbtn enrollbtn_inner">
					Enrolled
				</a>
			</div>
		</div>
		<div class="col-12 col-md-3">
			<div class="coursecard aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">
				<div class="courseimg">
					<img class="img-fluid" src="http://gyankhajana.com/socialseller/images/c1.jpg" />
					<span class="badge badge-warning">Business</span>
				</div>
				<div class="p-2 coursedetails">
					<span class="coursename">Reselling bussiness mastery course</span>
					<div class="clearfix"></div>
					<div class="coursehighlights">
						<img src="http://gyankhajana.com/socialseller/images/Icon_5.png" style="width: 20px;" />
						<p>0 Lessons</p>
					</div>
					<div class="coursehighlights">

						<svg class="svg-inline--fa fa-certificate fa-w-16" style="" aria-hidden="true" data-prefix="fas" data-icon="certificate" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
							<path
								fill="currentColor"
								d="M458.622 255.92l45.985-45.005c13.708-12.977 7.316-36.039-10.664-40.339l-62.65-15.99 17.661-62.015c4.991-17.838-11.829-34.663-29.661-29.671l-61.994 17.667-15.984-62.671C337.085.197 313.765-6.276 300.99 7.228L256 53.57 211.011 7.229c-12.63-13.351-36.047-7.234-40.325 10.668l-15.984 62.671-61.995-17.667C74.87 57.907 58.056 74.738 63.046 92.572l17.661 62.015-62.65 15.99C.069 174.878-6.31 197.944 7.392 210.915l45.985 45.005-45.985 45.004c-13.708 12.977-7.316 36.039 10.664 40.339l62.65 15.99-17.661 62.015c-4.991 17.838 11.829 34.663 29.661 29.671l61.994-17.667 15.984 62.671c4.439 18.575 27.696 24.018 40.325 10.668L256 458.61l44.989 46.001c12.5 13.488 35.987 7.486 40.325-10.668l15.984-62.671 61.994 17.667c17.836 4.994 34.651-11.837 29.661-29.671l-17.661-62.015 62.65-15.99c17.987-4.302 24.366-27.367 10.664-40.339l-45.984-45.004z"
							></path>
						</svg>

						<p>Certificate</p>
					</div>
					<div class="coursehighlights">
						<img src="http://gyankhajana.com/socialseller/images/Icon_7.png" style="width: 20px;" />
						<p>Hours</p>
					</div>
					<div class="clearfix"></div>
					<div class="progress">
					  <div class="progress-bar" role="progressbar" aria-valuenow="70"
					  aria-valuemin="0" aria-valuemax="100" style="width:70%">
						<span class="sr-only">70% Complete</span>
					  </div>
					</div>
				</div>
				<a href="http://gyankhajana.com/course_details/1" class="enrollbtn enrollbtn_inner">
					Enrolled
				</a>
			</div>
		</div>
		<div class="col-12 col-md-3">
			<div class="coursecard aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">
				<div class="courseimg">
					<img class="img-fluid" src="http://gyankhajana.com/socialseller/images/c1.jpg" />
					<span class="badge badge-warning">Business</span>
				</div>
				<div class="p-2 coursedetails">
					<span class="coursename">Reselling bussiness mastery course</span>
					<div class="clearfix"></div>
					<div class="coursehighlights">
						<img src="http://gyankhajana.com/socialseller/images/Icon_5.png" style="width: 20px;" />
						<p>0 Lessons</p>
					</div>
					<div class="coursehighlights">

						<svg class="svg-inline--fa fa-certificate fa-w-16" style="" aria-hidden="true" data-prefix="fas" data-icon="certificate" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
							<path
								fill="currentColor"
								d="M458.622 255.92l45.985-45.005c13.708-12.977 7.316-36.039-10.664-40.339l-62.65-15.99 17.661-62.015c4.991-17.838-11.829-34.663-29.661-29.671l-61.994 17.667-15.984-62.671C337.085.197 313.765-6.276 300.99 7.228L256 53.57 211.011 7.229c-12.63-13.351-36.047-7.234-40.325 10.668l-15.984 62.671-61.995-17.667C74.87 57.907 58.056 74.738 63.046 92.572l17.661 62.015-62.65 15.99C.069 174.878-6.31 197.944 7.392 210.915l45.985 45.005-45.985 45.004c-13.708 12.977-7.316 36.039 10.664 40.339l62.65 15.99-17.661 62.015c-4.991 17.838 11.829 34.663 29.661 29.671l61.994-17.667 15.984 62.671c4.439 18.575 27.696 24.018 40.325 10.668L256 458.61l44.989 46.001c12.5 13.488 35.987 7.486 40.325-10.668l15.984-62.671 61.994 17.667c17.836 4.994 34.651-11.837 29.661-29.671l-17.661-62.015 62.65-15.99c17.987-4.302 24.366-27.367 10.664-40.339l-45.984-45.004z"
							></path>
						</svg>

						<p>Certificate</p>
					</div>
					<div class="coursehighlights">
						<img src="http://gyankhajana.com/socialseller/images/Icon_7.png" style="width: 20px;" />
						<p>Hours</p>
					</div>
					<div class="clearfix"></div>
					<div class="progress">
					  <div class="progress-bar" role="progressbar" aria-valuenow="70"
					  aria-valuemin="0" aria-valuemax="100" style="width:70%">
						<span class="sr-only">70% Complete</span>
					  </div>
					</div>
				</div>
				<a href="http://gyankhajana.com/course_details/1" class="enrollbtn enrollbtn_inner">
					Enrolled
				</a>
			</div>
		</div>

	</div> -->
   <!-- <section class="courses ">
         <div class="container-fluid">
            <div class="row no-gutters justify-content-center">
               <div class="col-12 col-md-3" data-aos="fade-up" data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg">
                        <img class="img-fluid" src='../images/c1.jpg' />
                        <span class="badge badge-warning"> Mastery</span>
                     </div>
                     <div class="p-2 coursedetails">
                        <span class="coursename">रीसेलिंग बिज़नेस मास्टरी कोर्स</span>
                        <div class="clearfix"></div>
                        <div class="coursehighlights">
                           <img src="../images/icon_5.png" style="width: 20px;">
                           <p>11 Lessons</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="images/icon_6.png" style="width: 20px;">
                           <p>Certificate</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="../images/icon_7.png" style="width: 20px;">
                           <p> Hours</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <a href='courses_details.php' class="enrollbtn"><i class="fas fa-user-plus"></i>Enroll Now</a>
                  </div>
               </div>
               <div class="col-12 col-md-3" data-aos="fade-up" data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg">
                        <img class="img-fluid" src='../images/c1.jpg' />
                        <span class="badge badge-warning"> Mastery</span>
                     </div>
                     <div class="p-2 coursedetails">
                        <span class="coursename">रीसेलिंग बिज़नेस मास्टरी कोर्स</span>
                        <div class="clearfix"></div>
                        <div class="coursehighlights">
                           <img src="../images/icon_5.png" style="width: 20px;">
                           <p>11 Lessons</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="../images/icon_6.png" style="width: 20px;">
                           <p>Certificate</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="../images/icon_7.png" style="width: 20px;">
                           <p> Hours</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <a href='courses_details.php' class="enrollbtn"><i class="fas fa-user-plus"></i>Enroll Now</a>
                  </div>
               </div>
               <div class="col-12 col-md-3" data-aos="fade-up" data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg">
                        <img class="img-fluid" src='../images/c1.jpg' />
                        <span class="badge badge-warning"> Mastery</span>
                     </div>
                     <div class="p-2 coursedetails">
                        <span class="coursename">रीसेलिंग बिज़नेस मास्टरी कोर्स</span>
                        <div class="clearfix"></div>
                        <div class="coursehighlights">
                           <img src="../images/icon_5.png" style="width: 20px;">
                           <p>11 Lessons</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="../images/icon_6.png" style="width: 20px;">
                           <p>Certificate</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="../images/icon_7.png" style="width: 20px;">
                           <p> Hours</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <a href='courses_details.php' class="enrollbtn"><i class="fas fa-user-plus"></i>Enroll Now</a>
                  </div>
               </div>
               <div class="col-12 col-md-3" data-aos="fade-up" data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg">
                        <img class="img-fluid" src='../images/c1.jpg' />
                        <span class="badge badge-warning"> Mastery</span>
                     </div>
                     <div class="p-2 coursedetails">
                        <span class="coursename">रीसेलिंग बिज़नेस मास्टरी कोर्स</span>
                        <div class="clearfix"></div>
                        <div class="coursehighlights">
                           <img src="../images/icon_5.png" style="width: 20px;">
                           <p>11 Lessons</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="../images/icon_6.png" style="width: 20px;">
                           <p>Certificate</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="../images/icon_7.png" style="width: 20px;">
                           <p> Hours</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <a href='courses_details.php' class="enrollbtn"><i class="fas fa-user-plus"></i>Enroll Now</a>
                  </div>
               </div>
            </div>
         </div>
      </section> -->
    <div class="row">


<!-- <div class="col-md-6">
       <div class="card11">
                    <div class="row">
            <div class="col-md-4">
                <div class="courseheader">
                    <img src="images/c1.jpg" class="img-fluid">
                </div>
            </div>
        <div class="col-md-8 pt-2">
             <h6><i> RESELLING BUSINESS MASTERY COURSE</i></h6>
             <span class="coursefeatures">
                            <svg class="svg-inline--fa fa-book-open fa-w-18" aria-hidden="true" data-prefix="fas" data-icon="book-open" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M542.22 32.05c-54.8 3.11-163.72 14.43-230.96 55.59-4.64 2.84-7.27 7.89-7.27 13.17v363.87c0 11.55 12.63 18.85 23.28 13.49 69.18-34.82 169.23-44.32 218.7-46.92 16.89-.89 30.02-14.43 30.02-30.66V62.75c.01-17.71-15.35-31.74-33.77-30.7zM264.73 87.64C197.5 46.48 88.58 35.17 33.78 32.05 15.36 31.01 0 45.04 0 62.75V400.6c0 16.24 13.13 29.78 30.02 30.66 49.49 2.6 149.59 12.11 218.77 46.95 10.62 5.35 23.21-1.94 23.21-13.46V100.63c0-5.29-2.62-10.14-7.27-12.99z"></path></svg><!-- <i class="fas fa-book-open"></i> --
                            Modules
                        </span><br/>
             <span class="courseprice">₹&nbsp;9999.00</span>
        </div>
        </div>
        </div>
</div>
<div class="col-md-6">
       <div class="card11">
                    <div class="row">
            <div class="col-md-4">
                <div class="courseheader">
                    <img src="images/product.jpg" class="img-fluid">
                </div>
            </div>
        <div class="col-md-8 pt-2">
             <h6> S.M Business | Wholesellers</h6>

                          <br/>
             <span class="courseprice">₹&nbsp;399.00</span>
        </div>
        </div>
        </div>
</div> -->
</div>
</div>
</div>
</div>
</div>
@include('website.footer')
      <div class="bsnav-mobile">
         <div class="bsnav-mobile-overlay"></div>
         <div class="navbar">
            <div class="masterbarmobile"></div>
         </div>
      </div>

      @include('website.footer-scripts')
      <script type="text/javascript">

            // $(document).ready(function () {
            //     $('.fancynav .active').removeClass('active');
            //     $.each($('.fancynav').find('.nav-item'), function () {
            //         $(this).find('a').toggleClass('active',
            //             window.location.pathname.indexOf($(this).find('a').attr('href')) > -1);

            //         $(this).toggleClass('active',
            //             window.location.pathname.indexOf($(this).find('a').attr('href')) > -1);

            //         if (isTouchDevice()) {
            //             $('.mobileuinav').scrollTo('.nav-item.active');
            //         }
            //     });
            // });
      </script>
   </body>
</html>
