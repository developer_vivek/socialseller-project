<!DOCTYPE html>
<html>
   <head>
      <title>Socialseller</title>
      @include('website.head')
   <style>

.enq1 {
   
   
    
    padding: 10px 40px;
   
    color: #fff;
   background: linear-gradient(179deg, rgb(177 14 14) 19%, rgb(201 35 35) 98%) !important;
    font-weight: 500;
    border-radius: 4px 4px 0 0;
    font-size: 12px;
    cursor: pointer;
margin-bottom: 10px;
}
</style>
   </head>
   <body>

      @include('website.partial.header')

<section class="mb-4 mt-5">
         <div class="clearfix"></div>
         <div class="container">
            <div class="row banner no-gutters justify-content-center">

               <div class="col-12 col-md-6 text-center imgdiv  order-0 order-md-0">
                  <img src="{{ asset('socialseller/images/4136936.jpg')}} "
                     class="img-fluid bannerimg" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500">
               </div>
               <div class="col-12 col-md-6 textdiv order-1 order-md-1">
                  <h2 class="text-black mt-5"><span style="font-size:24px !important;color: #8e8b8b!important;">Free YouTube Videos To</span><br />
                     <strong style="    color: #22a754!important;"> Learn Online Busienss</strong>
                  </h2>
                  <ul class="checklist">
                     <li class="text-black">Quality Content </li>
                     <li class="text-black">In Simple Hindi + English</li>
                     <li class="text-black">Created By Lakshit Sethiya</li>
                  </ul>
                  <h5 class=" ml-1 mt-3 text-black">Get free Courses & Lectures on
                     <br/><span style="color: #22a754;">Whats App</span> <img src="{{ asset('socialseller/images/whts.png') }}" style="width: 20px;">
                     <strong>Signup Now!</strong>
                  </h5>
                  <div class="input-group pt-2 ">
                     <input name="whatsAppMobile" id="whatsAppMobile" required class="form-control" placeholder="Enter your number"
                        type="number" />
                  </div>
                        <div id="mobileError" style="color:red"></div>
                  <div class="input-group ">
                     <a id="" class="btn text-white ml-0" onclick="getWhatsApp()" style="width: 100%;background: #22a754  !important; border-radius: 0 0 0.5rem 0.5rem !important;">Get free Courses</a>
                  </div>
                 
               </div>

            </div>

         </div>
  
      </section>

     @include('website.blog')

        
     
  
   @include ('website.footer')
      <div class="bsnav-mobile">
         <div class="bsnav-mobile-overlay">
         <div class="navbar">
            <div class="masterbarmobile"></div>
         </div>
      </div>

      @include ('website.footer-scripts')
   </body>
</html>
