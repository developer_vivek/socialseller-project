<!DOCTYPE html>
<html>
   <head>
      <title>Socialseller</title>
      @include('website.head')
      <style type="text/css">
        .number
        {
          padding: 5px 15px;
          background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;
          border-radius: 50%;
          z-index: 1;
          color: #ffff;
          margin-bottom: -10px;
        }
        .free
        {
          color:red;
        }
.category11 {
   background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;
   padding: 2px;
margin-bottom: 7px;
border-radius: 2px;
}
.category11:hover {

box-shadow: 0px 0px 5px 5px #2173c738;
}
.category11 h6{

    font-family: inherit;
    font-weight: 200;
    color: inherit;
    font-size: 14px;
    margin-top: 5px;
    margin-bottom: 3px;
padding:5px;

}
.tabs {
  display: grid;
  cursor: pointer;
}

[role="tablist"] {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  grid-gap: 10px;
}

.tabWrapper {
  display: grid;

  margin-top: 0rem;
  padding: 1rem;

}

.bsnav-sticky {

    z-index: 110;
}

      </style>
   </head>
   <body>

      @include('website.partial.header')
      <!--section  style="background-size: 100% 100%!important;background-repeat: no-repeat;
         background-size: cover;">
         <div class="clearfix"></div>
         <div class="container-fluid">
            <div class="">
            <div class="row  justify-content-center">
               <div class="sectionheading pt-5 text-center">
                  <h2 style="font-size: 40px !important;color: #0d2366 !important;    font-weight: bold !important;">Connect with the Best Suppliers for <br/>Reselling Business</h2>
                  <p>Subscribe and Get the Database of Best Online Suppliers</p>
               </div>
            </div>
              <div class="row banner no-gutters text-center justify-content-center ">
               <div class="col-12 col-md-6 text-center imgdiv  order-1 order-md-1 text-center justify-content-center pt-5">

                    <iframe src="https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="" style=" border-radius: 2%;  border: 2px solid #012a54;" id="fitvid6"
                        width="100%" height="315" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500" ></iframe>
                        </div>


            </div>
          </div>
         </div>
      </section-->
	  <section class="mb-4 mt-5">

         <div class="clearfix"></div>
         <div class="container">
            <div class="row banner no-gutters justify-content-center">
            @if (Session::has('error'))
               <div class="col-md-12" style="margin-top:30px;">
                      <div class="alert alert-danger alert-block">
                     <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ Session::get('error') }}</strong>
               </div>


               </div>
         @endif

               <div class="col-12 col-md-6   order-0 order-md-0 text-left">
                  <!--img src="{{ asset('socialseller/images/supplier_cover.gif')}} "
                     class="img-fluid bannerimg" /-->
<div class="pt-5 mt-5">
 <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/YO8JQ4N0E_8" allowfullscreen style="box-shadow:0px 0px 5px 5px #ccc;
    border-radius: 5px;" width="100%" height="315"></iframe>
</div>

               </div>
               <div class="col-12 col-md-6 textdiv order-1 order-md-1 pl-3">
                  <h2 class="text-black mt-5"><strong>Connect with Best Product Suppliers</strong></h2>
				  <p class="text-muted">Connect with Wholesalers in Whatsapp</p>
                  <ul class="checklist pl-3 mt-0">
                     <li class="text-black">Best Wholesalers for Reseller </li>
                     <li class="text-black">Connect Directly in whatsapp</li>
                     <li class="text-black">Direct Shipping to customers</li>
                     <li class="text-black">Wholesalers & Direct Dealers</li>
                     <li class="text-black">Single product Shipping</li>
                     <li class="text-black">Verified Database</li>
                  </ul>
                   <div class="input-group pt-2 ">
                     <input name="whatsAppMobile" id="whatsAppMobile" required class="form-control" placeholder="Enter your number"
                        type="number" />
                  </div>
                        <div id="mobileError" style="color:red"></div>
                  <div class="input-group ">
                     <a id="" class="btn text-white ml-0" onclick="getWhatsApp()" style="width: 100%;background: #22a754  !important; border-radius: 0 0 0.5rem 0.5rem !important;">Get free Courses</a>
                  </div>
                  <!-- <h3 class="text-black">Recharge with Rs.300 to Get 3 Supplier's Connections</h3>
				  <p>Recharge wallet to connect with Suppliers</p>
				  <button type="button" class="btn btn-success btn-lg"><img class="mr-2" src="{{ asset('socialseller/images/wallet-filled-money-tool.svg')}}" width="20px">Recharge Wallet</button>
				  <h3 class="mt-3 text-danger"><strong>0 connections Remaining</strong></h3> -->
               </div>

            </div>

         </div>

      </section>
      <!--section class="" style=" background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;">
         <div class="container pt-2 pb-2">
            <div class="row justify-content-center no-gutters pt-2">
               <div class="col-12 col-md-6">
                  <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/YO8JQ4N0E_8" allowfullscreen style="border: 3px solid #fff;
    border-radius: 5px;"></iframe>
   
                  </div>
              </div>
            </div>
         </div>
      </section-->

      <section style="padding:4rem 0 4rem;">
        <div class="container">
          <div class="sectionheading text-center">
            <h3 class="dark-blue-text">List of Suppliers
            </h3>
            <p>Pay and Get the List of Best Suppliers
            </p>
         </div>

<div class="row">
  <div class="col-md-12">
     <div class="content">
      <div class="tabWrapper">
        <div class="tabs">
          <div class="row  text-center pt-2 justify-content-start ">
             <div class="col-md-2 col-6">
              <div class="category11" role="tab" aria-selected="true" id="tab-all" category_id="all">
                <h6 class="text-white" >All</h6>

              </div>
             </div>
             @foreach($supplier_categories as $key=>$row)
               <div class="col-md-2 col-6">
                <div class="category11" role="tab" aria-selected="true" id="tab-{{$row->id}}" category_id="{{ $row->id  }}">
                  <h6 class="text-white" >{{ $row->name }}</h6>
                </div>
               </div>
              @endforeach

          </div>
          <div role="tabpanel" class="mt-4" aria-labelledby="tab-all">
             <div class="row justify-content-center">
         @foreach($supplier_categories as $row)
         @foreach($row->supplier as $row2)
				<div class="col-12 col-md-4">
					<div class="coursecard aos-init aos-animate" >
					
						<div class="courseimg">
                  @if($row2->video_id)
                  <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{ $row2->video_id }}" allowfullscreen style="border: 3px solid #fff;
    border-radius: 5px;"></iframe>
                  </div>
                  @else
							<img class="img-fluid" src="{{ $row2->image }}" />
                     @endif
							<!--span class="badge badge-warning">{{ $row->name}}</span-->
						</div>
<div class="p-3">
							<div class="row">
								<div class="col-2">
									<div class="supller_logo">

										<img src="{{ url('socialseller/images/Icon_2.png') }}" width="100%">
									</div>
								</div>
								<div class="col-10">
									<h5 class="mb-0"><strong>{{ $row2->name }}</strong><small class="ml-2 text-muted"></small></h5>
									<div class="rating_b">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="p-2 coursedetails text-left">
							<h6 class="text-muted">Supplier Services</h6>
							<div class="row text-left">
                <div class="col-md-12">
                   <ul class="checklist pl-3 mt-0">
                           @foreach($row2->supplier_service as $row3)
                     <li class="text-black">{{ $row3->service_name }}</li>  @endforeach
                  </ul>
                </div>
              </div>
						</div>
	
						 @if($row2->is_free==1)
                  <a id="" class="btn text-white ml-0 btn-success" onclick='subscribe_free_supplier("{{ $row2->id }}","{{ $row2->mobile }}")' style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a>
                  @else

                           @if((Session::get('student_id')))

                  <a  href="{{ url('buy_suppliers_details_request_generate').'/'.$row2->id }}"  onclick="return confirm('Are you sure want to purchase this supplier ?')"  id="" class="btn text-white ml-0 btn-success"  style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a>

                           @else
                            <a  href="javascript:void(0)"  onclick="supplier_alert()"  class="btn text-white ml-0 btn-success"  style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a>
                           @endif
                           @endif



					</div>
            </div>
            @endforeach
            @endforeach
			</div>
      </div>



          @foreach($supplier_categories as $row)
          <div role="tabpanel" class="mt-4" aria-labelledby="tab-{{ $row->id }}" hidden>
			<div class="row justify-content-center">
         @foreach($row->supplier as $row2)
				<div class="col-12 col-md-4">
					<div class="coursecard aos-init aos-animate" >
						
						<div class="courseimg">
							 @if($row2->video_id)
                  <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{ $row2->video_id }}" allowfullscreen style="border: 3px solid #fff;
    border-radius: 5px;"></iframe>
                  </div>
                  @else
							<img class="img-fluid" src="{{ $row2->image }}" />
                     @endif
							<!--span class="badge badge-warning">{{ $row->name}}</span-->
						</div>
<div class="p-3">
							<div class="row">
								<div class="col-2">
									<div class="supller_logo">
										<img src="{{ url('socialseller/images/Icon_2.png') }}" width="100%">
									</div>
								</div>
								<div class="col-10">
									<h5 class="mb-0"><strong>{{ $row2->name }}</strong><small class="ml-2 text-muted"></small></h5>
									<div class="rating_b">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="p-2 coursedetails text-left">
							<h6 class="text-muted">Supplier Services</h6>
							<div class="row text-left">
                <div class="col-md-12">
                   <ul class="checklist pl-3 mt-0">
                           @foreach($row2->supplier_service as $row3)
                     <li class="text-black">{{ $row3->service_name }}</li>  @endforeach

                  </ul>

                </div>
							</div>
                  </div>
                  @if($row2->is_free==1)
                  <a id="" class="btn text-white ml-0 btn-success" onclick='subscribe_free_supplier("{{ $row2->id }}","{{ $row2->mobile }}")' style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a>
                  @else

                  @if((Session::get('student_id')))

                  <a  href="{{ url('buy_suppliers_details_request_generate').'/'.$row2->id }}"  onclick="return confirm('Are you sure want to purchase this supplier ?')"  id="" class="btn text-white ml-0 btn-success"  style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a>

               @else
                  <!-- <a  href="javascript:void(0)"  onclick="return alert('Please Login before purchase supplier ?')" id="" class="btn text-white ml-0 btn-success"  style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a> -->
                  <a  href="javascript:void(0)"  onclick="supplier_alert()" id="" class="btn text-white ml-0 btn-success"  style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a>
               @endif
               @endif

					</div>
            </div>
            @endforeach
         </div>
               <br>
               <section>
                  <div class="row justify-content-center">
                           <div class="col-md-6  p-4 text-center">
                                 <a href="#" class=" green-btn btn text-white ml-0" style=" border-radius: 10px !important;font-weight: bold !important;font-size: 1.4rem !important; width:100% !important;"> GET SUPPLIERS DETAILS</a>
                        </div>
                  </div>



               </section>


      </div>
            @endforeach

                     </div>
                     </div>
                  </div>
               </div>
            </div>
                </div>
              </div>
          </div>

      </div>
  </div>
</div>
              </div>
           </div>
           <!-- <div class="row justify-content-center">
                  <div class="col-md-6  p-4 text-center">
                          <a href="#" class=" green-btn btn text-white ml-0" style=" border-radius: 10px !important;font-weight: bold !important;font-size: 1.4rem !important; width:100% !important;"> GET SUPPLIERS DETAILS</a>
                 </div>
            </div> -->
        </div>
      </section>


       <!-- <section class="" style="background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;padding:0rem 0 6rem;">
         <div class="container  ">
           <div class="row justify-content-center" style="">

                  <div class="col-md-6  p-2 text-center" style="background: #fff;
    border-radius: 10px 10px 20px 20px;    margin-top: -20px;">
                             <h2 style="color: #00a64c!important;">Rs. 299/-</h2>
                          <a href="#" onclick="subscribe_paid()" class="  btn text-white ml-0" style=" border-radius: 10px !important;font-weight: bold !important;font-size: 3.4rem !important; background: #fff; width:100% !important; border: 2px solid #00a64c;color: #00a64c!important;"> SUBSCRIBE</a>
                 </div>

            </div>
            <div class="container ">
               <div class="row no-gutters  pt-5">
                <div class="col-md-12 text-center">
                  <h2 class="text-white">Get 10 New Supplier Details Everytime.</h2>
                  <h4 class="text-white pt-2">Buy Subscription for Just 299/-</h4>
                  <div class="pt-2">
                  <h5 class="text-white ">  Get New Suppliers / Wholesalers Everytime</h5>
                  <h5 class="text-white">  Verified Database of Wholesalers and Suppliers</h5>
                  <h5 class="text-white">  1000+ Registered Wholesalers and Suppliers</h5>
                  </div>
                </div>
               </div>
             </div>
           </div>
         </section> -->

<!--section class="" style="background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;padding:0rem 0 6rem;">
                  <div class="container">
                  <div class="row justify-content-center" style="">

                           <div class="col-md-6  p-2 text-center" style="background: #fff;
            border-radius: 10px 10px 20px 20px;    margin-top: -20px;">
                                    <h2 style="color: #00a64c!important;">Rs. 299/-</h2>
                                 <a href="#" onclick='subscribe_paid("{{ $row->id }}")' class="  btn text-white ml-0" style=" border-radius: 10px !important;font-weight: bold !important;font-size: 3.4rem !important; background: #fff; width:100% !important; border: 2px solid #00a64c;color: #00a64c!important;"> SUBSCRIBE</a>
                        </div>

                     </div>
                     <div class="container ">
                        <div class="row no-gutters  pt-5">
                        <div class="col-md-12 text-center">
                           <h2 class="text-white">Get 10 New Supplier Details Everytime.</h2>
                           <h4 class="text-white pt-2">Buy Subscription for Just 299/-</h4>
                           <div class="pt-2">
                           <h5 class="text-white ">  Get New Suppliers / Wholesalers Everytime</h5>
                           <h5 class="text-white">  Verified Database of Wholesalers and Suppliers</h5>
                           <h5 class="text-white">  1000+ Registered Wholesalers and Suppliers</h5>
                           </div>
                        </div>
                        </div>
                     </div>
                  </div>
            </section-->
      @include('website.feedback')
       @include('website.seller-testimonial')


   @include('website.helping')
   @include('website.trust')
   @include('website.consultation')
   @include('website.footer')
      <div class="bsnav-mobile">
         <div class="bsnav-mobile-overlay"></div>
         <div class="navbar">
            <div class="masterbarmobile"></div>
         </div>
      </div>

      @include('website.footer-scripts')
      <script type="text/javascript">
        const tabs = document.querySelector('.tabs');
const tabButtons = tabs.querySelectorAll('[role="tab"]');
const tabPanels = tabs.querySelectorAll('[role="tabpanel"]');

function handleTabClick(event) {
  tabPanels.forEach(function(panel) {
    panel.hidden = true;
  });

  tabButtons.forEach(function(tab) {
    tab.setAttribute('aria-selected', false);
  });
console.log(tabButtons);
  event.currentTarget.setAttribute('aria-selected', true);
// console.log(tabs.attr("category_id"));
  const {id} = event.currentTarget;
//   const {category_id} = event.currentTarget;
console.log(`${id}`);
  const tabPanel = tabs.querySelector(`[aria-labelledby="${id}"]`);
//   console.log(tabPanel);
  tabPanel.hidden = false;
}

function subscribe_free_supplier(id,mobile)
{
 $.post("/free_supplier_subscribe",
            {
               id:id,
               _token: "{{ csrf_token() }}"
            },
        function(response){
         //  console.log(response);
          if(response==1)
          {
             alert(response);
              window.location.href = "https://api.whatsapp.com/send?l=en&phone="+mobile+"&text=Hi,I am intersted in social seller";

          }
          else
          {
          alert(response);

          }

         //  $('#IncentiveModal').modal('hide');
         //  location.reload();
        });
}
function subscribe_paid(category_id)
{
   // alert("hii");
   // $('.').attr('');
    $.post("/buy_suppliers_details_request_generate",
            {
               category_id:category_id,
               _token: "{{ csrf_token() }}"
            },
        function(response){
               if(response==500)
               {
                  alert('Please login first for buy suppliers details' );
               }
         //  console.log(response);
         //  window.location.href = "https://api.whatsapp.com/send?l=en&phone=+918816933028&text=Hi,I am intersted in social seller";
         //  alert(response);
         //  $('#IncentiveModal').modal('hide');
         //  location.reload();
        });

}
 function getWhatsApp()
         {
            var mobile=$('#whatsAppMobile').val();
            if(!mobile)
            {

                 $('#mobileError').show();
               $('#mobileError').html('Please enter your whats app number');
            }
            else if(mobile.length<9)
            {
                $('#mobileError').show();
               $('#mobileError').html('Please enter correct mobile number');
            }
            else
            {
            console.log(mobile);

               $.post("/add_whatsapp_mobile",
                  {
                     mobile:mobile,
                     _token: "{{ csrf_token() }}"
                  },
            function(response){
               $('#mobileError').hide();

               window.location.href = "https://api.whatsapp.com/send?l=en&phone=+916260276922&text='k'";

            });
            }
         }

      function supplier_alert()
      {
         alert('Please Login before purchase supplier')
          <?php Session::put('path', 'all_suppliers');?>
          window.location.href="{{ url('login')  }}";

      }

tabButtons.forEach(button => button.addEventListener('click', handleTabClick));
      </script>
   </body>
</html>
