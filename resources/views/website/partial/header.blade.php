



            <div class="navbar navbar-expand-sm bsnav bsnav-sticky bsnav-sticky-fade" style="">

                <div class="container-fluid">

                    <a class="navbar-brand" href="{{url('/') }}">
                        <img src="{{asset ('socialseller/images/Logo.png')}} " class="img-fluid" />
                    </a>
                    <button type="button" class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
                    <div class="collapse navbar-collapse justify-content-sm-end">
                        <ul class="navbar-nav navbar-mobile mr-0 mt-5 mt-md-0">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('/') }}">Home</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('courses') }}">Course</a>
                            </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{ url('all_suppliers') }}">Supplier</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link" href="{{ url('free_videos') }}"> Free Videos</a>
                            </li>
                          
                            <li class="nav-item">
                                <a class="nav-link login blue-btn text-white" href="{{ url('login') }}"><i class="fas fa-lock"></i>&nbsp;Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
