<section  style="padding: 4rem 0 4rem; background: #f3f9fd;">
         <div class="sectionheading text-center">
            <h3 class="dark-blue-text">Online Seller's Feedback
            </h3>
            <p class="subheading">आखिर क्या कहना है हमारे बारे मैं लोगो का ?</p>
         </div>
         <div class="container">
			<div>
				<div class="Online_Feedback owl-carousel owl-theme">
					<div class="item">
						<img src="{{ asset('socialseller/images/0001.jpg')}}" width="100%">
					</div>
					<div class="item">
						<img src="{{ asset('socialseller/images/0002.jpg')}}" width="100%">
					</div>
					<div class="item">
						<img src="{{ asset('socialseller/images/0003.jpg')}}" width="100%">
					</div>
					<div class="item">
						<img src="{{ asset('socialseller/images/0004.jpg')}}" width="100%">
					</div>
					<div class="item">
						<img src="{{ asset('socialseller/images/Content Reviews.png')}}" width="100%">
					</div>
				</div>
			</div>
         </div>
      </section>

<script>
$(document).ready(function(){
	$('.Online_Feedback').owlCarousel({
		center: true,
		items:2,
		loop:true,
		margin:10,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:5
			}
		}
	});
});
</script>
<!--section  style="padding: 4rem 0 4rem; background: #f3f9fd;">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
			<img src="{{ asset('socialseller/images/0001.jpg')}}" width="100%">
		</div>
		<div class="col-md-3">
			<img src="{{ asset('socialseller/images/0002.jpg')}}" width="100%">
		</div>
		<div class="col-md-3">
			<img src="{{ asset('socialseller/images/0003.jpg')}}" width="100%">
		</div>
		<div class="col-md-3">
			<img src="{{ asset('socialseller/images/0004.jpg')}}" width="100%">
		</div>
		</div>
	</div>
</section-->