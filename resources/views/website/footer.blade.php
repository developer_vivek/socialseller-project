   <section class="footer  pt-2" style="background:url('/socialseller/images/background-footer.png');    background-size: 100% 100%!important;    background-repeat: no-repeat;">
         <div class="container">
            <div class="row footerrow">
               <div class="col-6 col-md-3">
                  <h6>Socialseller</h6>
                  <a href="">About us</a>
                  <a href="">Team</a>
                  <a href="">Blog</a>
                  <a href="">Privacy Policy</a>
                  <a href="">Terms &amp; Conditions</a>
               </div>
               <div class="col-6 col-md-3">
                  <h6>Featured Courses</h6>
                  <a href="">Reselling Business mastery Course </a>
                  <a href="">Canva Designing course</a>
                  <a href="">Digital Marketing Course for Online Selling </a>
                  <a href="">Dropservicing Mastery Course</a>
                  <a href="">YouTubing Business Course</a>
               </div>
               <div class="col-6 col-md-3">
                  <h6>Features</h6>
                  <a href="">Free Tutorials</a>
                  <a href="">Courses </a>
                  <a href="">Connections </a>
                  <a href="">Consultantation </a>
                  <a href="">Advertisement </a>
                  <a href="">Web Development</a>
                  <a href="">Digital Marketing</a>
               </div>
               <div class="col-6 col-md-3">
                  <h6>Contact Us</h6>
                  <p class="contact">
                     #301, AIC@36INC, 3rd Floor<br />
                     City Center Mall, Pandri<br />
                     Raipur - 492001, Chhattisgarh, IN
                  </p>
                  <p class="contact"><i class="fas fa-fw fa-phone-square"></i>&nbsp;8370044449</p>
                  <p class="contact"><i class="fas fa-fw fa-envelope-open"></i>&nbsp;support@socialseller.in</p>
               </div>
            </div>
         </div>
         <div class="row no-gutters py-2">
            <div class="col-12 text-white text-center small">
              copyright ©️ | Socialseller Technology Private Limited
            </div>
         </div>
   </section>
