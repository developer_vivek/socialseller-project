<!DOCTYPE html>

<html>

<head>
    <title>
       Socialseller Account - Login
    </title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
          crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600|Questrial"
          rel="stylesheet" />
    <script defer
            src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"
            integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9"
            crossorigin="anonymous"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.2/aos.css" />
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css" />
    <link href="{{ asset('socialseller/css/socialsellermain.css') }}" rel="stylesheet" />
    <link rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
          crossorigin="anonymous" />
    <link rel="stylesheet"
          type="text/css"
          href="https://cdn.jsdelivr.net/gh/fitodac/bsnav@master/dist/bsnav.min.css" />

</head>

<body class="loginpage">


@include('website.partial.header');
    <form method="post" action="" id="frmLogin">

        <div class="row no-gutters">
            <div class="col-12">
                <div class="loginform">
                    @if (session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                    @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif

                    <div id="updLogin">

                        <div class="signupform">
                            <div id="pnlEmail">

                                <h3>Socialseller Account</h3>
                                <div class="row no-gutters">
                                    <div class="col-6">
                                        <a id="lnkFB"
                                           class="btn btngoogle"
                                           href="{{ url('auth/facebook')}}">
                                           <img class="img-fluid"
                                                 src="{{ asset('socialseller/images/btn_facebook.png')}}" />
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <a id="lnkGoogle"
                                        class="btn btngoogle"
                                        href="{{ url('auth/google')}}">
                                            <img class="img-fluid"
                                            src="{{ asset('socialseller/images/btn_google_signin_dark_normal_web@2x.png')}}" />
                                        </a>
                                    </div>
                                    <div class="col-12 text-center p-3">
                                        OR

                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label>
                                        Enter your Mobile Number to Get Started:</label>
                                    <input name="txtMobileCheck"
                                           type="text"
                                           maxlength="100"
                                           id="txtMobileCheck"
                                           class="form-control"
                                           placeholder="Ex: 999xx85xxx"
                                           required="" />
                                    <small class="loginhelptext"><i
                                           class="fas fa-exclamation-circle text-danger"></i>&nbsp;Country Code With +
                                        for outside India. (Ex: +61405802xxx)</small>

                                </div> -->
                                <form action="{{ url('login') }}" method="post">
                                <!-- <form action="https://www.socialseller.in/signin" method="post"> -->
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <!-- @csrf -->
                                <div class="form-group">
                                    <label>
                                        Enter your Email id:</label>
                                    <input name="email"
                                           type="email"
                                           maxlength="100",
                                           value="{{old('email')}}"
                                           id="txtMobileCheck"
                                           class="form-control"
                                           placeholder="Enter email"
                                           required="" />
                                    <!-- <small class="loginhelptext"><i
                                           class="fas fa-exclamation-circle text-danger"></i>&nbsp;Country Code With +
                                        for outside India. (Ex: +61405802xxx)</small> -->

                                </div>
                                <div class="form-group">
                                    <label>
                                        Password</label>
                                    <input name="password"
                                           type="password"
                                           maxlength="100"
                                           value="{{old('password')}}"
                                           id="txtMobileCheck"
                                           class="form-control"
                                           placeholder="Enter password"
                                           required="" />
                                    <!-- <small class="loginhelptext"><i
                                           class="fas fa-exclamation-circle text-danger"></i>&nbsp;Country Code With +
                                        for outside India. (Ex: +61405802xxx)</small> -->

                                </div>
                                <input type="hidden" name="path" value="{{ $path }}">
                                <input type="hidden" name="id" value="{{ $id }}">
                                <input type="submit"
                                       name="btnCheck"
                                       value="Next"
                                       id="btnCheck"
                                       class="btn btn-primary btncheck" style="background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;" />
                                </form>
                                <div class="text-center w-100 mt-4">
                                    <a id="lnkLoginPass"
                                       class="loginpasslink"
                                       href="{{ url('forgot_password') }}"><i
                                           class="fas fa-lock"></i>&nbsp;Forgot Password</a>
                                    <a href='{{ url("signup") }}'
                                       class="loginpasslink ml-4"><i class="fas fa-user-plus"></i>&nbsp;New User?
                                        Signup.</a>
                                </div>

                            </div>



                        </div>

                    </div>
                    <input type="hidden"
                           name="hfReturnURL"
                           id="hfReturnURL" />
                </div>
            </div>
        </div>
        <div id="gobackrow"
             class="row no-gutters mb-5 pb-5">
            <div class="col-12">
                <div class="loginform">
                    <a href="{{ url('/') }}"><span>Go back to</span>
                        <img id="imgLogo"
                             src="{{ asset('socialseller/images/Logo.png') }}" /></a>
                </div>
            </div>
        </div>

        <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar">
            </div>
        </div>

    </form>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
            integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.2/aos.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fitodac/bsnav@master/dist/bsnav.min.js"></script>

    <script>

        $('.navbar-toggler').click(function () {
            $('.bsnav-sticky').toggleClass("bg-transparent");
        });

    </script>

</body>

</html>
