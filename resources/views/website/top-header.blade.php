  <section class="masterbar">
            <div class="container-fluid">
                <div class="row no-gutters">
                    <div class="col-12 col-md-10">
                        <a>Quick Switch</a>
                        <a href="#" class="btn btn-primary btn-sm" target="_blank">Legal Services</a>
                        <a href="" class="btn btn-info btn-sm" target="_blank">Online Courses</a>
                        <a href="" class="btn btn-success btn-sm" target="_blank">Investing Tools</a>
                        <a href="" class="btn btn-dark">Books</a>
                        <a href="" class="btn btn-warning">Affiliates</a>
                        <a href="" class="btn btn-danger">Careers</a>
                    </div>
                    <div class="col-12 col-md-2 text-right social">
                        <a href="https://facebook.com/"><i class="fab fa-facebook"></i></a>
                        <a href="https://twitter.com/"><i class="fab fa-twitter"></i></a>
                        <a href="https://instagram.com/"><i class="fab fa-instagram"></i></a>
                        <a href=""><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </section>