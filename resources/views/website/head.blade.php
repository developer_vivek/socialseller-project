<!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous" /><link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700|Montserrat:400,600,700,800" rel="stylesheet" />
    <script defer src="https://use.fontawesome.com/releases/v5.1.0/js/all.js" integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.2/aos.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css" />
    <link href="{{ asset('socialseller/css/socialseller.css') }}" rel="stylesheet" />

<link href="{{ asset('socialseller/images/favicon.png') }}" rel="shortcut icon" type="image/png">


    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/fitodac/bsnav@master/dist/bsnav.min.css" />
 <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.carousel.min.css"/>
    <link rel="stylesheet" href="https://owlcarousel2.github.io/OwlCarousel2/assets/owlcarousel/assets/owl.theme.default.min.css
"/>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '355501361605030');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=355501361605030&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
    <!-- <meta name="mobile-web-app-capable" content="yes" /> -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-WKVP706WTG"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-WKVP706WTG');
</script>

<style type="text/css">
    ul.timeline {
    list-style-type: none;
    position: relative;
}
ul.timeline:before {
    content: ' ';
    background: rgba(12,12,12,0.8);
    display: inline-block;
    position: absolute;
    left: 29px;
    width: 2px;
    height: 100%;
   
}
ul.timeline > li {
    margin: 10px 0;
    margin-left: 10px;

}
ul.timeline > li:before {
    content: ' ';
    background: #28a745;
    display: inline-block;
    position: absolute;
    border-radius: 50%;

    left: 20px;
    width: 20px;
    height: 20px;
    
}
ul.timeline > li .step
{
    color: #012a54 !important;
    font-size: 20px;
    font-weight: bold;
    margin-right: 6px;

}

 .green-btn
 {
   background: #00a64c;

 }
 .blue-btn
 {
    background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;
 }

.text-white
{
    color: #fff !important;
}
.icon11
{
    width:60px;
    margin-top: 20px;
}
.pb-2
{
    padding-bottom: 20px !important;
}

.videothumb
{
    border-radius: 0.5rem !important;
     border: 2px solid #eee;
    padding: 5px;
    
    box-shadow: 1px 0px 11px 4px #cec9c9;
}

.testimonial11 {
    padding: 30px;
    background-color: #e8f7fe;
    border-radius: 20px;
   
    margin-bottom: 50px;
}
.sideborder
{
    text-align: left !important;

    margin-left: 10px;
}
.videoblog
{
         border-radius: 0.5rem !important;
    border: 8px solid #fff;
    padding: 5px;
    background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;

    margin-top: 10px;
    text-align: center;
    box-shadow: 1px 0px 11px 4px #f3ecec;
}
.videoblog h6
{
 color: #fff;
}
.columns1:after {
    content: '';
    background-image: url(images/dots.svg);
    background-repeat: no-repeat;
    position: absolute;
    top: 35px;

    height: 100%;
    width: 50%;
    z-index: -1;
    -webkit-transform: scale(1.6);
    -ms-transform: scale(1.6);
    transform: scale(1.6);
}
@media only screen and (max-width: 600px) {
  .columns1:after {
    display: none;
  }
}

    .columns2:after {
    content: '';
    background-image: url(images/dots.svg);
    background-repeat: no-repeat;
    position: absolute;
    top: 222px;
    right: calc(100% - 299px);
    height: 100%;
    width: 100%;
    z-index: -1;
    -webkit-transform: scale(1.6);
    -ms-transform: scale(1.6);
    transform: scale(1.6);

}
@media only screen and (max-width: 600px) {
  .columns2:after {
    display: none;
  }
}
secard .coursename1 {
    font-size: 1rem;
    color: #000;
    text-align: center;
    display: block;
    font-weight: 500;
    width: 90%;
    margin: 0.5rem auto 0;
    height: 3rem !important;
}
.coursecard .courseimg1 img {

    margin-top: 15px;
}
.dark-blue-text1 {
    color: #0d2366 !important;
    font-weight: bold !important;
    font-size: 24px !important;
}
</style>
