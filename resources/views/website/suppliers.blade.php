<!DOCTYPE html>
<html>

<head>
  <title>Socialseller Account - Supplier</title>@include('website.head')
  <style type="text/css">
    .profile-userpic img {
        max-width: 3rem;
        border-radius: 50%;
    }
    .abs-center-x {
        position: absolute;
        left: 50%;
        transform: translateX(-50%);
    }
    .profiledd {
        width: 10rem;
        right: 0rem;
        left: auto;
        float: right;
        border: none;
        border-radius: 0.5rem;
        box-shadow: 0 5px 30px rgba(0,0,0,0.1);
        margin-top: 1rem;
        -webkit-animation-duration: 0.5s;
        animation-duration: 0.5s;
        padding: 0;
        color: #000;
        font-size: 0.875rem;
        z-index: 9999;
    }
    .authpagecontent {
        background-color: #f5f7fc !important;
        padding: 10rem 0 5rem;
    }
    .fancynav .nav-link.active {
        background-color: #0054d1;
        color: #fff !important;
    }
    .form-control {
        display: block;
        width: 100%;
        padding: 31px;
        margin-top: 5px;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #6c757d !important;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
       border-radius: 10px !important;
    }
    .accountprofile .profilesidebar img {
        margin: 0 auto 2rem;
        width: 70%;
        border-radius: 50%;
        border: solid 8px rgba(0, 84, 209, 0.1);
        display: block;
    }
    .accountprofile .profilesidebar {
        border-right: solid 1px rgba(0, 84, 209, 0.07);
    }
    .card-account {
        border: none !important;
        background-color: #fff;
        border-radius: 0.5rem;
        min-height: 80vh;
        box-shadow: 0 0px 20px rgba(36, 102, 178, 0.1);
        padding: 2rem;
    }
    .accountprofile .btn-success {
        background-color: #00e133 !important;
        border-color: #00e133 !important;
        border: 1px solid #ccc !important;
        margin-top: 1rem;
    }
    .card11{
            border: 1px solid;
        border-radius: 2px;
        height: 150px;
        padding: 5px;
        margin-top: 10px;
        box-shadow: 0px 0px 5px 5px #cccccc47;
    }
    .courseprice{
        background-color: #fff653;
        display: inline-block;
        color: #000 !important;
        font-weight: 500;
        font-size: 1rem;
        margin-top: 0.5rem;
        float: left;
        padding:10px 10px;
    }
  </style>
</head>

<body>@include ('website.dashboard_header')
  <div class="authpagecontent">
    <div class="container">
      <div class="row text-center">
        <div class="col-md-12">
          <div class="mobileuinav">
            <ul class="nav nav-tabs fancynav justify-content-center">
              <li class="nav-item "> <a class="nav-link" href="{{ url('dashboard') }} "><i data-feather="home"></i>Courses</a>
              </li>
              <!-- <li class="nav-item"> <a class="nav-link" href="{{ url('user_course_details/1') }}"><i data-feather="bar-chart">Courses</i></a> -->
              </li>
              <li class="nav-item active"> <a class="nav-link active" href="{{ url('suppliers') }}"><i data-feather="user"></i>Suppliers</a>
              </li>
              <li class="nav-item "> <a class="nav-link " href="{{ url('profile') }}"><i data-feather="user"></i>Profile</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="card card-account">
              <div class="row justify-content-center">
              <!-- <div class="row"> -->
		<div class="col-12">
			<h4><strong>Enrolled Supplier</strong></h4>
		<!-- <hr/> -->
        <hr/>
         @if(count($suppliers)==0)
		  	<p style="color:red">You have not enrolled to any Supplier yet. </p>
         @endif
    </div>
        @foreach($suppliers as $row)
				<div class="col-12 col-md-4">
					<div class="coursecard aos-init aos-animate" >
						<div class="p-3">
							<div class="row">
								<div class="col-2">
									<div class="supller_logo">
										<img src="{{ url('socialseller/images/Icon_2.png') }}" width="100%">
									</div>
								</div>
								<div class="col-10">
									<h5 class="mb-0"><strong>{{ $row->name }}</strong><small class="ml-2 text-muted"></small></h5>
									<div class="rating_b">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="courseimg">
							<img class="img-fluid" src="{{ $row->image }}" />
							<span class="badge badge-warning">{{ $row->category_name}}</span>
						</div>
						<div class="p-2 coursedetails text-center">
							<h6 class="text-muted">Supplier Services</h6>
							<div class="row text-left">
                <div class="col-md-12">
                   <ul class="checklist pl-3 mt-0">
                            @foreach($row->services as $service_row)
                     <li class="text-black">{{ $service_row->name }}</li>  @endforeach

                  </ul>

                </div>
              </div>
						</div>

                  <a href="{{ url('supplier_detail'.'/'.$row->id)}}" class="btn text-white ml-0 btn-success"  style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a>



					</div>
            </div>
            @endforeach

			</div>
        <div class="row">


          <!-- <div class="col-md-12 col-xs-12 justify-content-center">
            <div style="background: #e8f7fe; ">
              <div class="row no-gutters justify-content-center">
                <div class="col-md-7">
                  <h6><img class="img-circle" alt="" src="http://socialseller.co.in/assets/images/test5.png"
                           style="width: 40px;">
                           <b class="">S.M Business</b> | <span> Wholesellers</span>
                           </h6>
                  <div class="row no-gutters">
                    <div class="col-md-3 col-xs-6 col-6">
                      <img src="{{ asset('socialseller/images/product.jpg') }}" class="img-fluid border-radius">
                    </div>
                    <div class="col-md-3 col-xs-6 col-6">
                      <img src="{{ asset('socialseller/images/product.jpg') }}" class="img-fluid border-radius">
                    </div>
                    <div class="col-md-3 col-xs-6 col-6">
                      <img src="{{ asset('socialseller/images/product.jpg') }}" class="img-fluid border-radius">
                    </div>
                    <div class="col-md-3 col-xs-6 col-6">
                      <img src="{{ asset('socialseller/images/product.jpg') }}" class="img-fluid border-radius">
                    </div>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="mt-2 text-center">
                    <h5><i>Services</i></h5>
                    <div class="row pt-2">
                      <div class="col-md-6 col-6 text-center">
                        <label class="">
                          <input type="checkbox" checked="checked">Subscribe <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-6 col-6 text-center">
                        <label class="">
                          <input type="checkbox" checked="checked">Subscribe <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-6 col-6 text-center">
                        <label class="">
                          <input type="checkbox" checked="checked">Subscribe <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-6 col-6">
                        <label class="">
                          <input type="checkbox" checked="checked">Subscribe <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-6 col-6 text-center">
                        <label class="">
                          <input type="checkbox" checked="checked">Subscribe <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-6 col-6 text-center">
                        <label class="">
                          <input type="checkbox" checked="checked">Subscribe <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-6 col-6 text-center">
                        <label class="">
                          <input type="checkbox" checked="checked">Subscribe <span class="checkmark"></span>
                        </label>
                      </div>
                      <div class="col-md-6 col-6 text-center">
                        <label class="">
                          <input type="checkbox" checked="checked">Subscribe <span class="checkmark"></span>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> -->
          <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar">
              <div class="masterbarmobile"></div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
           @include('website.footer')
        @include('website.footer-scripts')
      </body>

</html>
