  <section class=" auth">

            <div class="navbar navbar-expand-sm bsnav bsnav-sticky bsnav-sticky-fade" style=" padding: 0.75rem 2rem !important; background: #fff; ">
                <div class="container-fluid">
                    <a class="navbar-brand" href="/">
                        <img src="{{ asset('socialseller/images/Logo.png')}}" class="img-fluid" />
                    </a>

                    <div class="justify-content-sm-end">
                        <ul class="navbar-nav abs-center-x d-none d-md-flex">
                            <li class="nav-item authpageheading">

                            <h2>Account</h2>

                            </li>
                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="profile-userpic dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img id="imgUserPic" class="img-fluid" src="{{ asset('socialseller/ffffff.png')}}" />
                                </a>
                                <div class="dropdown-menu profiledd animated pulse">
                                    <div class="dropdown-header text-center">
                                        <small>LOGGED IN AS:</small><br />
                                        <span class="text-dark font-weight-bold">
                                            {{ @$session_student_name}} </span>
                                    </div>

                                    <a class="dropdown-item border-top border-light" href="{{ url('home') }}">Home</a>
                                    <a class="dropdown-item border-top border-light" href="{{ url('profile') }}">Profile</a>
                                    <a class="dropdown-item" href="" target="_blank">Support</a>

                                    <a id="lnkLogoutApp" class="dropdown-item" href="{{ url('logout') }}">Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </section>
