<!DOCTYPE html>
<html>
   <head>
      <title>Socialseller</title>
       <link href="{{ asset('socialseller/css/manifest.json') }}" rel="stylesheet" />
      @include('website.head')
      <style>
body{
 overflow-x: hidden;
}
        .progress-item.style2 .progress {
    height: 25px;
}
        .progress-item .progress {
    background-color: #f8f8f8;
    box-shadow: 0 0 1px rgba(0, 0, 0, 0.4) inset;
    border-radius: 0 7px 7px 0;
    height: 14px;
    margin-bottom: 15px;
    overflow: visible;
}
        .checked {
    color: orange;
}
        .house
        {
            border: 1px solid #000;
            padding:10px;
                margin-top: 10px;

        }
         .owl-prev {
         position: absolute;
         left: -30px;
         background: aqua;
         top: 240px;
         }
         .owl-prev span {
         font-size: 70px
         }
         .owl-next {
         position: absolute;
         right: -30px;
         background: aqua;
         top: 240px;
         }
         .owl-next span {
         font-size: 70px
         }
@media only screen and (max-width: 600px) {
  #videoimg111 {
    width: 100% !important;
  }
}
#videoimg111
{
	width: 50%;
}

@media only screen and (max-width: 600px) {
  .video11 {
    width: 100% !important;
    height: 100%!important;
  }
}
.video11
{
    width: 50%;
    height: 315px;

}
      </style>

   </head>
<body>
    <div class="row no-gutters maincontent">
        <div class="col-12 col-md-3">
                <div id="pnlSidebar" class="mastsidebar position-fixed d-none d-md-block" style="background:linear-gradient(to left,#1895fd, #007bff);">

                    <a id="lnkBackLink" class="btnbackcourses" href="/"><i class="fas fa-chevron-circle-left"></i>&nbsp;Back to Home</a>
                     <div class="videowrapper">
                            <div class="videointro">
                                <iframe src="{{ $course->course_url  }}" frameborder="0"
                     allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                     allowfullscreen=""  id="fitvid6"
                     data-aos="zoom-in" data-aos-delay="500" width="100%" height="315"
                     data-aos-duration="500"></iframe>

                            </div>
                        </div>
                    <div class="highlights">
                        <h3>What Will You Get:</h3>
                      {!! $course->what_will_you_get !!}

                    </div>
                    <h3 class="text-center white-text" style="color: #fff">Rs. {{ $course->discount_price }}/-</h3>

                     <form id="paymentSubmit" action="/payment-initiate-request" method="post">
                        @csrf
                        <input type="hidden" name="course_id" value="{{ $course->id }}" >
                        <input type="hidden" name="amount" value="{{ $course->discount_price }}" >
                        @if($is_enroll==0)
                        <a href='#' class="enrollcourse" onclick="checkout()"><i class="fas fa-user-plus"></i>Enroll Now</a>
                        @else
                        <a href='{{ url("dashboard") }}' class="enrollcourse"><i class="fas fa-user-plus"></i>Access</a>
                        @endif
                        </form>

              </div>
        </div>
        <div class="col-12 col-md-9">
                <div class="mastcontent">

                    <!-- @include('website.partial.header'); -->
                    @include('website.partial.header')
                    <div class="clearfix"></div>

                    <div class="courseheader">
  
  	<div style="text-align: right!important;">
  		<label style="font-weight: 100; margin-right: 10px; text-align: center;border-radius: 20px;color: #fff;   padding:4px; background: #3cb773;font-size: 12px;
">
                      <img src="{{ asset('socialseller/images/eye.png') }}" style="width: 20px;">&nbsp;
{{ $course->viewer  }}

     </label>
  	</div>
 
                       <div style="text-align: left;" >
      
     <br/>
                        <!--div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="{{ $course->course_url }}" allowfullscreen></iframe>
</div-->

                       	 <img src="{{ $course->image }}" class="img-fluid bannerimg aos-init aos-animate" style="margin-top:0px !important;">

                       </div><br/>

                        <h5 class="dark-blue-text1"><i> {{ $course->title  }}</i></h5>
                        <p class="lead">
                           {!! $course->description !!}
                        </p>
                        <span class="coursefeatures">
                            <img src="{{ asset('socialseller/images/Icon_5.png') }}" style="width: 20px;">&nbsp;{{ $course->total_lesson }}
                              Lessons
                        </span>
                        <span class="coursefeatures">
                          <img src="{{ asset('socialseller/images/Icon_6.png') }}" style="width: 20px;">&nbsp;Certificate
                        </span>
                        <span class="coursefeatures">
                            <img src="{{ asset('socialseller/images/Icon_7.png') }}" style="width: 20px;">&nbsp;{{ $course->total_hours }}  Hours
                        </span>
                        <div class="clearfix"></div>
                       <del>&#8377;
                               {{ $course->total_price }}</del>
                        <span class="courseprice">&#8377;&nbsp; {{ $course->discount_price }}</span>
                        <form id="paymentSubmit" action="/payment-initiate-request" method="post">
                        @csrf
                        <input type="hidden" name="course_id" value="{{ $course->id }}" >
                        <input type="hidden" name="amount" value="{{ $course->discount_price }}" >
                        @if($is_enroll==0)
                        <a href='#' class="enrollcourse" onclick="checkout()"><i class="fas fa-user-plus"></i>Enroll Now</a>
                        @else
                        <a href='{{ url("dashboard") }}' class="enrollcourse"><i class="fas fa-user-plus"></i>Access</a>
                        @endif

                        </form>
                        <div class="clearfix"></div>


                    </div>

                    <div id="pnlMobileOnly" class="mastsidebarmobile d-block d-md-none">

                        <div class="videowrapper">
                            <div class="videointro">
                                <iframe src="{{ $course->course_url  }}" frameborder="0"
                     allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                     allowfullscreen=""  id="fitvid6"
                     data-aos="zoom-in" data-aos-delay="500" width="100%" height="315"
                     data-aos-duration="500"></iframe>

                            </div>
                        </div>
                         <div class="highlights">
                        <h3>What Will You Get:</h3>
                      {!! $course->what_will_you_get !!}

                    </div>

                    <h3 class="text-center white-text" style="color: #fff">Rs. {{ $course->discount_price }}/-</h3>

                     <form id="paymentSubmit" action="/payment-initiate-request" method="post">
                        @csrf
                        <input type="hidden" name="course_id" value="{{ $course->id }}" >
                        <input type="hidden" name="amount" value="{{ $course->discount_price }}" >
                        @if($is_enroll==0)
                        <a href='#' class="enrollcourse" onclick="checkout()" style="background-color: #0e53b1;
                            color: #FFF;
                            padding: 1.2rem 1.2rem;
                            border-radius: 0.2rem;
                            text-decoration: none;

                            margin: 0 auto;
                            display: block;
                            box-shadow: 0 1px 5px rgb(233 214 214 / 5%);
                            text-align: center;"><i class="fas fa-user-plus"></i>Enroll Now</a>
                         @else
                        <a href='{{ url("dashboard") }}' class="enrollcourse" style="background-color: #0e53b1;
                            color: #FFF;
                            padding: 1.2rem 1.2rem;
                            border-radius: 0.2rem;
                            text-decoration: none;

                            margin: 0 auto;
                            display: block;
                            box-shadow: 0 1px 5px rgb(233 214 214 / 5%);
                            text-align: center;"><i class="fas fa-user-plus"></i>Access</a>
                        @endif
                       </form>

                    </div>
                    <div class="coursedesc">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="coursesell">
                                    <img src="{{ asset('socialseller/images/Language.png')}}" />
                                    <p>
                                       {{ $course->langauge_name  }}
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="coursesell">
                                    <img src="{{ asset('socialseller/images/Student.png')}}" />
                                    <p>
                                         {{ $course->total_enrollment  }}+ Students Enrolled
                                    </p>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="coursesell">
                                    <img src="{{ asset('socialseller/images/Drafted By.png')}}" />
                                    <p>
                                        Drafted by {{ $course->author  }}
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="coursesection">
                        <h2 class="dark-blue-text1">What will you learn ?</h2><br/>
                        <span>इस कोस से आप क्या  सीखोगे </span><br/>
                        <img src="{{ asset('socialseller/images/boder.png')}}" style="width: 200px;">
                              {!! $course->what_will_you_get !!}



                    </div>
                    <div class="coursesection alt">
                        <h2 class="dark-blue-text1">How This Course will Help?</h2><br/>
                        <span>यह कोर्स आपकी कैसे मदद करेगा ? </span><br/>
                        <img src="{{ asset('socialseller/images/boder.png')}} " style="width: 200px;">
                                        {!! $course->how_this_course_will_help_you !!}
                        <!-- <p style="text-align:justify">क्या आप बेहतर भविष्य के लिए निवेश करना चाह रहे हैं पर स्टॉक्स में निवेश करने से डरते हैं? क्या आप मार्केट में प्रचलित मिथों के चलते ख़ुद को रोक रहे हैं?</p>

                        <p style="text-align:justify">चिंता ना करें, हम लाए हैं ये कोर्स जो स्टॉक मार्केट में आपकी सफलता की सीढ़ी बनेगा। इस कोर्स की मदद से स्टॉक मार्केट की सिद्धांतिक और वास्तविक ख़ूबियाँ और ख़ामियाँ जान कर आप निवेश से जुड़े कठोर और सही निर्णय लेने में सक्षम होंगे।</p>

                        <p style="text-align:justify">ये कोर्स आपका पर्चय कराएगा वॉरन बफे, बेंजामिन ग्राहम, आदि जैसे महान निवेशकों की रणनीतियों से।</p>

                        <p style="text-align:justify">ये कोर्स लिखा गया है दोनो नए और पुराने निवेशकों को ध्यान में रखते हुए जिसके लिए आपको पहले से किसी डिग्री, सर्टिफ़िकेट या जानकारी की आवश्यकता नहीं है।</p> -->

                    </div>

<div class="coursesection alt">
   <h2 class="dark-blue-text1">Course Structure</h2>
                        <br/>
                        <span>कोर्स के विषय
 </span><br/>
                        <img src="{{ asset('socialseller/images/boder.png')}} " style="width: 200px;">
       <section style="">

         <div class="">
            <div class=" ">

               <div class=" d-flex flex-row">
                  <ul class="timeline">

                      @foreach($course_structure as $row)
                     <li style="padding-left: 10px;">

                           <img  class="videoimg111" src="{{ @$row->image }}" id="videoimg111">
                     <p>{{ @$row->title }}</p>

                     </li>

                    @endforeach

                  </ul>
               </div>
            </div>
         </div>
      </section>
                    </div>
<div class="coursesection alt">
   <h2 class="dark-blue-text1">Who can Learn From this Course</h2>
        <br/>
        <span>इस कोर्स से कौन कौन सीख सकता है ?</span><br/>
        <img src="{{ asset('socialseller/images/boder.png')}} " style="width: 200px;">
       <section>
        <div class="container">
            <div class="row">

               @foreach($course_for as $row)
                <div class="col-md-4 col-6">
                    <div class="text-center house">
                        <img src="{{$row->image }}" class="img-fluid" >
                        <p>{{ $row->name  }}</p>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
      </section>
</div>
    @include ('website.feedback')
    @include ('website.seller-testimonial')
     <!--@include ('website.enrollinside')-->
    <section class="coursesection alt">
                  <div class="container">
                    <div class="row">
                        <div class="col-12">
                             <h2 class="dark-blue-text1">Know who is founder of socialseller</h2>

                        <!-- <p>इस कोस से आप क्या  सीखोगे </p> -->

                     </div>
                      <div class="row">

                         <div class="col-md-6 col-xs-12">

                             <div style="border: 4px solid #101010; border-radius: 2%;     box-shadow: 2px 0px 14px 1px #928e8e;">
                           <img src="{{ asset('socialseller/images/lesson4.jpg')}}" class="img-fluid">
                          </div>
                         </div>
                         <div class="col-md-6 col-xs-12 ">


                           <span> <i class="fa fa-quote-left font-22 text-black" aria-hidden="true" style="float: left; top:0px"></i>&nbsp; Lakshit Sethiya is a lawyer and an Entrepreneur by Profession.
                           He is a Professional Online Business Trainer, especially for small online businesses.
                           He has successfully helped more than 2000 Resellers to build their online business without any Investment.
                           He has a rich 3 years+ experience in Reselling business and dropshipping business. He is currently CEO and Founder of Socialseller App and Socialseller Academy <i class="fa fa-quote-right font-22 text-black" aria-hidden="true" style="float: right; top:0px"></i></span>


                         </div>




                       </div>

                    </div>

    </section>


<div class="coursesection">
    <section data-bg-color="#fff" style="background: rgb(255, 255, 255) !important;">
                           <div class="container">
        <div class="row">
           <div class="col-md-12">
               <h1 class=" dark-blue-text1">Students Feedback</h1>

            </div>
        </div>
                <div class="row">
                     <div class="col-md-3 col-xs-12 text-center">
                         <span style="font-size:110px;color:#000;">{{ $course->ratings }}</span><br>
                         <span>
                             <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa  fa-star checked font-22"></span>
                         </span><br>
                         <span style="font-size:20px;color:#000;">Course Rating</span>
                     </div>
                     <div class="col-md-6 col-xs-12">
                         <div class="progressbar-container">
                          <div class="progress-item style2">
                            <div class="progress">
                              <div class="progress-bar appeared" data-percent="56" style="width: 56%;"><span class="percent">56%</span></div>
                            </div>

                          </div>
                          <div class="progress-item style2">

                            <div class="progress">
                              <div class="progress-bar appeared" data-percent="36" style="width: 36%;"><span class="percent">36%</span></div>
                            </div>
                          </div>
                          <div class="progress-item style2">

                            <div class="progress">
                              <div class="progress-bar appeared" data-percent="8" style="width: 8%;"><span class="percent">8%</span></div>
                            </div>
                          </div>
                          <div class="progress-item style2">

                            <div class="progress">
                              <div class="progress-bar appeared" data-percent="1" style="width: 1%;"><span class="percent">1%</span></div>
                            </div>
                          </div>
                          <div class="progress-item style2">

                            <div class="progress">
                              <div class="progress-bar appeared" data-percent="1" style="width: 1%;"><span class="percent">1%</span></div>
                            </div>
                          </div>
                        </div>
                     </div>
                     <div class="col-md-3 col-xs-12 pt-10">
                        <p class="pt-20"><span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>&nbsp;&nbsp;<span class="font-18">56%</span>
                         </p>
                         <p class="pt-0"><span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star font-22"></span>&nbsp;&nbsp;<span class="font-18">36%</span>
                         </p>
                         <p class="pt-0"><span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star  font-22"></span>
                            <span class="fa fa-star font-22"></span>&nbsp;&nbsp;<span class="font-18">8%</span>
                         </p>
                          <p class="pt-0"><span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star  font-22"></span>
                            <span class="fa fa-star  font-22"></span>
                            <span class="fa fa-star font-22"></span>&nbsp;&nbsp;<span class="font-18">1%</span>
                         </p>
                         <p class="pt-0"><span class="fa fa-star checked font-22"></span>
                            <span class="fa fa-star  font-22"></span>
                            <span class="fa fa-star  font-22"></span>
                            <span class="fa fa-star  font-22"></span>
                            <span class="fa fa-star font-22"></span>&nbsp;&nbsp;<span class="font-18">&lt;1%</span>
                         </p>
                     </div>
                </div>
            </div>
    </section>
</div>

@include('website.footer')
<div class="bsnav-mobile">
         <div class="bsnav-mobile-overlay">
         <div class="navbar">
            <div class="masterbarmobile"></div>
         </div>
      </div>
   </div>
<div class="container">
<div class="row justify-content-center text-center">
<div class="col-md-12" style="position: fixed;
    bottom: 0px;
    transition: top 0.3s;
    overflow: hidden;
    background: #e8f7fe;
    z-index: 1000;">

<div class="courseheader d-block d-md-none " style="margin-top: 5px !important;    background: #e8f7fe;">
                      <div class="text-center">
                        <del>&#8377;
                               {{ $course->total_price }}</del>
                        <span class="courseprice">&#8377;&nbsp; {{ $course->discount_price }}</span>
                        <form id="paymentSubmit" action="/payment-initiate-request" method="post">
                        @csrf
                        <input type="hidden" name="course_id" value="{{ $course->id }}" >
                        <input type="hidden" name="amount" value="{{ $course->discount_price }}" >
                        @if($is_enroll==0)
                        <a href='#' class="enrollcourse" onclick="checkout()" style="    width: 52%;"><i class="fas fa-user-plus"></i>Enroll Now</a>
                        @else
                        <a href='{{ url("dashboard") }}' class="enrollcourse" style="    width: 52%;"><i class="fas fa-user-plus"></i>Access</a>
                        @endif

                        </form>
                        <div class="clearfix"></div>
</div>

                    </div>

</div>
</div>
</div>

                </div>
            </div>

        </div>



      @include ('website.footer-scripts')
      <script>
          function checkout()
          {
             let student_id ="{{ Session::get('student_id') }}";
             if(student_id)
             {
                document.getElementById("paymentSubmit").submit();
             }
             else
             {

                //  let url="{{ url('login/course').'/'.$course->id   }}";
              <?php Session::put('path', 'course_details/' . $course->id);?>

                 let url="{{ url('login')}}";
                 alert("please login first before enrollment");
                 location.replace(url);
             }

          }
    </script>
   </body>
</html>
