<!DOCTYPE html>

<html>

<head>
    <title>
       Socialseller Appointment
    </title>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
          crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600|Questrial"
          rel="stylesheet" />
    <script defer
            src="https://use.fontawesome.com/releases/v5.1.0/js/all.js"
            integrity="sha384-3LK/3kTpDE/Pkp8gTNp2gR/2gOiwQ6QaO7Td0zV76UFJVhqLl4Vl3KL1We6q6wR9"
            crossorigin="anonymous"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.2/aos.css" />
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/css/swiper.min.css" />
    <link href="{{ asset('socialseller/css/socialsellermain.css') }}" rel="stylesheet" />
    <link rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
          crossorigin="anonymous" />
    <link rel="stylesheet"
          type="text/css"
          href="https://cdn.jsdelivr.net/gh/fitodac/bsnav@master/dist/bsnav.min.css" />

</head>

<body class="loginpage">


@include('website.partial.header');


        <div class="row no-gutters">
            <div class="col-12">
                <div class="loginform">

                    @if (session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif
                    @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                    @endif


                    <div id="updLogin text-center">
                       <img src="{{ asset('socialseller/images/consult.png')}} "
                     class="img-fluid " >
                         <h3 class="pt-5" style="margin-bottom: 0px !important"> Payment Details</h3>
                         <h3 style="color: green">Rs. 2000</h3>
                        <div class="signupform">
                            <div id="pnlEmail">

                                <form action="{{ url('appointment') }}" method="post" id="paymentSubmit">
                                    @csrf
                                <div class="form-group">
                                    <label>
                                        Enter your Name</label>
                                    <input name="name"
                                           type="text"
                                           maxlength="100",
                                           value="{{old('name')}}"
                                           id="name"
                                           class="form-control"
                                           placeholder="Enter Name"
                                           required="" />
                                    <!-- <small class="loginhelptext"><i
                                           class="fas fa-exclamation-circle text-danger"></i>&nbsp;Country Code With +
                                        for outside India. (Ex: +61405802xxx)</small> -->

                                </div>
                                @error('name')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                                <div class="form-group">
                                    <label>
                                        Mobile Number</label>
                                    <input name="mobile"
                                           type="number"
                                           maxlength="100"
                                           value="{{old('mobile')}}"
                                           id="mobile"
                                           class="form-control"
                                           placeholder="Enter mobile"
                                           required="" />
                                    <!-- <small class="loginhelptext"><i
                                           class="fas fa-exclamation-circle text-danger"></i>&nbsp;Country Code With +
                                        for outside India. (Ex: +61405802xxx)</small> -->
                                @error('mobile')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                                </div>
                                <div class="form-group">
                                    <label>
                                        Enter your Preferred Date and Time
                                    </label>
                                   <input type="datetime-local"  id="birthdaytime" name="appointment_time" maxlength="100" class="form-control">
                                </div>
                                @error('appointment_time')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                                <!-- <input type="submit"
                                       name="btnCheck"
                                       value="Pay"
                                       id="btnCheck"
                                       class="btn btn-primary btncheck" style="background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;" /> -->



                                    <button id="btnCheck" type="button"
                                       class="btn btn-primary btncheck" style="background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;" onclick="checkout()">Pay</button>

                                </form>

                            </div>



                        </div>

                    </div>

                </div>
            </div>
        </div>
                <div class="container-fluid pt-5">
<div class="row justify-content-center">
 @foreach($appointment_videos as $row)
<div class="col-md-4 mb-4">

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="
{{ $row->url }}" allowfullscreen></iframe>
</div>
</div>
@endforeach
</div>
</div>
        <div id="gobackrow"
             class="row no-gutters mb-5 pb-5">
            <div class="col-12">
                <div class="loginform">
                    <a href="{{ url('/') }}"><span>Go back to</span>
                        <img id="imgLogo"
                             src="{{ asset('socialseller/images/Logo.png') }}" /></a>
                </div>
            </div>
        </div>

        <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar">
            </div>
        </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
            integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.2/aos.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.3/js/swiper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/fitodac/bsnav@master/dist/bsnav.min.js"></script>

    <script>

        $('.navbar-toggler').click(function () {
            $('.bsnav-sticky').toggleClass("bg-transparent");
        });
        function checkout()
          {
            let name=$('#name').val();
            let mobile=$('#mobile').val();
            let time=$('#birthdaytime').val();
            // alert(time);
            // if(name || !mobile || !time)
            // {
            //     alert("Please fill all fields");
            // }
            // // if(time)
            // // {

            // // }
            // else
            // {
             let student_id ="{{ Session::get('student_id') }}";

             if(student_id!="")
             {
                document.getElementById("paymentSubmit").submit();
             }
             else
             {
                 let url="{{ url('login')   }}";
                  <?php Session::put('path', 'appointment');?>
                 alert("please login first before enrollment");
                 location.replace(url);
             }
            }

        //   }
    </script>

</body>

</html>
