<!DOCTYPE html>
<html>
   <head>
      <title>Socialseller</title>
      @include('website.head')
   </head>
   <body>

      @include('website.partial.header')


      <section class="courses ">
      	<section class="pt-5 text-center">
         <div class="container">
            <div class="row  justify-content-center">

               	<div class="sectionheading pt-5 text-center">
                  <h3 class="dark-blue-text">Best Online Courses</h3>
                  <p class="subheading">These Courses will help you Grow your Online Business</p>
               </div>

            </div>
         </div>
      </section>
         <div class="container-fluid">
            <div class="row  justify-content-center">
                @foreach($courses as $row)
            <div class="col-12 col-md-3" data-aos="fade-up" data-aos-delay='0'>

                  <div class="coursecard">
                     <div class="courseimg">
                        @if(isset($row->image))
                        <img class="img-fluid" src="{{ $row->image }}" />
                        @endif
                        <span class="badge badge-warning">{{ $row->name }}</span>
                     </div>
                     <div class="p-2 coursedetails">
                        <span class="coursename">{{ $row->title }}</span>
                        <div class="clearfix"></div>
                        <div class="coursehighlights">
                           <img src="{{ asset('socialseller/images/Icon_5.png')}}" style="width: 20px;">
                           <p>{{ $row->total_lesson }} Lessons</p>
                        </div>
                        <div class="coursehighlights">
                           <!-- <img src="{{ asset('socialseller/images/Icon_6.png')}}" style="width: 20px;"> -->
                           @if($row->is_certificate==0)
                            <i class="fas fa-certificate" style='color:#d4eefe'></i>
                                            <p><del>Certificate</del></p>
                                            @else
                                            <i class="fas fa-certificate" style=''></i>
                                            <p>Certificate</p>
                                            @endif

                        </div>
                        <div class="coursehighlights">
                           <img src="{{ asset('socialseller/images/Icon_7.png')}}" style="width: 20px;">
                           <p>{{ $row->total_hours }} Hours</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <a href="{{ url('course_details').'/'.$row->id }}" class="enrollbtn"><i class="fas fa-user-plus"></i>Enroll Now</a>
                  </div>
               </div>
             @endforeach


         </div>
      </section>


   @include ('website.footer')
      <div class="bsnav-mobile">
         <div class="bsnav-mobile-overlay">
         <div class="navbar">
            <div class="masterbarmobile"></div>
         </div>
      </div>

      @include ('website.footer-scripts')
   </body>
</html>
