<style type="text/css">
   .img-circle
   {
    border: 1px solid #000;
    border-radius: 50%;
   }
   .checked {
    color: orange;
}
.fa-star-half-empty:before, .fa-star-half-full:before, .fa-star-half-o:before {
    content: "\f123";
}

</style>
<section  style="padding: 4rem 0 4rem;">
         <div class="sectionheading text-center">
            <h3 class="dark-blue-text">Sellers Testimonials
            </h3>
            <p class="subheading">आखिर लर्नर्स का अनुभव कैसा था
            </p>
         </div>
         <div class="container">
         <div>
            <div class="Online_Feedback owl-carousel owl-theme">
               <div class="item text-center">
                <div class="testimonial text-center pt-60">
                   <div class="testimonial11">
                     <div class="thumb text-center mb-0 mr-0 pr-20"  >
                    <center> <img  class="img-circle" alt="" 
                      src="{{ asset('socialseller/images/testip1.png')}}" style="width: 100px;"></center>
                  </div>
                  <div class=" text-center pull-center">
                     <p class="author mt-20">- <span class="text-black-333">Ranu Jain  </span> <small><em>Jainzstore Watches</em></small></p>
                  
                    
                   <center> <img src="{{ asset('socialseller/images/testi3.png')}}"  class="pull-center"></center>
                  </div>
                   </div>
                </div>
              </div>
              <div class="item text-center">
                <div class="testimonial text-center pt-60">
                   <div class="testimonial11">
                     <div class="thumb text-center mb-0 mr-0 pr-20"  >
                    <center> <img  class="img-circle" alt="" src="{{ asset('socialseller/images/testip3.png')}}" style="width: 100px;"></center>
                  </div>
                  <div class=" text-center pull-center">
                     <p class="author mt-20">- <span class="text-black-333">Khushboo</span> <small><em>founder: Printed Hub</em></small></p>
                     
                   
                   <center> <img src="{{ asset('socialseller/images/testi1.png')}}"  class="pull-center"></center>
                  </div>
                   </div>
                </div>
              </div>
               <div class="item text-center">
                <div class="testimonial text-center pt-60">
                   <div class="testimonial11">
                     <div class="thumb text-center mb-0 mr-0 pr-20"  >
                    <center> <img  class="img-circle" alt="" src="{{ asset('socialseller/images/testip2.png')}}" style="width: 100px;"></center>
                  </div>
                  <div class=" text-center pull-center">
                     <p class="author mt-20">- <span class="text-black-333">Vardhman Parakh</span> <small><em>founder: Milan Stores </em></small></p>
                     
                   
                   <center> <img src="{{ asset('socialseller/images/testi4.png')}}" class="pull-center"></center>
                  </div>
                   </div>
                </div>
              </div>
               <div class="item text-center">
                <div class="testimonial text-center pt-60">
                   <div class="testimonial11">
                     <div class="thumb text-center mb-0 mr-0 pr-20"  >
                    <center> <img  class="img-circle" alt="" src="{{ asset('socialseller/images/testip4.png')}}" style="width: 100px;">
                    </center>
                  </div>
                  <div class=" text-center pull-center">
                     <p class="author mt-20">- <span class="text-black-333">Kunwar Singh Tak</span> <small><em>founder: Konkani Store</em></small></p>
                     
                    
                   <center> <img src="{{ asset('socialseller/images/testi2.png')}}" class="pull-center"></center>
                  </div>
                   </div>
                </div>
              </div>
            </div>
         </div>
         </div>
      </section>

<script>
$(document).ready(function(){
   $('.Online_Feedback').owlCarousel({
      center: true,
      items:2,
      loop:true,
      margin:10,
      responsive:{
         0:{
            items:1
         },
         600:{
            items:3
         },
         1000:{
            items:5
         }
      }
   });
});
</script>
