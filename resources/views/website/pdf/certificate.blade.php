<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<style>
.container {
  position: relative;
  text-align: center;
  color: #000;
  /* height:100vh; */
}

/* Bottom left text */
.bottom-left {
    font-size: 17px;
    font-weight: 300;
    position: absolute;
    top: 33.51%;
    left: 82.4%;
     white-space: nowrap;
    transform: translate(-50%, -50%);}


/* Centered text */
.centered {
    font-size: 20px;
    font-weight: 500;
    position: absolute;
    top: 30.55%;
    left: 75%;
     white-space: nowrap;
    transform: translate(-50%, -50%);
}
</style>
</head>
<body>
<div class="container">
  <img src="{{ asset('socialseller/certificate.jpeg') }}" style="width:100%;">
  <!-- <img src="socialseller/certificate.jpeg" style="width:100%;"> -->

  <div class="centered"> {{ $student_name }}</div>
  <div class="bottom-left">{{ $completed_at  }}</div>
</div>
</body>
</html>
