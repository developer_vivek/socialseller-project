<style type="text/css">
  .department-tab2 {
    padding: 5px 15px;
    transition: 0.2s;
    text-align: left;
    box-shadow: 0px 4px 10px rgb(0 0 0 / 8%);
    margin: 0px 0 20px;
}
.department-tabs2  {
    display: flex;
    justify-content: center;
    flex-direction: column;

    margin: 15px;
    background-color: #ffffff;
}
.department-tab2-number {
    font-size: 32px;
    line-height: 1em;
    font-weight: 600;
    font-family: "Poppins", serif;
    color: #3368c6;
}
.department-tab2-decor {
    width: 33px;
    height: 3px;
    background-color: #3368c6;
}
.department-tab2-decor:not(:first-child) {
    margin-top: 15px;
}
.department-tab2.active, .department-tab2:hover {
    background-color: #3368c6;
    color: #ffffff;
    }
     .department-tab2.active .department-tab2-number, .department-tab2.active .department-tab2-text, .department-tab2:hover .department-tab2-number, .department-tab2:hover .department-tab2-text {
      color: #ffffff; }
    .department-tab2.active .department-tab2-decor, .department-tab2:hover .department-tab2-decor {
      background-color: #ffffff; }
  .double-title {
  position: relative; }
  .double-title span {
    position: relative;
    z-index: 1; }

.double-title:after {
  position: absolute;
  z-index: 0;
  right: 0;
  left: -0.05em;
  top: -.65em;
  font-size: 120px;
  line-height: 1em;
  white-space: nowrap;
  content: attr(data-title);
  color: #f7f9fb; }
  @media (max-width: 1229px) {
    .double-title:after {
      font-size: 100px; } }
  @media (max-width: 575px) {
    .double-title:after {
      font-size: 70px; } }

.bg-title {
  font-size: 120px;
  line-height: 1em;
  white-space: nowrap;
  content: attr(data-title);
  color: #ffffff; }
  @media (max-width: 1229px) {
    .bg-title {
      font-size: 100px; } }
  @media (max-width: 1229px) {
    .bg-title {
      margin-bottom: -.75em; } }
  @media (max-width: 575px) {
    .bg-title {
      font-size: 70px; } }
  .bg-title + * {
    position: relative;
    margin-top: -60px; }

.double-title--white:after {
  color: #ffffff; }

.double-title--vcenter:after {
  top: -.36em; }

.double-title--center:after {
  right: auto;
  left: 50%;
  transform: translateX(-50%); }

@media (min-width: 1230px) {
  .text-lg-left .double-title:after {
    left: -0.2em; } }
@media (max-width: 1229px) {
  .text-lg-left .double-title:after {
    left: 0; } }
@media (max-width: 1229px) {
  .double-title--center-lg:after {
    right: auto;
    left: 50%;
    transform: translateX(-50%); } }
.double-title--left:after {
  left: -0.2em; }

.double-title--right:after {
  left: auto;
  right: -0.2em; }
  @media (max-width: 1229px) {
    .double-title--right:after {
      right: auto;
      left: 50%;
      transform: translateX(-50%); } }

.double-title--top:after {
  left: -0.2em;
  top: -.76em; }

.double-title--top-md:after {
  left: -0.2em;
  top: -.76em; }
</style>
 <section class="py-5">

    <div class="container">
		<div class="text-center">
			<h3 class="dark-blue-text">How to Enroll inside the Course?</h3>
			<p class="subheading"> जानिये कैसे online course को ख़रीदा जाता है</p>
		</div>
        <div class="row text-center pt-4">
            <div class="col-12 col-md-6 mb-3">
               <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/X0dNH9hjs_I?controls=0" allowfullscreen></iframe>
</div>
               
               
            </div>

            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-md-6">

                        <div class="department-tab2">
                            <div class="department-tab2-number">1</div>
                            <div class="department-tab2-text">Check out the Course Intro Videos and Lessons</div>
                            <div class="department-tab2-decor"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="department-tab2">
                            <div class="department-tab2-number">2</div>
                            <div class="department-tab2-text">Click on Enroll Button and Create your Account on Socialseller  </div>
                            <div class="department-tab2-decor"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="department-tab2">
                            <div class="department-tab2-number">3</div>
                            <div class="department-tab2-text"> Complete the Payment and Tutorial Videos will be open to watch. </div>
                            <div class="department-tab2-decor"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="department-tab2">
                            <div class="department-tab2-number">4</div>
                            <div class="department-tab2-text">Complete the Course and Download your Completion Certificate.</div>
                            <div class="department-tab2-decor"></div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>
</section>
