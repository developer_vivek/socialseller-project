<!DOCTYPE html>
<html>
   <head>
      <title>Socialseller</title>
      @include('website.head')

   </head>
   <body>

      @include('website.partial.header')
		<section class="mb-4 mt-5">
         <div class="clearfix"></div>
         <div class="container">
            <div class="row banner no-gutters justify-content-center">

               <div class="col-12 col-md-6 text-center imgdiv  order-0 order-md-0">
                  <img src="{{ asset('socialseller/images/Graphic.png')}} "
                     class="img-fluid bannerimg" data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500">
               </div>
               <div class="col-12 col-md-6 textdiv order-1 order-md-1">
                  <h2 class="text-black mt-5"><span style="font-size:24px !important;color: #8e8b8b!important;">Courses That Help You </span><br />
                     <strong style="    color: #2376c9 !important;">Create your Online Business</strong>
                  </h2>
                  <ul class="checklist">
                     <li class="text-black">Drafted By Lakshit Sethiya.</li>
                     <li class="text-black">Explained in Hindi + English</li>
                     <li class="text-black">24X7 Lifetime access </li>
                  </ul>
                  <h5 class=" ml-3 mt-3 text-black">Get free Courses & Lectures on
                     <br/><span style="color: #22a754;">Whats App</span> <img src="{{ asset('socialseller/images/whts.png') }}" style="width: 20px;">
                     <strong>Signup Now!</strong>
                  </h5>
                  <div class="input-group pt-2 ">
                     <input name="whatsAppMobile" id="whatsAppMobile" required class="form-control" placeholder="Enter your number"
                        type="number" />
                  </div>
                        <div id="mobileError" style="color:red"></div>
                  <div class="input-group ">
                     <a id="" class="btn text-white ml-0" onclick="getWhatsApp()" style="width: 100%;background: #22a754  !important; border-radius: 0 0 0.5rem 0.5rem !important;">Get free Courses</a>
                  </div>
                  <a href="#" class="mt-2 btn-block text-black">OR View Our Courses</a>
               </div>

            </div>

         </div>

      </section>
      <section class="bg-light py-5">
		<div class="container">
			<div class="text-center pb-4">
				<h3 class="dark-blue-text">Trusted by Thousands of Online Sellers</h3>
				<p class="subheading">These Courses will help you Grow your Online Business</p>
			</div>
			<div class="row justify-content-center">
				<div class="col-6 col-md-3 text-center ">
					<div class="box123"  data-aos="fade-up" data-aos-delay='0'>
						<div class="columns1 hidden-xs hidden-sm"></div>
						<img src="{{ asset('socialseller/images/Icon_1.png')}}" class="text-center icon11">
						<h3 class="text-white text-center pt-4">{{ $count->satisfied_customers }}</h3>
						<p class="text-white text-center">Satisfied Students </p>
					</div>
				</div>
				<div class="col-6 col-md-3 text-center ">
					<div class="box123" data-aos="fade-up" data-aos-delay='0'>
						<img src="{{ asset('socialseller/images/Icon_2.png')}}" class="text-center icon11">
						<h3 class="text-white text-center pt-4">{{ $count->complete_projects }}</h3>
						<p class="text-white text-center">Tutorial Videos </p>
					</div>
				</div>
				<div class="col-6 col-md-3 text-center">
					<div class="box123" data-aos="fade-up" data-aos-delay='0'>
						<img src="{{ asset('socialseller/images/Icon_3.png')}}" class="text-center icon11">
						<h3 class="text-white text-center pt-4">{{ $count->views }}</h3>
						<p class="text-white text-center">Views</p>
					</div>
				</div>
				<div class="col-6 col-md-3 text-center ">
					<div class="box123 " data-aos="fade-up" data-aos-delay='0'>
						<img src="{{ asset('socialseller/images/Icon_4.png')}}" class="text-center icon11">
						<h3 class="text-white text-center pt-4">{{ $count->award_won }}</h3>
						<p class="text-white text-center">Award Won</p>
						<div class="columns1 hidden-xs hidden-sm"></div>
					</div>
				</div>
			</div>
		</div>
	</section>

      <section class="py-5">
         <div class="container-fluid">
			<div class="text-center pb-4">
				<h3 class="dark-blue-text">Best Online Courses</h3>
				<p class="subheading">इन ऑनलाइन Courses के द्वारा अपना ऑनलाइन बिज़नेस शुरू करे |
</p>
			</div>
            <div class="row justify-content-center">
                @foreach($courses as $row)
 <a href="{{ url('course_details').'/'.$row->id }}" >
            <div class="col-12 col-md-3">

                  <div class="coursecard"  data-aos="fade-up" data-aos-delay='0'>
                     <div class="courseimg">

                        @if(isset($row->image))

                        <img class="img-fluid" src="{{ $row->image }}" />
                        @endif
                        <!--span class="badge badge-warning">{{ $row->name }}</span-->
                     </div>
                     <div class="p-2 coursedetails">
                        <span class="coursename">{{ $row->title }}</span>
                        <div class="clearfix"></div>
                        <div class="coursehighlights">
                           <img src="{{ asset('socialseller/images/Icon_5.png')}}" style="width: 20px;">
                           <p>{{ $row->total_lesson }} Lessons</p>
                        </div>
                        <div class="coursehighlights">
                           <!-- <img src="{{ asset('socialseller/images/Icon_6.png')}}" style="width: 20px;"> -->
                           @if($row->is_certificate==0)
                            <i class="fas fa-certificate" style='color:#d4eefe'></i>
                                            <p><del>Certificate</del></p>
                                            @else
                                            <i class="fas fa-certificate" style=''></i>
                                            <p>Certificate</p>
                                            @endif

                        </div>
                        <div class="coursehighlights">
                           <img src="{{ asset('socialseller/images/Icon_7.png')}}" style="width: 20px;">
                           <p>{{ $row->total_hours }} Hours</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <a href="{{ url('course_details').'/'.$row->id }}" class="enrollbtn"><i class="fas fa-user-plus"></i>Enroll Now</a>
                  </div>
               </div>
</a>
             @endforeach


         </div>
      </section>
      <section  class="bg-light py-5">
         <div class="container">
         	<div class="text-center">
                <h3 class="dark-blue-text">Frequently Asked Questions</h3>
                <p class="subheading"> अधिकतर पूछे जाने वाले सवाल
</p>
            </div>
            <div class="faq mt-4">
               <div class="accordion" id="accordion-tab-1">
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-1">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-1" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-1"><i
                              class="fas fa-plus-circle"></i>&nbsp;Socialseller Academy se App Kya Seekh Sakte ho ? Hum Apki Kaise Madad Krenge ? </button>
                        </h5>
                     </div>
                     <div class="collapse show" id="accordion-tab-1-content-1"
                        aria-labelledby="accordion-tab-1-heading-1" data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>Socialseller Academy ke online Video courses ki madad se koi bhi person bina kuch nivesh kiye appna online business create kar sakta hai aur continiously grow kar sakte hai.  Through Our Video Lessons, Any person can Create and Grow their online business without any Investment.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-2">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-2" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-2"><i
                              class="fas fa-plus-circle"></i>&nbsp;Who can Sign-up, Iss course kon kon Enroll kar sakta hai / Any Restriction on Age?</button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-2" aria-labelledby="accordion-tab-1-heading-2"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>Socialseller Academy ke sath koi bhi Vayakti Online business seekh sakta hai. Couse main enroll hone ke liye kisi specific knowledge ki zaruruat nahi hai. Agar aapko normal Social Media aur Internet use krna atta hai toh app course ko join kar Online Business Seekh sakte hai.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-3">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-3" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-3"><i
                              class="fas fa-plus-circle"></i>&nbsp;How can I signup Inside the Couse? Course main kaise enroll kare ?</button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-3" aria-labelledby="accordion-tab-1-heading-3"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>To Sign-up any course, you just have to press "Enroll" button and after filling very few Information, you just need to complete payment. Payment hone ke baad course aapke dashboard main Aajayega. You can access the course anytime from Dashboard, You will have lifetime access to the course.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-4">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-4" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-4"><i
                              class="fas fa-plus-circle"></i>&nbsp;Whats Payment Modes are acceptable in webiste ?</button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-4" aria-labelledby="accordion-tab-1-heading-4"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>You can use all major Payment modes in our website, The payment is secured by Razorpay. You can use Debit Card, Credit Card, Net Banking, UPI and Other wallets to make Payment. Your Payment and Information will be 100% Secured with our System.

                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-5">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-5" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-5"><i
                              class="fas fa-plus-circle"></i>&nbsp;
How can I access the course? Can I download Videos?  </button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-5" aria-labelledby="accordion-tab-1-heading-5"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>It is an online course, You can see Tutorial Videos in your Dashboard only after payment. You cannot Download or Save Videos. Your videos will be saved in the Dashboard. You can access to tutorial Videos 24x7. We will also be updating new Videos during Intervals.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-6">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-6" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-6"><i
                              class="fas fa-plus-circle"></i>&nbsp;Do You Offer Refunds or Money Back? </button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-6" aria-labelledby="accordion-tab-1-heading-6"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>Sorry, We do not offer refunds or money back in online courses. Once you have paid for the course, You have to finish it in your own time. Although, if you have specific doubts then you can connect with Lakshit sir personally.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-7">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-7" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-7"><i class="fas fa-plus-circle"></i>&nbsp;Do you help in Resolving Doubts After Course? </button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-7" aria-labelledby="accordion-tab-1-heading-7"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>Yes, We will help you out throughout your startup journey. You can directly connect to Socialseller Team to resolve your doubts or Queries. You can email to us or in case you bigger doubts. We also help in preparing Business Model, You can take paid Consultation from Lakshit Sir.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-8">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-8" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-8"><i class="fas fa-plus-circle"></i>&nbsp;When will we get Certification Of Completion?
                             </button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-8" aria-labelledby="accordion-tab-1-heading-8"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>You will get Certification of Completion from your name signed by Lakshit Sir as soon as you finish the Video Course.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      @include('website.enrollinside')
      @include('website.feedback')
       @include('website.seller-testimonial')

      <section style="padding:4rem 0 4rem;background: #f3f9fd;">
         <div class="sectionheading text-center">
            <h3 class="dark-blue-text">Connect with the Best Online <br/>Product Suppliers
            </h3>
            <p class="subheading"> ऑनलाइन सप्लायर्स से सीधा जुड़े और बिज़नेस चालू करे
            </p>
         </div>
         <!--div class="container">
            <div class="mt-5">
               <div class="row justify-content-center">
               	<div class="col-md-10">
               		<div class="row">
	                  <span style="padding: 0px 10px;
	                  background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;
	                   border-radius: 50%;    z-index: 1;
	                   color: #ffff;    margin-bottom: -7px;">1</span>
                   </div>
               <div style="background: #f3fafd;
                    box-shadow: 0px 0px 13px 5px #f6f3f3f7; ">
                  <div class="row no-gutters justify-content-center">
                     <div class="col-md-7">
                           <h4><img class="img-circle" alt="" src="https://socialseller.co.in/assets/images/test5.png"
                           style="width: 50px;">
                           <b style="color: #09256d;">S.M Business</b> <span class="subheading">|</span> <span class="subheading"> Wholesellers</span>
                           </h4>
                         <div class="row no-gutters">
                             <div class="col-md-3 col-xs-6 col-6"><img src="images/product.jpg" class="img-fluid border-radius"> </div>
                             <div class="col-md-3 col-xs-6 col-6"><img src="images/product.jpg" class="img-fluid border-radius"> </div>
                             <div class="col-md-3 col-xs-6 col-6"><img src="images/product.jpg" class="img-fluid border-radius"> </div>
                             <div class="col-md-3 col-xs-6 col-6"><img src="images/product.jpg" class="img-fluid border-radius"> </div>

                         </div>
                     </div>
                     <div class="col-md-5">
                        <div  class="mt-2 text-center">
                           <h6><i>Services</i></h6>
                          <div class="row pt-2" >
                               <div class="col-md-6 col-6 text-center">
                             <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                              <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                              <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                               <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>

                          </div>
                          <div class="col-md-6 col-6">
                             <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                               <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                              <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                                <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                          </div>

                          </div>
                        </div>
                        <div class="col-md-12  p-1 pt-3">
                             <a href="#" class=" green-btn btn text-white ml-0" style=" border-radius: 5px !important; width: 100% !important"><img src="images/wht-w.png" width="20px;"> Connect</a>
                         </div>
                     </div>
                  </div>
               </div>
               	</div>
               </div>
            </div>



         </div-->
         <div class="container">
            <div class="row no-gutters justify-content-center">
               <!-- @foreach($suppliers as $row)
             <div class="col-12 col-md-4" data-aos="fade-up" data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg1 text-center">
                        <img class="img-fluid" src="{{ $row->image }}" />
                        <span class="badge badge-warning">{{ $row->category_name  }}</span>
                     </div>
                     <div class="p-1 coursedetails">
                        <span class="coursename1">{{ $row->name }}</span>

                        <div class="row justify-content-center">

                        	<div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                           <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                        </div>

                     </div>
                     <a href="https://api.whatsapp.com/send?l=en&phone={{ $row->mobile }}&text=Hi,I am intersted in social seller
" class=" green-btn btn text-white ml-0"><img src="{{ asset('socialseller/images/wht-w.png') }}" width="20px;"> Connect</a>
                  </div>
             </div>
             @endforeach -->


            </div>
<div class="row justify-content-center">
  @foreach($suppliers as $row)
	<div class="col-12 col-md-4">
					<div class="coursecard aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">
						<div class="p-3">
							<div class="row">
								<div class=" col-2">
									<div class="supller_logo">
										<img src="{{ asset('socialseller/images/Icon_2.png') }}" width="100%">
									</div>
								</div>
								<div class="col-10">
									<h5 class="mb-0"><strong>{{ $row->name }}</strong><small class="ml-2 text-muted"></small></h5>
									<div class="rating_b">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="courseimg">
                   @if($row->video_id)
                  <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{ $row->video_id }}" allowfullscreen style="border: 3px solid #fff;
    border-radius: 5px;"></iframe>
                  </div>
                  @else
							<img class="img-fluid" src="{{ $row->image }}" />
                     @endif
							<span class="badge badge-warning">{{ $row->category_name}}</span>
							<!-- <img class="img-fluid" src="{{ $row->image }}" />
							<span class="badge badge-warning">{{ $row->category_name }}</span> -->
						</div>
						<div class="p-2 coursedetails text-center">
							<h4><strong class="text-muted">Supplier Services</strong></h4>
							<div class="row justify-content-center">
                                                            <div class="col-md-12">
                                                                    <ul class="checklist pl-3 mt-0">
      @foreach($row->supplier_services as $row2)
             <li class="text-black">{{  $row2->service_name    }}</li>
     @endforeach
 </ul>
                                                             </div>
							</div>
						</div>
						 @if($row->is_free==1)
                  <a id="" class="btn text-white ml-0 btn-success" onclick='subscribe_free_supplier("{{ $row->id }}","{{ $row2->mobile }}")' style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a>
                  @else

                  @if((Session::get('student_id')))

                  <a  href="{{ url('buy_suppliers_details_request_generate').'/'.$row->id }}"  onclick="return confirm('Are you sure want to purchase this supplier ?')"  id="" class="btn text-white ml-0 btn-success"  style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a>

               @else
                  <a  href="javascript:void(0)" onclick="supplier_alert()" id="" class="btn text-white ml-0 btn-success"  style=" border-radius: 0 0 0.5rem 0.5rem !important;"><img src="{{ asset('socialseller/images/wht-w.png') }}" style="width: 20px;"> Connect</a>
               @endif
               @endif
					</div>
            </div>
            @endforeach
<!-- <div class="col-12 col-md-4">
					<div class="coursecard aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">
						<div class="p-3">
							<div class="row">
								<div class=" col-2">
									<div class="supller_logo">
										<img src="http://gyankhajana.com/socialseller/images/Icon_2.png" width="100%">
									</div>
								</div>
								<div class="col-10">
									<h5 class="mb-0"><strong>SM Accessories</strong><small class="ml-2 text-muted">Shoes</small></h5>
									<div class="rating_b">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="courseimg">
							<img class="img-fluid" src="http://gyankhajana.com/storage/course/1598781363.jpg" />
							<span class="badge badge-warning">Business</span>
						</div>
						<div class="p-2 coursedetails text-center">
							<h4><strong class="text-muted">Supplier Services</strong></h4>
							<div class="row justify-content-center">
								<div class="col-6">
									<button type="button" class="btn btn-secondary mb-2">Primary</button>
								</div>
								<div class="col-6">
									<button type="button" class="btn btn-secondary mb-2">Primary</button>
								</div>
								<div class="col-6">
									<button type="button" class="btn btn-secondary mb-2">Primary</button>
								</div>
								<div class="col-6">
									<button type="button" class="btn btn-secondary mb-2">Primary</button>
								</div>
							</div>
						</div>
						<a id="" class="btn text-white ml-0 btn-success" onclick="getWhatsApp()" style=" border-radius: 0 0 0.5rem 0.5rem !important;"><i class="fa fa-whatsapp" aria-hidden="true"></i> Connect</a>
					</div>
				</div>
<div class="col-12 col-md-4">
					<div class="coursecard aos-init aos-animate" data-aos="fade-up" data-aos-delay="0">
						<div class="p-3">
							<div class="row">
								<div class=" col-2">
									<div class="supller_logo">
										<img src="http://gyankhajana.com/socialseller/images/Icon_2.png" width="100%">
									</div>
								</div>
								<div class="col-10">
									<h5 class="mb-0"><strong>SM Accessories</strong><small class="ml-2 text-muted">Shoes</small></h5>
									<div class="rating_b">
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star checked"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="courseimg">
							<img class="img-fluid" src="http://gyankhajana.com/storage/course/1598781363.jpg" />
							<span class="badge badge-warning">Business</span>
						</div>
						<div class="p-2 coursedetails text-center">
							<h4><strong class="text-muted">Supplier Services</strong></h4>
							<div class="row justify-content-center">
								<div class="col-6">
									<button type="button" class="btn btn-secondary mb-2">Primary</button>
								</div>
								<div class="col-6">
									<button type="button" class="btn btn-secondary mb-2">Primary</button>
								</div>
								<div class="col-6">
									<button type="button" class="btn btn-secondary mb-2">Primary</button>
								</div>
								<div class="col-6">
									<button type="button" class="btn btn-secondary mb-2">Primary</button>
								</div>
							</div>
						</div>
						<a id="" class="btn text-white ml-0 btn-success" onclick="getWhatsApp()" style=" border-radius: 0 0 0.5rem 0.5rem !important;"><i class="fa fa-whatsapp" aria-hidden="true"></i> Connect</a>
					</div>
				</div> -->
</div>
            <div class="row justify-content-center">

                  <div class="col-md-12  p-4 text-center">
                          <a href="{{ url('all_suppliers') }}" class=" green-btn btn text-white ml-0" style=" border-radius: 5px !important;font-weight: 600 !important;font-size: 1.2rem !important; width:80% !important;"> GET SUPPLIERS DETAILS</a>
                 </div>

            </div>
         </div>
      </section>
   @include ('website.helping')

   @include ('website.consultation')
   <section style="background: #f8f9fa;">
    	<div class="container">
    		<div class="row justify-content-center">

    			<div class="col-md-4 col-2">
    				<img src="{{ asset('socialseller/images/Patrika.png') }}" style="width: 80%;">
    			</div>

    			<div class="col-md-4 col-2">
    				<img src="{{ asset('socialseller/images/inc.png') }}"  style="width: 80%;">
    			</div>
    			<div class="col-md-4 col-2">
    				<img src="{{ asset('socialseller/images/FM.png') }}"  style="width: 80%;">
    			</div>
    			<div class="col-md-4 col-2">
    				<img src="{{ asset('socialseller/images/EQ.png') }}"  style="width: 80%;">
    			</div>
                         <div class="col-md-4 col-2">
    				<img src="{{ asset('socialseller/images/HEAD.png') }}"  style="width: 80%;">
    			</div>
    		</div>
    	</div>
    </section>
   @include ('website.footer')
      <div class="bsnav-mobile">
         <div class="bsnav-mobile-overlay">
         <div class="navbar">
            <div class="masterbarmobile"></div>
         </div>
      </div>
   </div>

      @include ('website.footer-scripts')
      <script>
         function getWhatsApp()
         {
            var mobile=$('#whatsAppMobile').val();
            if(!mobile)
            {

                 $('#mobileError').show();
               $('#mobileError').html('Please enter your whats app number');
            }
            else if(mobile.length<9)
            {
                $('#mobileError').show();
               $('#mobileError').html('Please enter correct mobile number');
            }
            else
            {
            console.log(mobile);

               $.post("/add_whatsapp_mobile",
                  {
                     mobile:mobile,
                     _token: "{{ csrf_token() }}"
                  },
            function(response){
               $('#mobileError').hide();

               window.location.href = "https://api.whatsapp.com/send?l=en&phone=+918370044449&text=<?=$msg?>";

            });
            }
         }
          function supplier_alert()
      {
         alert('Please Login before purchase supplier')
          window.location.href="{{ url('login')  }}";

      }

      </script>
   </body>
</html>
