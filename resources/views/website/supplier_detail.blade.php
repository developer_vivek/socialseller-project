<!DOCTYPE html>
<html>

<head>
  <title>Socialseller Account - Supplier</title>@include('website.head')
  <style type="text/css">
    .profile-userpic img {
        max-width: 3rem;
        border-radius: 50%;
    }
    .abs-center-x {
        position: absolute;
        left: 50%;
        transform: translateX(-50%);
    }
    .profiledd {
        width: 10rem;
        right: 0rem;
        left: auto;
        float: right;
        border: none;
        border-radius: 0.5rem;
        box-shadow: 0 5px 30px rgba(0,0,0,0.1);
        margin-top: 1rem;
        -webkit-animation-duration: 0.5s;
        animation-duration: 0.5s;
        padding: 0;
        color: #000;
        font-size: 0.875rem;
        z-index: 9999;
    }
    .authpagecontent {
        background-color: #f5f7fc !important;
        padding: 10rem 0 5rem;
    }
    .fancynav .nav-link.active {
        background-color: #0054d1;
        color: #fff !important;
    }
    .form-control {
        display: block;
        width: 100%;
        padding: 31px;
        margin-top: 5px;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #6c757d !important;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
       border-radius: 10px !important;
    }
    .accountprofile .profilesidebar img {
        margin: 0 auto 2rem;
        width: 70%;
        border-radius: 50%;
        border: solid 8px rgba(0, 84, 209, 0.1);
        display: block;
    }
    .accountprofile .profilesidebar {
        border-right: solid 1px rgba(0, 84, 209, 0.07);
    }
    .card-account {
        border: none !important;
        background-color: #fff;
        border-radius: 0.5rem;
        min-height: 80vh;
        box-shadow: 0 0px 20px rgba(36, 102, 178, 0.1);
        padding: 2rem;
    }
    .accountprofile .btn-success {
        background-color: #00e133 !important;
        border-color: #00e133 !important;
        border: 1px solid #ccc !important;
        margin-top: 1rem;
    }
    .card11{
            border: 1px solid;
        border-radius: 2px;
        height: 150px;
        padding: 5px;
        margin-top: 10px;
        box-shadow: 0px 0px 5px 5px #cccccc47;
    }
    .courseprice{
        background-color: #fff653;
        display: inline-block;
        color: #000 !important;
        font-weight: 500;
        font-size: 1rem;
        margin-top: 0.5rem;
        float: left;
        padding:10px 10px;
    }
  </style>
</head>

<body>@include ('website.dashboard_header')
  <div class="authpagecontent">
    <div class="container">
      <div class="row text-center">
        <div class="col-md-12">
          <div class="mobileuinav">
            <ul class="nav nav-tabs fancynav justify-content-center">
              <li class="nav-item "> <a class="nav-link" href="{{ url('dashboard') }} "><i data-feather="home"></i>Courses</a>
              </li>

              <li class="nav-item active"> <a class="nav-link active" href="{{ url('suppliers') }}"><i data-feather="user"></i>Suppliers</a>
              </li>
              <li class="nav-item "> <a class="nav-link " href="{{ url('profile') }}"><i data-feather="user"></i>Profile</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="card card-account">
        <div class="row">
         <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">S.No.</th>
      <th scope="col">Name</th>
      <th scope="col"></th>

    </tr>
  </thead>
  <tbody>
    @foreach($supplierGroup as $key=>$row)
    <tr>
      <th scope="row">{{ ++$key }}</th>
      <td>{{ $row->name }}</td>
      <td><a id="" class="btn text-white ml-0 btn-success" onclick="getWhatsApp('{{ $row->mobile }}')" style=""><img src="https://www.socialseller.in/socialseller/images/wht-w.png" style="width: 20px;"> Connect</a></td>

    </tr>
    @endforeach
    <!-- <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td><a id="" class="btn text-white ml-0 btn-success" onclick="getWhatsApp()" style=""><img src="https://www.socialseller.in/socialseller/images/wht-w.png" style="width: 20px;"> Connect</a></td>

    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td><a id="" class="btn text-white ml-0 btn-success" onclick="getWhatsApp()" style=""><img src="https://www.socialseller.in/socialseller/images/wht-w.png" style="width: 20px;"> Connect</a></td>

    </tr> -->
  </tbody>
</table>

        </div>
      </div>
    </div>
  </div>
   <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar">
              <div class="masterbarmobile"></div>
            </div>

          </div>
    <script>
              function getWhatsApp(mobile)
                  {


                        window.location.href = "https://api.whatsapp.com/send?l=en&phone="+mobile+"&text=Hello";


                  }
            </script>
           @include('website.footer')
        @include('website.footer-scripts')
      </body>

</html>
