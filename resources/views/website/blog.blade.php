
<section style="padding:4rem 0 4rem;">
         <div class="section heading text-center">
            <h3 class="dark-blue-text">Top Blogs & Videos</h3>
            <p class="subheading">लक्षित सर द्वारा बनाई गई सारी वीडियोस देखे
            </p>
               <section >
<div class="container">
<div class="row  justify-content-center">
<div class="enq1">
<span class="enq1_btn_1">Subscribe To Our Youtube Channel</span>

</div>
</div>
</div>
</section>
         </div>
         <div class="container-fluid pt-5">
<div class="row justify-content-center">
 @foreach($blogs as $row)
<div class="col-md-4 mb-4">

<div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="{{ $row->url }}" allowfullscreen></iframe>
</div>
</div>
@endforeach
</div>
</div>

         </div>
</section>
