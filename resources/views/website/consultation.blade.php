 <section  style="padding:4rem 0 4rem;">
         <div class="container">
            <div class="row justify-content-center">
              <div class="col-md-6 text-center">
                  <img src="{{ asset('socialseller/images/consult.png')}} "
                     class="img-fluid "  data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500">
               </div>
               <div class="col-md-6 ">
                  <h1 class="dark-blue-text">
                     Take Professional Consultation
                  </h1>
                  <p>Are you caught up in the dilemma of what you like and what you dislike as a career option? Are you having difficulty in understanding what you should or shouldn't do? Our experienced career counsellors will show you the way to your dreams. Get in touch with us and clear this dilemma.</p>
                  <h5>Don’t worry, we are here to help!</h5>
                  <a href="{{url('appointment')}}" class=" green-btn btn text-white ml-0" style=" border-radius: 5px !important;font-size: 1.1rem !important;"> BOOK APPOINTMENT</a>
               </div>
            </div>
         </div>
      </section>
