@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
  <style>
   .error{
     color:red;
     font-style: italic;
   }
   </style>
@endsection

@section('content')
<div class="col-xs-12 col-md-5 col-lg-5">
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Add blog</h3>

   </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="{{ url('admin/add_blog')}}" method="post" id="form_validation" enctype="multipart/form-data">
            @csrf
            <div class="col-md-10">
                        <div class="form-group">
                            <label for="exampleInputEmail2">Url (Embed)</label>
                            <input type="url" name="url" required class="form-control" value="{{old('url')}}"  >
                            @error('url')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
            </div>
             <div class="col-md-10">
            <div class="form-group">
                    <label class="form-label">Thumbnail Image</label>
                    <input type="file" name="image"  class="form-control" required >
                 @error('image')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
             </div>
              <button type="submit" class="btn btn-primary btn-sm" >Create</button>
        </form>
    </div>
    <!-- /.card-body -->
</div>
</div>
<div class="col-xs-12 col-md-7 col-lg-7">
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Blog list</h3>

   </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table id="blog" class="table table-hover">
                <thead>
                <tr>
                        <th>Url</th>
                        <th>Thumbnail</th>
                        <th>Action</th>
                 </tr>
                </thead>
                <tbody>
                    @foreach($blogs as $key=>$row)
                    <tr>
                        <td>{{ $row->url   }}</td>
                        <td><img src="{{ $row->image }}" class="img-thumbnail"  ></td>

                        <td><button type="button" onclick="openModal('{{$row->id}}','{{ $row->url  }}')"  data-toggle="modal" data-target="#blogModal" class="btn btn-success">Edit</button>
                        <button type="button" onclick="deleteBlog('{{$row->id}}')"   class="btn btn-danger">Delete</button>

                      </td>
                    </tr>
                    @endforeach
                </tbody>
             </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
</div>
  <!-- Modal -->
<div class="modal fade" id="blogModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit blog</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name"  id="blog_name"  class="form-control">
          <input type="hidden" name="id"  id="blog_id"  class="form-control">
        </div>
     </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="editblog()">Update</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

  @section('scripts')

<script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>

<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<script src="{{ asset('admin_panel/dist/js/validate.js') }}"></script>
  <script>
    $(function () {
        $("#form_validation").validate();
      $('#blog').DataTable({
            // "processing": true,
            // "serverSide": true,
            "responsive":true,
              "aaSorting": [],

    })
    });
    function openModal(id,name)
    {
            $('#blog_id').val(id);
            $('#blog_name').val(name);
    }

    function editblog()
    {
        let id= $('#blog_id').val();
         let name=   $('#blog_name').val();
          $.post("/admin/edit_blog",
        {
          id: id,
          url: name,

          _token: "{{ csrf_token() }}"
        },
        function(response){
          // console.log(response);
          alert(response);

          location.reload();
        });
    }
function deleteBlog(id)
{
  // alert(id);
    swal({
        title: "Are you sure?",
        text:"Do you want to delete this blog"  ,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Change it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: true
      },
      function(isConfirm){
          if (isConfirm) {
              $(".confirm").attr('disabled', 'disabled');
      $.post("/admin/delete_blog",
        {
          id: id,
          _token: "{{ csrf_token() }}"
        },
        function(response){
          // console.log(response);
          if(response==1)
          {
             alert("suscessfully deleted");
          }
          else
          {
          alert(response);
          }

          location.reload();
        });
          }});
}
</script>

   @endsection
