@extends('admin_panel/partial.master')
@section('stylesheets')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
 <style>
   /* .widget-user-header
   {
     height:85px!important;
   }
   .widget-user-image
   {
     top:51px!important;
   }
.widget-user .widget-user-image>img {

    height: 90px!important;
    object-fit: cover!important;
} */

   </style>


@endsection
@section('content')






<div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <!-- <img class="profile-user-img img-fluid img-circle"
                       src="{{ url('storage/default.png') }}"
                       alt="User profile picture"> -->
                </div>

                <h3 class="profile-username text-center">{{ $adminUser->name }}</h3>

                <!-- <p class="text-muted text-center">Software Engineer</p> -->

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{ $adminUser->email }}</a>
                  </li>
                  <!-- <li class="list-group-item">
                    <b>Following</b> <a class="float-right">543</a>
                  </li>
                  <li class="list-group-item">
                    <b>Friends</b> <a class="float-right">13,287</a>
                  </li> -->
                </ul>


              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->


          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <!-- <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li> -->
                  <!-- <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Timeline</a></li> -->
                  <li class="nav-item"><a class="nav-link active" href="#password" data-toggle="tab">Change Password</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">


                  <!-- /.tab-pane -->

                  <div class="tab-pane active" id="password">
                    <form class="form-horizontal">
                        <div id="error"></div>
                      <div class="form-group row">
                        <label for="opass" class="col-sm-2 col-form-label">Old Password</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="opass" onfocusout="checkOldPassword()"  requireds placeholder="old password">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="npass" class="col-sm-2 col-form-label">New Password</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="npass" placeholder="New Password">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="cpass" class="col-sm-2 col-form-label">Confirm Password</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="cpass" placeholder="Confirm password">
                        </div>
                      </div>


                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="button" onclick="updatePassword()" class="btn btn-danger btnDisable">Update Password </button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
@endsection
@section('scripts')

<script>
 $(function () {
$('.btnDisable').prop('disabled',true);
 });
const updatePassword =()=>
{
    let cpass=$('#cpass').val();
    let opass=$('#opass').val();
    let npass=$('#npass').val();
  // console.log(npass.length);
    if(cpass=='' || opass=='' || npass=='')
    {
         $('#error').text('');
         $('#error').text('All Fields are mandatory');
         $('#error').css('color','red');
    }
    else if(cpass!=npass)
    {
        $('#error').text('');
        $('#error').text('new password and confirm password shold be same');
         $('#error').css('color','red');
    }
    else if(npass.length<5) {
        $('#error').text('');
         $('#error').text('Password length should be gretter than 5');
         $('#error').css('color','red');
    }
    else
    {

          $.ajax({
                      url: "{{ url('admin/update_password') }}",
                      type: "POST",
                      data:{new_password:npass,_token: "{{csrf_token()}}"},

                      success: function (data) {
                        // console.log('save='+data)
                        if(data==1)
                        {
                          $('#error').text('');
                            $('#error').text('Successfully password update');
                            $('#error').css('color','green');
                            $('#cpass').val('');
                        $('#opass').val('');
                        $('#npass').val('');
                        }
                        else
                        {

                        }
                      },
                  });
           }

}
let checkOldPassword=()=>{
     let opass=$('#opass').val();
     $.ajax({
        url: "{{ url('admin/check_password') }}",
        type: "POST",
        data:{old_password:opass,_token: "{{csrf_token()}}"},
         async:true,
        success: function (data) {
            // console.log(data)
          if(data==1)
          {
            $('#error').text('');
            $('.btnDisable').prop('disabled',false);
          }
          else
          {
             $('#error').text('old Password is not matched');
             $('#error').css('color','red');
             $('.btnDisable').prop('disabled',true);

                return 0;
          }
        },
     });
}

</script>

@endsection
