@extends('admin_panel/partial.master')

@section('stylesheets')
    <!-- <link rel="stylesheet" href="{{ asset('admin/bower_components/morris.js/morris.css') }}"> -->
    <style>
     .dropdown-menu> a{color:black!important;}
      </style>

@endsection

@section('content')



 <!-- <h2 class="page-header"> Graph Reports</h2> -->
 <div class="col-md-12">
   <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Customer Graph</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                   <div class="btn-group">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a href="#" onclick="customer_graph(1)" class="dropdown-item">This Year</a>
                      <a href="#" onclick="customer_graph(2)" class="dropdown-item">This Month</a>
                      <a href="#" onclick="customer_graph(3)" class="dropdown-item">Previous Month</a>
                      <!-- <a href="#" class="dropdown-item">Something else here</a>
                      <a class="dropdown-divider"></a>
                      <a href="#" class="dropdown-item">Separated link</a> -->
                    </div>
                  </div>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="barChart" style="height:230px; min-height:230px"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
    </div>
 </div>
  <div class="col-md-12">
   <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Captain Graph</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                   <div class="btn-group">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a href="javascript:void(0)" onclick="captain_graph(1)" class="dropdown-item">This Year</a>
                      <a href="javascript:void(0)" onclick="captain_graph(2)" class="dropdown-item">This Month</a>
                      <a href="javascript:void(0)" onclick="captain_graph(3)" class="dropdown-item">Previous Month</a>

                    </div>
                  </div>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="captainChart" style="height:230px; min-height:230px"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
    </div>
 </div>
<div class="col-md-12">
 <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Customer Android App Version</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>

                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="pieChart" style="height:230px; min-height:230px"></canvas>
              </div>
              <!-- /.card-body -->
  </div>
            <!-- /.card -->
</div>
<div class="col-md-12">
 <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Captain Android App Version</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>

                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="CaptainVersion" style="height:230px; min-height:230px"></canvas>
              </div>
              <!-- /.card-body -->
  </div>
            <!-- /.card -->
</div>















@endsection

@section('scripts')
    <!-- <script src="{{ asset('admin/bower_components/morris.js/morris.min.js') }} "></script>
    <script src="{{ asset('admin/bower_components/raphael/raphael.min.js') }} "></script>
    <script src="{{ asset('admin/bower_components/chart.js/Chart.js') }} "></script> -->

<!-- ChartJS -->
<script src="{{ asset('admin_panel/plugins/chart.js/Chart.min.js')}}"></script>

    <script>

  $(function () {

    customer_graph(0);
    captain_graph(0);
    captain_app_version_graph();
  })
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    // var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

function captain_graph(option)
{
    $.ajax({
        url: "{{ url('captain_graph') }}",
        type: "POST",
        data:{option:option,_token: "{{csrf_token()}}"},
        async:true,
        success: function (data) {
          // console.log(data);
            var obj=JSON.parse(data);
            // console.log(obj);
            var labelDate=[];
            var customerCount=[];
            for(var i=0;i<obj.length;i++)
            {
              labelDate.push(obj[i].date);
              customerCount.push(obj[i].count);
            }
            // console.log(labeldate);
    var areaChartData = {
      labels  : labelDate,
      datasets: [
        {
          label               : 'Captains',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : customerCount
        }

      ]
    }

    var barChartCanvas = $('#captainChart').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, areaChartData)
    // var temp0 = areaChartData.datasets[0]
    // var temp1 = areaChartData.datasets[1]
    // barChartData.datasets[0] = temp1
    barChartData.datasets[0] = areaChartData.datasets[0]

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })

        }
       });

}



 function customer_graph(option)
    {

       $.ajax({
        url: "{{ url('customer_graph') }}",
        type: "POST",
        data:{option:option,_token: "{{csrf_token()}}"},
        async:true,
        success: function (data) {
          // console.log(data);
            var obj=JSON.parse(data);
            console.log(obj);
            var labelDate=[];
            var customerCount=[];
            for(var i=0;i<obj.length;i++)
            {
              labelDate.push(obj[i].date);
              customerCount.push(obj[i].count);
            }
            // console.log(labeldate);
    var areaChartData = {
      labels  : labelDate,
      datasets: [
        {
          label               : 'Customers',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : customerCount
        }

      ]
    }

    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    // var temp1 = areaChartData.datasets[1]
    // barChartData.datasets[0] = temp1
    barChartData.datasets[0] = temp0

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })

   }
 });


var count       = [];
var app_version = [];
     <?php

foreach ($app_version_user_android as $row) {
    ?>

    count.push(<?=$row->count?>);
    app_version.push("<?=$row->app_version?>");
<?php
}
?>
    var donutData        = {
      labels:app_version ,
      datasets: [
        {
          data: count,
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        = donutData;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })


   }
function captain_app_version_graph()
{
  var count       = [];
  var app_version = [];
     <?php
foreach ($app_version_captain_android as $row) {
    ?>

    count.push(<?=$row->count?>);
    app_version.push("<?=$row->app_version?>");
<?php
}
?>
    var donutData        = {
      labels:app_version ,
      datasets: [
        {
          data: count,
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#CaptainVersion').get(0).getContext('2d')
    var pieData        = donutData;
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })
}




    </script>

@endsection
