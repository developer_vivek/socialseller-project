@extends('admin_panel/partial.master')
@section('stylesheets')
<style>

  a{
    text-decoration: none;
    color:black;
  }
</style>
@endsection
@section('content')



          <div class="col-12 col-sm-6 col-md-3">
            <a href="{{url('admin/registered_users')}}" >
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Registered User</span>
                <span class="info-box-number">
                    {{  $student_count
                      }}
               </span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </a>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <a href="{{url('admin/courses')}}" >
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Courses</span>
                <span class="info-box-number">{{  $course_count  }}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </a>
            <!-- /.info-box -->
          </div>
          <!-- <div class="col-12 col-sm-6 col-md-3">
            <a href="{{url('admin/enrolled_users')}}" >
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-motorcycle"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Enrolled Users</span>

              </div>

            </div>
          </a>

          </div> -->
          @if(Session::get('social_role_id')==1)

          <div class="col-12 col-sm-6 col-md-3">
             <a href="{{url('admin/payment_details/0')}}" >
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-rupee-sign"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Razorpay Paid</span>
                <span class="info-box-number"> {{ $razorpay_payment_count }}  </span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </a>
            <!-- /.info-box -->
          </div>
          <div class="col-12 col-sm-6 col-md-3">
             <a href="{{url('admin/payment_details/1')}}" >
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-rupee-sign"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Course Paid</span>
                <span class="info-box-number"> {{ $razorpay_payment_count_course }}  </span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </a>
            <!-- /.info-box -->
          </div>
          <div class="col-12 col-sm-6 col-md-3">
             <a href="{{url('admin/payment_details/2')}}" >
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-rupee-sign"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Supplier Paid</span>
                <span class="info-box-number"> {{ $razorpay_payment_count_supplier }}  </span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </a>
            <!-- /.info-box -->
          </div>
          <div class="col-12 col-sm-6 col-md-3">
             <a href="{{url('admin/payment_details/3')}}" >
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-rupee-sign"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Appointment Paid</span>
                <span class="info-box-number"> {{ $razorpay_payment_count_appointment }}  </span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </a>
            <!-- /.info-box -->
          </div>
          @endif

            <!-- <div class="col-md-12">
             <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Profit Graph</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                   <div class="btn-group">
                    <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fas fa-wrench"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" role="menu">
                      <a href="javascript:void(0)" onclick="profit_graph(1)" class="dropdown-item">This Year</a>
                      <a href="javascript:void(0)" onclick="profit_graph(2)" class="dropdown-item">This Month</a>


                    </div>
                  </div>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="captainChart" style="height:230px; min-height:230px"></canvas>
                </div>
              </div>

    </div>
 </div>

 -->











  @endsection

  @section('scripts')
  <script src="{{ asset('admin_panel/plugins/chart.js/Chart.min.js')}}"></script>
<script>

  $(function () {
    // profit_graph(2);
})
  function profit_graph(option)
{
    $.ajax({
        url: "{{ url('profit_graph_ajax') }}",
        type: "POST",
        data:{option:option,_token: "{{csrf_token()}}"},
        // async:true,
        success: function (data) {
          // console.log(data);
            var obj=JSON.parse(data);
            console.log(obj);
            var labelDate=[];
            var profit=[];
            var total_fare=[];
            for(var i=0;i<obj.length;i++)
            {
              labelDate.push(obj[i].date);
              profit.push(obj[i].profit);
              total_fare.push(obj[i].total_fare);
            }
            // console.log(labeldate);
    var areaChartData = {
      labels  : labelDate,
      datasets: [
        {
          label               : 'Total',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : total_fare
        },
         {
          label               : 'Profit',
          backgroundColor     : 'green',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#000',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : profit
        }


      ]
    }

    var barChartCanvas = $('#captainChart').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    // barChartData.datasets[0] = areaChartData.datasets[0]

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : true
    }

    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })

        }
       });

}

</script>
@endsection
