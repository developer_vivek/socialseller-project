@extends('admin_panel/partial.master')
@section('stylesheets')
 <link rel="stylesheet" href="{{  asset('admin_panel/plugins/summernote/summernote-bs4.css') }}">
 <link rel="stylesheet" href="{{  asset('admin_panel/plugins/select2/css/select2.min.css') }}">
 <style>
   .error{
     color:red;
     font-style: italic;
   }
   </style>
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="col-md-12">
   <div class="card">
           <div class="card-header">
             <h3 class="card-title">Add/Edit Lesson (course structure)</h3>
              <button type="button" id="add" class="btn btn-success" style="float:right">+</button>
           </div>
     <div class="card-body">

        <form action="{{ url('admin/modify_lesson') }}" method="post" enctype="multipart/form-data" id="form_validation" >
            @csrf

<div  id="dynamic_field">
      <div class="row">
  <input type="hidden" name="course_id" value="{{  $course_id}}">
        <?php
for ($i = 0; $i < count($lessons); $i++) {
    if ($i == 0) {
        ?>
                        <div class="col-md-6">
                        <div class="form-group">
                            <input type="hidden" name="l_id[]" value="{{  $lessons[$i]->id}}">
                        <label >Title </label>
                        <input type="text" name="l_title[]" required class="form-control l_title" value="{{ $lessons[$i]->title }}"  >
                        @error('l_title.0')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                        <label>Vimio video id <small>ex. 492832134</small></label>
                        <input type="text" name="l_url[]" required class="form-control" value="{{$lessons[$i]->url}}"  >
                        @error('l_url.0')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        </div>
                        <!-- <div class="col-md-2">
                        <label ></label>

                        </div> -->
                        <div class="col-md-6">
                        <textarea  name="l_description[]" class="form-control" required="required" placeholder="Place some text here" style="width:100%">{{$lessons[$i]->description}}</textarea>
                        </div>
                        <div class="col-md-4">
                        <input type="file" name="lesson_image[]" class="form-control" accept="image/*">
                        </div>
                        <div class="col-md-2">
                        @if($lessons[$i]->image)
                        <img src="{{ $lessons[$i]->image }}" width="100" height="100">
                        @endif
                        </div>
            <?php } else {?>
             <div class="col-md-6">
                  <input type="hidden" name="l_id[]" value="{{  $lessons[$i]->id}}">
                        <div class="form-group">
                        <label >Title </label>
                        <input type="text" name="l_title[]" required class="form-control l_title" value="{{ $lessons[$i]->title  }}"  >
                        @error('l_title.0')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                        <label>Vimio video id <small>ex. 492832134</small></label>
                        <input type="text" name="l_url[]" required class="form-control" value="{{$lessons[$i]->url}}"  >
                        @error('l_url.0')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        </div>

                        <div class="col-md-6">
                        <textarea  name="l_description[]" class="form-control" required="required" placeholder="Place some text here" style="width:100%">{{$lessons[$i]->description}}</textarea>
                        </div>
                        <div class="col-md-4">
                        <input type="file" name="lesson_image[]" class="form-control" accept="image/*">
                        </div>
                        <div class="col-md-2">

                        @if(isset($lessons[$i]->image))
                        <img src="{{ $lessons[$i]->image }}" width="100" height="100">
                        @endif
                        </div>
                        <?php }}?>



         </div>
        </div>
        <button type="submit" class="btn btn-success">Submit</button>
@endsection
@section('scripts')
<!-- <script src="{{ asset('admin_panel/plugins/summernote/summernote-bs4.min.js') }}"></script> -->
<script src="{{ asset('admin_panel/dist/js/validate.js') }}"></script>
<!-- <script src="{{ asset('admin_panel/plugins/select2/js/select2.full.min.js') }}"></script> -->
<script>
  $(function () {

  var i=1;

 $('#add').click(function(){
        var task = $("#task").val();
        i++;
        let data=`<div id="row${i}" class="row">
        <div class="col-md-6">
                  <div class="form-group">
                      <label >Title</label>
                      <input type="text" name="n_title[]" required class="form-control l_title" value="{{old('n_title.${i}')}}"  >
                        @error('n_title.${i}')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Vimio video id <small>ex. 492832134</small></label>
                    <input type="text" name="n_url[]" required class="form-control" value="{{old('n_url.${i}')}}"  >
                      @error('n_url.${i}')
                      <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                  </div>
            </div>
            <div class="col-md-2">
              <label ></label>
             <button type="button"  id="${i}" class="btn btn-danger btn_remove">X</button>
            </div>
            <div class="col-md-6">
               <textarea  name="n_description[]" class="form-control" required="required" placeholder="Place some text here" style="width:100%"
                      >{{old('n_description.${i}')}}</textarea>
            </div>
             <div class="col-md-6">
                <input type="file" required name="n_lesson_image[]" class="form-control" accept="image/*">
            </div>
            </div>`;
        $('#dynamic_field').append(data);
  });

$(document).on('click', '.btn_remove', function(){
      var button_id = $(this).attr("id");

      $('#row'+button_id+'').remove();
});

  });

$('form#form_validation').on('submit', function(event) {
        //Add validation rule for dynamically generated name fields
    $('.l_title').each(function() {
        $(this).rules("add",
            {
                required: true,
                messages: {
                    required: "Lesson title  is required",
                }
            });
    });
});
</script>

@endsection
