@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
@endsection

@section('content')

<div class="col-xs-12 col-md-12 col-lg-12">


<div class="card">
    <div class="card-header">
        <h3 class="card-title">Course list</h3>
         <a href="{{ url('admin/add_course')  }}" class="btn btn-success btn-sm" style="float:right">Add</a>
   </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table id="course" class="table table-bordered  table-hover">
                <thead>
                <tr>
                <!-- <th>id</th> -->
                <th>Title</th>
                <th>Langauge</th>
                <th>Category</th>
                <th>Author</th>
                <th>Total Price</th>
                <th>Discount Price</th>
                <th>Total Enrollment</th>
                <!-- <th>Total Lesson</th> -->
                <th>Is Free</th>
                <th>Is Certificate</th>
                <th>Status</th>

                <th>Created at</th>
                <!-- <th>Status</th> -->
                <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($courses as $key=>$row)
                    <tr>
                        <td>{{ $row->title   }}</td>
                        <td>{{ $row->langauge_name   }}</td>
                        <td>{{ $row->name   }}</td>
                        <td>{{ $row->author   }}</td>
                        <td>{{ $row->total_price   }}</td>
                        <td>{{ $row->discount_price   }}</td>
                        <td>{{ $row->total_enrollment   }}</td>
                        <!-- <td>{{ $row->total_modules   }}</td> -->
                        @if($row->is_free==1)
                         <td>Yes</td>
                         @else
                          <td>No</td>
                         @endif
                        @if($row->is_certificate==1)
                         <td>Yes</td>
                         @else
                          <td>No</td>
                         @endif
                        <td>{{  $row->status  }}</td>
                        <td>{{ $row->created_at   }}</td>
                        <td><a href="{{ url('admin/lessons/'.$row->id) }}" class="btn btn-info btn-xs"><small>Add/View Lesson</small></a>
                        <a href="{{ url('admin/course/edit/'.$row->id) }}" class="btn btn-warning btn-xs"><small>Edit</small></a>
                        @if($row->status=='Active')

                        <a onclick='change_status("{{ $row->id }}","Inactive")' class="btn btn-danger btn-xs"><small>Inactive</small></a>
                        @else
                           <a onclick='change_status("{{ $row->id }}","Active")' class="btn btn-success btn-xs"><small>Active</small></a>
                        @endif


                      </td>
                    </tr>
                    @endforeach
                </tbody>
             </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
</div>

@endsection

  @section('scripts')

<script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src=" {{ asset ('admin_panel/dist/js/myscript.js') }}"></script>
<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
  <script>
    $(function () {
      $('#course').DataTable({
            // "processing": true,
            // "serverSide": true,
            "responsive":true,
              "aaSorting": [],

    })
    });

    function change_status(course_id,status)
    {
       swal({
                title: "Are you sure?",
                text: "Do you want Change this status",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Verify it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true

            },
          function(isConfirm){
              if (isConfirm) {
                        $.ajax({
                                url:"{{ url('admin/change_status_course') }}",
                                method:"POST",
                                data: {course_id:course_id,status:status, _token: "{{ csrf_token() }}"},
                                // contentType: false,
                                cache:false,
                                // processData: false,
                                // dataType:"json",
                                success:function(data)
                                {
                                    console.log(data);
                                    if(data.status==0)
                                    {
                                            // $('.subCategoryError').text(data.error);
                                    }
                                    if(data.status==1)
                                    {
                                      location.reload();
                                    }
                                },
                            });

                        }
          });
    }

</script>

   @endsection
