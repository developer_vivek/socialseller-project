@extends('admin_panel/partial.master')
@section('stylesheets')
 <link rel="stylesheet" href="{{  asset('admin_panel/plugins/summernote/summernote-bs4.css') }}">
 <link rel="stylesheet" href="{{  asset('admin_panel/plugins/select2/css/select2.min.css') }}">
 <style>
   .error{
     color:red;
     font-style: italic;
   }
   </style>
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-md-12">
   <div class="card">
           <div class="card-header">
             <h3 class="card-title">Create Course</h3>
           </div>
     <div class="card-body">

        <form action="{{ url('admin/add_course') }}" method="post" enctype="multipart/form-data" id="form_validation" >
            @csrf
            <div class="row">
               <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail2">Title</label>
                         <input type="text" name="title" required class="form-control" value="{{old('title')}}"  >
                          @error('title')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                </div>
                 <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail2">Langauge</label>
                        <select name="langauge" class="form-control" required>
                         <option value="">--Select Langauge--</option>

                        @foreach($langauges as $row)
                         @if (old('langauge') == $row->id)
                         <option value="{{ $row->id }}" selected>{{ $row->name  }}</option>
                         @else
                             <option value="{{ $row->id }}">{{ $row->name  }}</option>
                          @endif
                        @endforeach
                         </select>

                          @error('langauge')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                </div>
               <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail2">Author</label>
                         <input type="text" name="author" required class="form-control" value="{{old('author')}}"  >
                          @error('author')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                </div>
               <div class="col-md-6">
                      <div class="form-group">
                        <label >Total Hours</label>
                         <input type="number" min="1" name="total_hours" required class="form-control" value="{{old('total_hours')}}"  >
                          @error('total_hours')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label>Who can learn from this course</label>
                  <select class="select2" name="who_can_learn_from_this_course[]" required multiple="multiple" data-placeholder="Who can learn from this course" style="width: 100%;">
                   @foreach($coursefor as $value)
                   <option value="{{ $value->id }}" {{ (old('who_can_learn_from_this_coursed') == $value ? "selected":"") }} >{{ $value->name }}</option>
                   @endforeach
                  </select>
                </div>
               </div>
                 <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail2">Category</label>
                          <select name="category" class="form-control" required>
                             <option value="">--Select Category--</option>
                        @foreach($categories as $row)

                        @if (old('category') == $row->id)
                         <option value="{{ $row->id }}" selected>{{ $row->name  }}</option>
                         @else
                         <option value="{{ $row->id }}">{{ $row->name  }}</option>
                          @endif
                          @endforeach
                         </select>
                         @error('category')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                </div>


              <div class="col-md-12">
                  <label>Description </label>
                      <textarea class="textarea" name="description"  required="required" placeholder="Place some text here"
                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd;padding: 10px;">{{old('description')}}</textarea>
                @error('description')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

              <div class="col-md-12">
                  <label>What you learn </label>
                      <textarea class="textarea" name="what_you_learn"  required="required" placeholder="Place some text here"
                    style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('what_you_learn')}}</textarea>
                @error('what_you_learn')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="col-md-12">
                <label>how this course help you?</label>
                    <textarea class="textarea" name="how_this_course_help_you" required="required" placeholder="Place some text here"
                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{old('how_this_course_help_you')}}</textarea>
              @error('how_this_course_help_you')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
              <div class="error how_this_course_help_you"></div>
           </div>
            <div class="col-md-12">
                <label>What will you get?</label>
                    <textarea class="textarea" name="what_will_you_get" required="required" placeholder="Place some text here"
                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" >{{old('what_will_you_get')}}</textarea>
                    @error('what_will_you_get')
              <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Total price</label>
                <input type="number"  required name="total_price" class="form-control"  min="1" value="{{ old('total_price') }}">
                @error('total_price')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Discount price</label>
                <input type="number"  required name="discount_price" class="form-control"  min="1" value="{{ old('discount_price') }}">
                @error('discount_price')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group ">
                <div class="col-sm-10">
                  <div class="form-check">
                    <input type="checkbox" name="is_certificate" value="1" class="form-check-input"  >
                    <label class="form-check-label" for="exampleCheck2">Is Certificate</label>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
                <div class="col-sm-10">
                  <div class="form-check">
                    <input type="checkbox" name="is_free" value="1" class="form-check-input" >
                    <label class="form-check-label" for="exampleCheck2">Is Free</label>
                  </div>
                </div>
            </div>
          </div>
           <div class="col-md-6">
            <div class="form-group">
                    <label class="form-label">Course Image</label>
                    <input type="file" name="course_image" required class="form-control" accept="image/*" >
                 @error('course_image')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
          </div>
           <div class="col-md-6">
                      <div class="form-group">
                        <label >Total Enrollment</label>
                         <input type="number" min="1" name="total_enrollment" required class="form-control" value="{{old('total_enrollments')}}"  >
                          @error('total_enrollments')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                </div>
           <div class="col-md-6">
            <div class="form-group">
                    <label class="form-label">video Thumbnail</label>
                    <input type="file" name="video_thumbnail" required class="form-control" accept="image/*" >
                 @error('video_thumbnail')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
          </div>
           <div class="col-md-6">
              <div class="form-group">
                  <label for="exampleInputEmail2">Course Url</label>
                  <input type="url" name="course_url" required class="form-control" value="{{old('course_url')}}"  >
                    @error('course_url')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
          </div>
            <div class="col-md-6">
                      <div class="form-group">
                        <label >Total Lesson</label>
                         <input type="number" min="1" name="total_lesson" required class="form-control" value="{{old('total_lesson')}}"  >
                          @error('total_lesson')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                </div>
            <div class="col-md-6">
                      <div class="form-group">
                        <label >Ratings<small>(out of 5)</small></label>
                         <input type="number" min="1" name="ratings" required class="form-control" value="{{old('ratings')}}"  >
                          @error('ratings')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                </div>
            </div>



      <button type="submit" class="btn btn-primary btn-sm">Create</button>
        </div>

       </div>
              <!-- /.card -->
</div>
     </form>
 </div>
</div>
 </div>


@endsection
@section('scripts')
<script src="{{ asset('admin_panel/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('admin_panel/dist/js/validate.js') }}"></script>
<script src="{{ asset('admin_panel/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
     $('.select2').select2()
  $('.textarea').summernote({
 toolbar: [
    // [groupName, [list of button]]
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']]
  ]


  });
  // $("#form_validation").validate();
  var i=1;

 $('#add').click(function(){
        var task = $("#task").val();
        i++;
        let data=`<div id="row${i}" class="row">
        <div class="col-md-5">
                  <div class="form-group">
                      <label >Title</label>
                      <input type="text" name="l_title[]" required class="form-control l_title" value="{{old('l_title.${i}')}}"  >
                        @error('l_title.${i}')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label>Url</label>
                    <input type="url" name="l_url[]" required class="form-control" value="{{old('l_url.${i}')}}"  >
                      @error('l_url.${i}')
                      <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                  </div>
            </div>
            <div class="col-md-2">
              <label ></label>
             <button type="button"  id="${i}" class="btn btn-danger btn_remove">X</button>
            </div>
            <div class="col-md-10">
               <textarea  name="l_description[]" class="form-control" required="required" placeholder="Place some text here" style="width:100%"
                      >{{old('l_description.${i}')}}</textarea>
            </div>
            </div>`;
        $('#dynamic_field').append(data);
  });

$(document).on('click', '.btn_remove', function(){
      var button_id = $(this).attr("id");

      $('#row'+button_id+'').remove();
});

  });

$('form#form_validation').on('submit', function(event) {
        //Add validation rule for dynamically generated name fields
    $('.l_title').each(function() {
        $(this).rules("add",
            {
                required: true,
                messages: {
                    required: "Lesson title  is required",
                }
            });
    });
    var a=$('[name="how_this_course_help_you"]').val()
    if(a=='')
    {
     $('.how_this_course_help_you').text("This field is required");
    }
    //Add validation rule for dynamically generated email fields
    // $('.email_input').each(function() {
    //     $(this).rules("add",
    //         {
    //             required: true,
    //             email: true,
    //             messages: {
    //                 required: "Email is required",
    //                 email: "Invalid email",
    //               }
    //         });
    // });
});


$("#form_validation").validate({
  rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      how_this_course_help_you: "required",
  }
});
</script>

@endsection
