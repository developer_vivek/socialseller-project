@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
@endsection

@section('content')

<div class="col-xs-12 col-md-12 col-lg-12">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Who can learn list</h3>
      <a href="{{ url('admin/who_can_learn/create')  }}" class="btn btn-success btn-sm" style="float:right">Add</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table id="coupans" class="table table-bordered table-hover">
                <thead>
                <tr>
                <th>Image</th>
                <th>Title</th>

                <!-- <th>Status</th> -->
                <!-- <th>Action</th> -->

                </tr>
                </thead>
                <tbody>
                    @foreach($course_for as $key=>$value)
                    <tr>
                        <td><img src="{{ $value->image}}" height="150" width="150"</td>
                        <td>{{ $value->name}}</td>

                        <!-- <td>{{ $value->status}}</td>
                        <td>
                        @if($value->status=='0')
                          <button onclick="changeStatus({{ $value->id }},1)"  class="btn btn-success btn-sm">Activate</button>
                        @else
                          <button onclick="changeStatus({{ $value->id }},0)" class="btn btn-danger btn-sm">Deactivate</button>
                        @endif
                         <button onclick="deleteBanner({{ $value->id }})" class="btn btn-danger btn-sm">Delete</button>

                        </td> -->


                    </tr>
                    @endforeach
                </tbody>
             </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
</div>

@endsection

  @section('scripts')

  <script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
  <script>
    $(function () {
      $('#coupans').DataTable();
      });

  function changeStatus(id,type)
  {
     swal({
        title: "Are you sure?",
        text: "Do you want to change status!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Change it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
          if (isConfirm) {
               $.getJSON('banner/change_status/'+id+'/'+type,function(res){
                 console.log(res);
                 swal("Success!", "Status Successfully changed!", "success");
                      location.reload();
                     });
          } else {
              swal("Cancelled", "Your Status is safe :)", "error");
          }
          });
  }
   function deleteBanner(id)
  {
     swal({
        title: "Are you sure?",
        text: "Do you want to delete banner!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
          if (isConfirm) {
               $.getJSON('banner/delete/'+id,function(res){
                 console.log(res);
                  swal("Success!", ""+res+"!", "success");
                  location . reload();

                      });
          } else {
              swal("Cancelled", "Your Banner is safe :)", "error");
          }
          });
  }



</script>

   @endsection
