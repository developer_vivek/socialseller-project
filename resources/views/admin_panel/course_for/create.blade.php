@extends('admin_panel/partial.master')

@section('content')


  <div class="col-md-6">
        <div class="card">
           <div class="card-header">
             <h3 class="card-title">Create Slider</h3>
           </div>
            <div class="card-body">
              @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
                <div class="col-md-9">
                    <form action="{{ url('admin/who_can_learn/create') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                        <label for="exampleInputEmail1">Name </label>
                        <input type="text" name='name' class="form-control" required  >
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Image</label>
                          <input type="file" name="image" required class="form-control"   >
                          @error('image')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                       <div class="form-group">
                         <button type="submit" class="btn btn-success">Submit</button>
                        </div>

@endsection
