@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css"> -->
@endsection

@section('content')

<div class="col-xs-12 col-md-12 col-lg-12">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Coupan list</h3>
      @if(check('coupan-add'))
      <a href="{{ url('coupan/create')  }}" class="btn btn-success btn-sm" style="float:right">Add</a>
      @endif
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table id="coupans" class="table  table-hover">
                <thead>
                <tr>
                <th>Title</th>
                <th>Coupan code</th>
                <th>Discount unit</th>
                <th>Discount value</th>
                <th>For First Ride</th>
                <th>Max. Discount Amount</th>
                <th>Start Time </th>
                <th>End Time</th>
                <!-- <th>Expire At</th> -->
                <th>Created at</th>
                <th>Status</th>
                <!-- <th>Action</th> -->
                </tr>
                </thead>
                <tbody>
                    @foreach($coupans as $key=>$value)
                    <tr>
                        <td>{{ $value->offer_title}}</td>
                        <td>{{ $value->coupan_code}}</td>
                        <td>{{ $value->discount_unit}}</td>
                        <td>{{ $value->discount_value}}</td>
                        <td>{{ $value->is_for_first_ride}}</td>
                        <td>{{ $value->maximum_discount_amount}}</td>
                        <td>{{ $value->start_time }}</td>
                        <td>{{ $value->end_time }}</td>
                        <!-- <td>{{ $value->expire_at }}</td> -->
                        <td>{{ $value->created_at }}</td>
                        <td>{{ $value->status }}</td>
                        <!-- <td>action</td> -->

                    </tr>
                    @endforeach
                </tbody>
             </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
</div>

@endsection

  @section('scripts')

  <script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
  <script>
    $(function () {
      $('#coupans').DataTable();
      });

  function blockUser(id)
  {
     swal({
        title: "Are you sure?",
        text: "Do you want to block this user!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Block it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
          if (isConfirm) {
               $.getJSON('/block_user/'+id+'/2',function(res){
                      swal("Success!", "User has successfully blocked!", "success");
                      });
          } else {
              swal("Cancelled", "Your User Status is safe :)", "error");
          }
          });
  }



</script>

   @endsection
