 <aside class="main-sidebar sidebar-light-warning elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{ asset('admin_panel/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">SocialSeller</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('admin_panel/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{ url('admin/user_profile') }}" class="d-block">{{  Session::get('social_user_name')   }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column text-sm nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

               <li class="nav-item ">
                 <a href="{{ url('admin/dashboard')  }}" class="nav-link {{ (request()->is('admin/dashboard')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-house-user"></i>
              <p>
                Dashboard

              </p>
            </a>
          </li>


          <!-- <li class="nav-item">
            <a href="{{ url('users')  }}" class="nav-link {{ (request()->is('users*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Customers

              </p>
            </a>
          </li> -->
          <li class="nav-item">
            <a href="{{ url('admin/courses')  }}" class="nav-link {{ (request()->is('admin/courses*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-book-reader"></i>
              <p>
                Courses

              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/registered_users')  }}" class="nav-link {{ (request()->is('admin/registered_users')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Registered Users

              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/categories')  }}" class="nav-link {{ (request()->is('admin/categories')) ? 'active' : '' }}">
              <i class="fas fa-circle nav-icon"></i>
              <p>
               Course Categories

              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/supplier_categories')  }}" class="nav-link {{ (request()->is('admin/supplier_categories')) ? 'active' : '' }}">
              <i class="fas fa-circle nav-icon"></i>
              <p>
               Supplier Categories

              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/enrolled_users')  }}" class="nav-link {{ (request()->is('admin/enrolled_users')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-male"></i>
              <p>
               Enrolled Users

              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/suppliers')  }}" class="nav-link {{ (request()->is('admin/suppiers')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-male"></i>
              <p>
              Suppliers

              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/services')  }}" class="nav-link {{ (request()->is('admin/services')) ? 'active' : '' }}">

              <i class="fas fa-circle nav-icon"></i>
              <p>
              Suppliers Services
             </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/blogs')  }}" class="nav-link {{ (request()->is('admin/blogs')) ? 'active' : '' }}">
              <i class="nav-icon fab fa-blogger"></i>
              <p>
              Blogs
             </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/whatsapp_record')  }}" class="nav-link {{ (request()->is('admin/whatsapp_record')) ? 'active' : '' }}">
              <i class="nav-icon fab fa-blogger"></i>
              <p>
              WhatsApp Record
             </p>
            </a>
          </li>
          @if(Session::get('social_role_id')==1)
          <li class="nav-item">
            <a href="{{ url('admin/payment_details/0')  }}" class="nav-link {{ (request()->is('admin/payment_details')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-money-bill-wave"></i>
              <p>
             Payment Details
             </p>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a href="{{ url('admin/appointments')  }}" class="nav-link {{ (request()->is('admin/appointments')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-money-bill-wave"></i>
              <p>
            Appointments
             </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/appointment_video')  }}" class="nav-link {{ (request()->is('admin/appointment_video')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-money-bill-wave"></i>
              <p>
            Appointments Videos
             </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/settings')  }}" class="nav-link {{ (request()->is('admin/settings')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-money-bill-wave"></i>
              <p>
             Settings
             </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('admin/seller_feedbacks')  }}" class="nav-link {{ (request()->is('admin/seller_feedbacks')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-money-bill-wave"></i>
              <p>
             Seller Feedbacks
             </p>
            </a>
          </li>

           <li class="nav-item">
            <a href="{{ url('admin/logout')  }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>







        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
