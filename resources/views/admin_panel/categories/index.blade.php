@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css"> -->
  <style>
   .error{
     color:red;
     font-style: italic;
   }
   </style>
@endsection

@section('content')
<div class="col-xs-12 col-md-5 col-lg-5">
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Add category</h3>

   </div>
    <!-- /.card-header -->
    <div class="card-body">
        <form action="{{ url('admin/add_category')}}" method="post" id="form_validation">
            @csrf
            <div class="col-md-10">
                        <div class="form-group">
                            <label for="exampleInputEmail2">Name</label>
                            <input type="text" name="name" required class="form-control" value="{{old('name')}}"  >
                            @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
            </div>
              <button type="submit" class="btn btn-primary btn-sm">Create</button>
        </form>
    </div>
    <!-- /.card-body -->
</div>
</div>
<div class="col-xs-12 col-md-7 col-lg-7">
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Category list</h3>

   </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table id="category" class="table  table-hover">
                <thead>
                <tr>
                        <th>Name</th>
                        <th>Action</th>
                 </tr>
                </thead>
                <tbody>
                    @foreach($categories as $key=>$row)
                    <tr>
                        <td>{{ $row->name   }}</td>

                        <td><button type="button" onclick="openModal('{{$row->id}}','{{ $row->name  }}')"  data-toggle="modal" data-target="#categoryModal" class="btn btn-success">Edit</button> </td>
                    </tr>
                    @endforeach
                </tbody>
             </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
</div>
  <!-- Modal -->
<div class="modal fade" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Name</label>
          <input type="text" name="name"  id="category_name"  class="form-control">
          <input type="hidden" name="id"  id="category_id"  class="form-control">
        </div>
     </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="add_incentive" onclick="editCategory()">Update</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

  @section('scripts')

<script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>

<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<script src="{{ asset('admin_panel/dist/js/validate.js') }}"></script>
  <script>
    $(function () {
        $("#form_validation").validate();
      $('#category').DataTable({
            // "processing": true,
            // "serverSide": true,
            "responsive":true,
              "aaSorting": [],

    })
    });
    function openModal(id,name)
    {
            $('#category_id').val(id);
            $('#category_name').val(name);
    }

    function editCategory()
    {
        let id= $('#category_id').val();
         let name=   $('#category_name').val();
          $.post("/admin/edit_category",
        {
          id: id,
          name: name,

          _token: "{{ csrf_token() }}"
        },
        function(response){
          // console.log(response);
          alert(response);
          $('#IncentiveModal').modal('hide');
          location.reload();
        });
    }

</script>

   @endsection
