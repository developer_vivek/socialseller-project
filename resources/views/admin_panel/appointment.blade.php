@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
@endsection

@section('content')



<div class="col-xs-12 col-md-12 col-lg-12">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Appointment list</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table id="users" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <!-- <th>Email</th> -->
                    <th>Mobile</th>
                    <!-- <th>Mobile Verified </th> -->
                    <th>Appointment time</th>

                    <th>Status</th>
                    <th>Created at</th>
                    <!-- <th>Action</th> -->
                </tr>
              </thead>
              <tbody>
                @foreach($appointment as $key=>$value)
                  <tr>
                      <td>{{ $value->name }}</td>
                      <!-- <td>{{ $value->email }}</td> -->
                      <td>{{ $value->mobile }}</td>

                      <td>{{ $value->appointment_time }}</td>
                      <td>{{ $value->status }}</td>
                      <td>{{ $value->created_at }}</td>
                      <!-- <td></td> -->

                  </tr>

              @endforeach
              </tbody>
             </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
</div>

@endsection

  @section('scripts')

<script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>

  <script>
    $(function () {
      $('#users').DataTable({
        "aaSorting":[],
      },

    );

  });
</script>

   @endsection
