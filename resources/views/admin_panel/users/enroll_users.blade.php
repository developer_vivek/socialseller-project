@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
@endsection

@section('content')

<div class="col-xs-12 col-md-12 col-lg-12">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Enrolled User list</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table id="users" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>S No</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Courses Enrolled</th>
                    <!-- <th>Mobile Verified </th> -->
                    <!-- <th>Gender</th> -->
                    <!-- <th>Course Name</th> -->
                    <!-- <th>created at</th> -->
                    <!-- <th>Status</th> -->
                    <!-- <th>Action</th> -->
                </tr>
              </thead>
              <tbody>
                @foreach($users as $key=>$value)
                  <tr>
                      <td>{{ ++$key }}</td>

                       <td><a href="{{ url('admin/user_details').'/'.$value->student_id }}" target="_blank">{{ $value->name }}</a></td>
                      <td>{{ $value->email }}</td>
                      <td>{{ $value->mobile }}</td>
                      <td>{{ $value->course_count }}</td>

                      <!-- <td>{{ $value->gender }}</td> -->
                      <!-- <td>{{ $value->course_name }}</td> -->
                      <!-- <td>{{ $value->created_at }}</td> -->
                      <!-- <td>{{ $value->status }}</td> -->
                      <!-- <td>{{ $value->status }}</td> -->
                      <!-- <td></td> -->

                  </tr>

              @endforeach
              </tbody>
             </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
</div>

@endsection

  @section('scripts')

<script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>

  <script>
    $(function () {
      $('#users').DataTable(

    );

  });
</script>

   @endsection
