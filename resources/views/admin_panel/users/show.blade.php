@extends('admin_panel/partial.master')
@section('stylesheets')
 <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
 <style>
   .widget-user-header
   {
     height:85px!important;
   }
   .widget-user-image
   {
     top:51px!important;
   }
.widget-user .widget-user-image>img {

    height: 90px!important;
    object-fit: cover!important;
}

   </style>


@endsection
@section('content')

<div class="col-md-12">


            <!-- Widget: user widget style 1 -->
            <div class="card card-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <div class="widget-user-header bg-warning">
                <h3 class="widget-user-username">{{ $data['user']->name  }}</h3>

              </div>
              <div class="widget-user-image">
                 @if(isset($data['user']->profile_image))
                <img class="img-circle elevation-2" src="{{ $data['user']->profile_image }}" alt="User Avatar">
                @else
                <img class="img-circle elevation-2" src="{{ url('storage/default.png') }}" alt="User profile">
               @endif

              </div>
              <div class="card-footer">
                <div class="row justify-content-around">
                  <div class="col-sm-2 border-right">
                    <div class="description-block">
                      <h5 class="description-header">{{ count($data['course_enroll']) }}</h5>
                      <span class="description-text">Total Course Enroll</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-2 border-right">
                    <div class="description-block">
                      <h5 class="description-header">{{ count($data['supplier_assign']) }}</h5>
                      <span class="description-text">Total Supplier</span>
                    </div>
                    <!-- /.description-block -->
                  </div>

                   <div class="col-sm-2">
                    <div class="description-block border-right">
                      <h5 class="description-header"> {{ count($data['appointments']) }}</h5>
                      <span class="description-text">Appointments</span>
                    </div>
                    <!-- /.description-block -->
                  </div>

              </div>
        </div>
              <!-- <div class="col-md-12" style="text-align:center" >
                <div class="col-md-12">
                  <a href="javascript:void(0);"  class="col-md-1 btn btn-primary" onclick='userMsg("{{$data['user']->id}}","/send_message");'><b>Send Message</b></a>
                </div>
             </div> -->


</div>

      </div>


  <div class="col-md-12">
     <div class="card">
        <div class="card-header p-2">
            <ul class="nav nav-pills ">
              <li class="nav-item active" ><a class="nav-link active" href="#details" data-toggle="tab">Details</a></li>
              <li class="nav-item" ><a class="nav-link" href="#courses" data-toggle="tab">Courses</a></li>
              <li class="nav-item" ><a class="nav-link" href="#suppliers" data-toggle="tab">Suppliers</a></li>
              <!-- <li class="nav-item" ><a class="nav-link" href="#appoinrments" data-toggle="tab">Appointments</a></li> -->

              <!-- <li class="nav-item"><a class="nav-link" href="#passbook" data-toggle="tab">Passbook</a></li> -->
            </ul>
        </div>
    <div class="card-body">
      <div class="tab-content">
          <div class="tab-pane active" id="details">
            <table class="table table-striped">
                <tr>
                  <th>Full name</th>
                   <td>{{ $data['user']->name }}</td>
                </tr>


                <tr>
                   <th>Email</th>
                  <td>{{ $data['user']->email }}</td>
                </tr>
                <tr>
                  <th>Mobile</th>
                  <td>{{ $data['user']->mobile }}</td>
                </tr>

                <tr>
                   <th>Status</th>
                  <td>{{ $data['user']->status }}</td>
                </tr>
                <tr>
                   <th>Joining Date</th>
                  <td>{{ $data['user']->created_at }}</td>
                </tr>
                 <tr>
                   <th>Otp</th>
                  <td>{{ $data['user']->otp }}</td>
                </tr>


            </table>
        </div>

    <div class="tab-pane" id="courses">
    <div class="float-right">

    <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#assignCourse">Assign Course</button>
    </div>
            <div class="table-responsive">
                <table id="package_order" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>S NO</th>
                            <th>Course name</th>

                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($data['course_enroll'] as  $key=> $row)
                            <tr>
                              <td>{{ ++$key }}</td>
                              <td>{{ $row->course_name }}</td>
                            </tr>
                            @endforeach
                    </tbody>
                 </table>
            </div>
        </div>
        <!-- /.tab-pane -->
         <div class="tab-pane" id="suppliers">
            <div class="table-responsive">
                <table id="rides_datatable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>S No</th>
                            <th>Supplier name</th>

                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody>
                       @foreach($data['supplier_assign'] as  $key=> $row)
                            <tr>
                              <td>{{ ++$key }}</td>
                              <td>{{ $row->name }}</td>
                            </tr>
                            @endforeach

                    </tbody>
                 </table>
            </div>
        </div>

        <div class="tab-pane" id="passbook">
            <div class="table-responsive">
                <table id="passbook_datatable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Order id</th>
                            <th>Razorpay payment id</th>
                            <th>Razorpay status</th>
                            <th>Amount(₹)</th>
                            <th>Service type</th>
                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($data['appointments'] as $key=>$value)
                        <tr>
                            <td><a href="{{ url('order_details').'/'.$value->order_id.'/package' }}">{{ $value->order_id }}</a></td>
                            <td>{{ $value->razorpay_payment_id }}</td>
                            <td>{{ $value->razorpay_status }}</td>
                            <td>{{ $value->amount }}</td>
                            <td>{{ $value->service_type }}</td>
                            <td>{{ $value->created_at }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                 </table>
            </div>
        </div>


      <!-- /.tab-content -->
    </div>
    <!-- /.nav-tabs-custom -->
  </div>
  <!-- /.col -->


   <!-- Modal start -->
  <div id="assignCourse" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <div class="modal-content">
        <div class="modal-header">
              <h4 class="modal-title">Assign Course</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">

        <div id="displayMessage"></div>
            <div class="form-group">
              <label>Course</label>
             <select id="courseId" class="form-control" required>
             <option value="">--select course--</option>
             @foreach($data['course'] as $row)
              <option value="{{ $row->id }}">{{ $row->title }}</option>
              @endforeach
             </select>
            </div>

            <button type="button" onclick="assignCourse()" class="btn btn-success submitButton">Submit</button>

        </div>
      </div>
    </div>
  </div>




<!-- {{-- Modal end --}} -->
  @endsection
  @section('scripts')
<script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<script>$(function () {


$('#package_order').DataTable({

             "aaSorting": [],

        });

// For RIdes server side datatable
$('#rides_datatable').DataTable({


             "aaSorting": [],


        });

    $('#passbook_datatable').DataTable({
          // "responsive":true,
            "aaSorting": [],

  });



    })
    function userMsg(id, url){
      $('#userId').val(id);
      $('#myModal').modal();
      $('#myModal form').attr('action', url);
    }
    function send_message()
    {
      let userId=$('#userId').val();
      let message=$('#message').val();
      if(message=='')
      {
         $('.message_modal').show();
         $('.message_modal').text('Message Field is required');
                     $('.message_modal').css('color','red');
                      $('.message_modal').fadeOut(5000);
      }
      else
      {
        $.ajax({
                  url: "{{ url('send_fcm_message') }}",
                  type: "POST",
                  data:{user_id:userId,message:message,_token: "{{csrf_token()}}"},

                  success: function (data) {
                    // console.log('save='+data)
                    if(data==1)
                    {
                      $('.message_modal').show();
                      $('.message_modal').text('Successfully message send');
                      $('.message_modal').css('color','green');
                      $('#userId').val('');
                      $('#message').val('');
                      $('.message_modal').fadeOut(5000);
                    }
                    else
                    {
                      $('.message_modal').text('Message Not send');
                       $('.message_modal').css('color','red');
                    }

                  },
              });
        }

    }
    function assignCourse()
    {
      let userId="{{ $data['user']->id }}";
      let courseId=$("#courseId").val();
      if(!courseId)
      {
        alert("Please Select any course");
        return false;
      }
      $('.submitButton').attr('disabled',true);

        $.ajax({
                  url: "{{ url('admin/assign_course_to_student') }}",
                  type: "POST",
                  data:{student_id:userId,course_id:courseId,_token: "{{csrf_token()}}"},

                  success: function (data) {
                    // console.log('save='+data)
                    console.log(data);
                    console.log(data.status);

                    if(data.status==1)
                    {

                      $('#displayMessage').text(data.message);
                      $('#displayMessage').css("color","green");
                      setTimeout(function () {
                        location.reload();
                      }, 2000);


                    }
                    else
                    {
                      $('#displayMessage').text(data.message);
                      $('#displayMessage').css("color","red");
                      $('.submitButton').attr('disabled',false);

                    }

                  },
              });


    }





    </script>



  @endsection
