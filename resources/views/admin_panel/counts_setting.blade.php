@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css"> -->
  <style>
   .error{
     color:red;
     font-style: italic;
   }
   </style>
@endsection

@section('content')
<div class="col-xs-12 col-md-12 col-lg-12">
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Website counts</h3>

   </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(Session::has('message'))
                 <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
             @endif
        <form action="{{ url('admin/settings')}}" method="post" id="form_validation">
            @csrf
    <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                        </label>Satisfied Customers</label>
                                <input type="text"  required name="satisfied_customers" class="form-control"   value="{{ $counts->satisfied_customers }}">
                                @error('satisfied_customers')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Complete Projects</label>
                                <input type="text"  required name="complete_projects" class="form-control"   value="{{ $counts->complete_projects }}">
                                @error('complete_projects')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Cup of coffee</label>
                                <input type="text"  required name="cup_of_coffee" class="form-control"   value="{{ $counts->cup_of_coffee }}">
                                @error('cup_of_coffee')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Award_won</label>
                                <input type="text"  required name="award_won" class="form-control"   value="{{ $counts->award_won }}">
                                @error('award_won')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Suppliers</label>
                                <input type="text"  required name="suppliers" class="form-control"   value="{{ $counts->suppliers }}">
                                @error('suppliers')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Resellers</label>
                                <input type="text"  required name="resellers" class="form-control"   value="{{ $counts->resellers }}">
                                @error('resellers')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Students</label>
                                <input type="text"  required name="students" class="form-control"   value="{{ $counts->students }}">
                                @error('students')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Connects</label>
                                <input type="text"  required name="connects" class="form-control"   value="{{ $counts->connects }}">
                                @error('connects')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Views</label>
                                <input type="text"  required name="views" class="form-control"   value="{{ $counts->views }}">
                                @error('views')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Subscriber</label>
                                <input type="text"  required name="subscriber" class="form-control"   value="{{ $counts->subscriber }}">
                                @error('subscriber')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Followers</label>
                                <input type="text"  required name="followers" class="form-control"   value="{{ $counts->followers }}">
                                @error('followers')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                        </label>Shares</label>
                                <input type="text"  required name="shares" class="form-control"   value="{{ $counts->shares }}">
                                @error('shares')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                 </div>
            </div>
    </div>
           <button type="submit" class="btn btn-success">Update</button>
    </div>
        </form>
</div>
</div>

 @endsection
 @section('scripts')

<script src="{{ asset('admin_panel/dist/js/validate.js') }}"></script>
<script>
  $(function () {
$("#form_validation").validate();
  });
</script>
 @endsection
