@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
   <!-- daterange picker -->
  <link rel="stylesheet" href="{{ asset('admin_panel/plugins/daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')

<div class="col-xs-12 col-md-12 col-lg-12">
<div class="card">
<div class="card-body">
 <form action="{{ url('admin/export_csv') }}" method="post">
 @csrf
             <div class="row">
                  <div class="col-md-6">
                  <div class="input-group">
                  <span class="input-group-text">Date Range</span>
                    <input type="text" class="form-control" id="daterange" value="" name="date_range" autocomplete="off" required>
                  </div>
                    </div>


                  <div class="col-md-3">
                    <button type="submit"  class="btn btn-info">Export</button>
                  </div>

        </form>
</div>
    <div class="card-header">
      <h3 class="card-title">Whatsapp User list</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table id="users" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>S. NO</th>
                    <th>Mobile</th>
                    <th>Whats app link </th>
                    <th>Created at</th>
                </tr>
              </thead>
              <tbody>
              <?php $count = count($data);?>
                @foreach($data as $key=>$value)
                  <tr>
                    <td>{{ $count--   }}</td>
                      <td>{{ $value->mobile }}</td>

                      <td><a href="https://api.whatsapp.com/send?l=en&phone=+91{{ $value->mobile }}&text=Hi,I am intersted in social seller" target="_blank">Access Whats app</a>
</td>
                      <td>{{ $value->created_at }}</td>


                  </tr>

              @endforeach
              </tbody>
             </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
</div>

@endsection

  @section('scripts')

<script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<!-- date-range-picker -->
<script src="{{ asset('admin_panel/plugins/daterangepicker/daterangepicker.js') }}"></script>

  <script>
    $(function () {
      $('#users').DataTable({
        "aaSorting":[]
      }
    );


    //  $('#daterange').daterangepicker();
    $('#daterange').daterangepicker(
    {
      locale: {
            format: 'DD/MM/YYYY'
        },
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate: moment(),
      // format:'DD/MM/YYYY'
    },
    function (start, end) {
      $('#daterange').val(start.format('MM/DD/YYYY') + '-' + end.format('DD/MM/YYYY'));
    }
    );
  });

  function exportCsv()
  {
      // let daterange=$('#daterange').val();
      let daterange=$('#hidden_daterange').val();
      if(daterange=='')
      {
            daterange=$('#daterange').val();
      }
      console.log(daterange);

     let res = daterange.replace(" ","");
      res = res.replace(" ","");

      console.log(res);
      res="";
      window.location="{{ url('export_csv') }}?date_range="+res;


  }

</script>

   @endsection
