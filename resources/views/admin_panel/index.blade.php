@extends('admin_panel/partial.master')
@section('stylesheets')
<!-- !-- Select2 -->

@endsection
@section('content')


  <div class="col-md-6">
        <div class="card">
           <div class="card-header">
             <h3 class="card-title">Send Notification</h3>
           </div>
            <div class="card-body">
              @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
                <div class="col-md-9">
                    <form action="{{ url('send_notification') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label >Notification For</label>
                            <select name="app" required class="form-control ">
                                <option value="" >---Select--</option>
                                <option value="1" >Customer App</option>
                                <option value="2" >Partner App</option>
                            </select>
                             @error('app')
                             <div class="alert alert-danger">{{ $message }}</div>
                             @enderror
                         </div>
                        <div class="form-group">
                            <label >Image url(optional)</label>
                            <input type="text" name="image_url" required class="form-control" value="{{ old('image_url')}}"  >
                            @error('image_url')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label >Message</label>
                           <textarea name="message">{{ old('message')}}</textarea>
                            @error('message')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                    <button type="submit" class="btn btn-primary btn-sm">send</button>
                  </form>
              </div>
            </div>
              <!-- /.card -->
           </div>
        </div>



@endsection
@section('scripts')



@endsection
