@extends('admin_panel/partial.master')
@section('stylesheets')
 <!-- summernote -->
  <link rel="stylesheet" href="{{  asset('admin_panel/plugins/summernote/summernote-bs4.css') }}">

@endsection
@section('content')


        <div class="col-md-12">
          <!-- Custom Tabs -->
           <div class="card">
              <div class="card-header p-2">
          <div class="nav-tabs-custom">
            <ul class="nav nav-pills ">
                  <li class="nav-item" ><a class="nav-link active" href="#setting" data-toggle="tab">General</a></li>
                  <li class="nav-item" ><a class="nav-link" href="#terms" data-toggle="tab">Terms and Condition</a></li>
            </ul>
          </div>
          <div class="card-body">
              @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                @endif
            <div class="tab-content">
              <div class="tab-pane active" id="setting">

                    <form action="{{ url('settings') }}" method="post">
                        @csrf
                        <div class="col-md-6">
                      <!-- general form elements -->
                              <input type="hidden" name="id" value="{{$settings->id}}" >
                            <div class="form-group">
                              <label for="exampleInputEmail1">Base Fare (₹)</label>
                              <input type="number" name="base_fare" required class="form-control" id="exampleInputEmail1" min="1" value="{{ $settings->base_fare }}">
                              @error('base_fare')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Per Km fare Captain (₹) </label>
                              <input type="number" name="per_km_fare_captain" class="form-control" id="exampleInputEmail1" min="1" value="{{ $settings->per_km_fare_captain }}">
                              @error('per_km_fare_captain')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Per Km fare customer (₹)</label>
                              <input type="number" name="per_km_fare_customer" class="form-control" id="exampleInputEmail1" min="1" value="{{ $settings->per_km_fare_customer }}">
                              @error('per_km_fare_customer')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Per Minute fare (₹)</label>
                              <input type="number" name="per_minute_fare" class="form-control" id="exampleInputEmail1" min="1" value="{{ $settings->per_minute_fare }}">
                              @error('per_minute_fare')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Referral Bonus (₹) </label>
                              <input type="number" name="referral_bonus" class="form-control" id="exampleInputEmail1" min="1" value="{{ $settings->referral_bonus }}">
                              @error('referral_bonus')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Tax(%)</label>
                              <input type="number" name="tax" class="form-control" id="exampleInputEmail1" min="1" max="100" value="{{ $settings->tax }}">
                              @error('tax')
                              <div class="alert alert-danger">{{ $message }}</div>
                              @enderror
                            </div>
                      <!-- /.card-body -->
                              <button type="submit" class="btn btn-primary btn-sm">Update</button>
                        </div>
              <!-- /.card -->
                </form>
           </div>
            <!-- /.tab-content -->
                <div class="tab-pane" id="terms">

                   <div class="card">
                    <div class="card-header p-2">
                      <h3 class="card-title">
                      Partner App
                    </h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('update_terms_and_condition') }}" method="post">
                          @csrf
                            <div class="col-md-12">
                                <textarea class="textarea" name="term_partner_app" placeholder="Place some text here"
                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ @$extra[0]->text }}</textarea>
                            </div>
                            <div class="col-md-12">
                              <button type="submit" class="btn btn-primary btn-sm">Update</button>
                            </div>

                      </form>
                    </div>
                   </div>
                </div>
            </div>
          </div>
          <!-- nav-tabs-custom -->
        </div>
              </div>


@endsection
@section('scripts')
<!-- Summernote -->
<script src="{{ asset('admin_panel/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script>
 $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>

@endsection
