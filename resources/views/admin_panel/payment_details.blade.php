@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
@endsection

@section('content')

<div class="col-xs-12 col-md-12 col-lg-12">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Payment list</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
                <table id="payment_datatable" class="table table-bordered table-hover">
                    <thead>
                        <tr>

                            <th>Product</th>
                            <th> Customer</th>
                            <th>Razorpay payment id</th>
                            <th>Razorpay status</th>
                            <th>Amount(₹)</th>

                            <th>Created at</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($payment_details as $key=>$value)
                        <tr>
                            <td>{{ $value->course_name }} </td>
                            <td>{{ $value->user_name }} </td>
                            <td>{{ $value->razorpay_payment_id }}</td>
                            <td>{{ $value->razorpay_status }}</td>
                            <td>{{ $value->amount }}</td>

                            <td>{{ $value->created_at }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                 </table>

        </div>
    </div>
    <!-- /.card-body -->
</div>
</div>

@endsection

  @section('scripts')

<script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>

  <script>
    $(function () {
      $('#payment_datatable').DataTable({
        "aaSorting":[],
      }

    );

  });
</script>

   @endsection
