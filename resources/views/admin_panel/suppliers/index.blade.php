@extends('admin_panel/partial.master')

@section('stylesheets')
 <!-- DataTables -->

 <link rel="stylesheet" href="{{ asset('admin_panel/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
@endsection

@section('content')

<div class="col-xs-12 col-md-12 col-lg-12">
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Suppliers list</h3>
       <a href="{{ url('admin/add_supplier')  }}" class="btn btn-success btn-sm" style="float:right">Add</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="table-responsive">
            <table id="suppliers" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Name</th>
                    <!-- <th>Email</th> -->
                    <!-- <th>Mobile</th> -->
                    <!-- <th>Amount</th> -->
                    <th>Image</th>
                    <!-- <th>Service</th> -->
                    <th>Is Free</th>
                    <!-- <th>Validity_in_days</th> -->
                    <!-- <th>Created at</th> -->
                    <th>Status</th>
                    <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($suppliers as $key=>$value)
                  <tr>
                      <td>{{ $value->name }}</td>
                      <!-- <td>{{ $value->email }}</td> -->
                      <!-- <td>{{ $value->mobile }}</td> -->
                      <!-- <td>{{ $value->amount }}</td> -->
                      <td>
                        @if($value->image)
                        <img src="{{ $value->image }}" width="200" height="150" class="img-thumbnail">
                        @endif
                      </td>
                      <!-- <td>{{ $value->services }}</td> -->
                      <td>
                        @if($value->is_free==1)
                          Yes
                        @else
                        No
                        @endif
                       </td>

                      <td>{{ $value->status }}</td>
                      <!-- <td>{{ $value->status }}</td> -->
                      <td><a href="{{ url('admin/edit_supplier').'/'.$value->id   }}" class="btn btn-warning btn-xs">Edit</a>
                      @if($value->status=='Active')

                        <button type="button" onclick='change_status("{{ $value->id }}","Inactive")' class="btn btn-danger btn-xs"><small>Inactive</small></button>
                        @else
                           <button type="button" onclick='change_status("{{ $value->id }}","Active")' class="btn btn-success btn-xs"><small>Active</small></button>
                        @endif


                    </td>

                  </tr>

              @endforeach
              </tbody>
             </table>
        </div>
    </div>
    <!-- /.card-body -->
</div>
</div>

@endsection

  @section('scripts')

<script src=" {{ asset ('admin_panel/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset ('admin_panel/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>

  <script>
    $(function () {
      $('#suppliers').DataTable(

    );

  });
   function change_status(supplier_id,status)
    {
       swal({
                title: "Are you sure?",
                text: "Do you want Change this status",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Verify it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true

            },
          function(isConfirm){
              if (isConfirm) {
                        $.ajax({
                                url:"{{ url('admin/change_status_supplier') }}",
                                method:"POST",
                                data: {supplier_id:supplier_id,status:status, _token: "{{ csrf_token() }}"},
                                // contentType: false,
                                cache:false,
                                // processData: false,
                                // dataType:"json",
                                success:function(data)
                                {
                                    console.log(data);
                                    if(data.status==0)
                                    {
                                            // $('.subCategoryError').text(data.error);
                                    }
                                    if(data.status==1)
                                    {
                                      location.reload();
                                    }
                                },
                            });

                        }
          });
    }




</script>

   @endsection
