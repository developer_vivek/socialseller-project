@extends('admin_panel/partial.master')
@section('stylesheets')
<link rel="stylesheet" href="{{  asset('admin_panel/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">
 <style>
   .error{
     color:red;
     font-style: italic;
   }
   </style>
@endsection
@section('content')


  <div class="col-md-12">
        <div class="card">
           <div class="card-header">
             <h3 class="card-title">Update Supplier</h3>
           </div>
            <div class="card-body">
              @if(Session::has('message'))
                  <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
              <form action="{{ url('admin/update_supplier') }}" method="post" id="form_validation" enctype="multipart/form-data">
                        @csrf
                <input type="hidden" name="supplier_id" value="{{ $suppliers->id }}">
                     <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail2">Name</label>
                         <input type="text" name="name" required class="form-control"  value="{{ $suppliers->name }}"  >
                          @error('name')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                    </div>
                     <!-- <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Whats app Number</label>
                        <input type="number" name="mobile" required class="form-control"  value="{{  $suppliers->mobile  }}">
                        @error('mobile')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>
                     </div>
                     <div class="col-md-6">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" name="email" class="form-control"  value="{{ $suppliers->email  }}">
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>
                     </div> -->

                     <div class="col-md-6">
                                <div class="form-group">
                                    <label>Services</label>
                                <select class="select2" name="services[]" required multiple="multiple" data-placeholder="services" style="width: 100%;">
                                @foreach($services as $value)
                                    @if(in_array($value->id, $supplier_services))
                                    <option value="{{ $value->id }}" selected="true"  >{{ $value->name }}</option>
                                    @else
                                        <option value="{{ $value->id }}"  >{{ $value->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                </div>
                     </div>
                    <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail2">Category</label>
                                <select name="category" class="form-control" required>
                                    <option value="">--Select Category--</option>
                                @foreach($categories as $row)

                                @if ($suppliers->category == $row->id)
                                <option value="{{ $row->id }}" selected>{{ $row->name  }}</option>
                                @else
                                <option value="{{ $row->id }}">{{ $row->name  }}</option>
                                @endif
                                @endforeach
                                    </select>
                                    @error('category')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                                <label class="form-label">Image</label>
                                <input type="file" name="image" accept="image/*" class="form-control" >
                            @error('image')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <img src="{{  $suppliers->image }}" class="img-thumbnail" height="120" width="120">
                    </div>
                    <div class="col-md-2">
                                <div class="form-group">
                                    <div class="col-sm-10">
                                      <div class="form-check">
                                        <input type="checkbox" name="is_free" value="1" class="form-check-input is_free" >
                                        <label class="form-check-label" for="exampleCheck2">Is Free</label>
                                      </div>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-3">

                          <button type="button" class="btn btn-success" id="add">
                                Add More
                            </button>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group display">
                                <label >Amount</label>
                                <input type="number"  required name="amount" class="form-control validity_in_days"  min="0" value="{{ $suppliers->amount }}">
                                @error('amount')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                         </div>
                           <div class="col-md-5">
                      <div class="form-group">
                        <label >Video url<small>(embed optional if image is not upload )</small></label>
                         <input type="text" name="video_id" required class="form-control"  value="{{ $suppliers->video_id }}"  >
                          @error('video_id')
                          <div class="alert alert-danger">{{ $message }}</div>
                          @enderror
                      </div>
                    </div>

                      <!-- <div class="row"> -->
                        @foreach($supplier_person as $row)
                        <input name="supplier_groups_id[]" type="hidden" value="{{  $row->id }} ">
                            <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="exampleInputEmail2">Supplier Name</label>
                                        <input type="text" name="supplier_name[]" required class="form-control"  value="{{ $row->name  }}"  >
                                          @error('supplier_name')
                                          <div class="alert alert-danger">{{ $message }}</div>
                                          @enderror
                                    </div>
                            </div>
                            <div class="col-md-5">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Whats app Number</label>
                                <input type="number" name="mobile[]" required class="form-control"  value="{{ $row->mobile  }}">
                                @error('mobile')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                                </div>
                            </div>
                            <div class="col-md-2">
                                  <div class="form-group">
                                      <button type="button"  onclick='delete_supplier_group("{{  $row->supplier_id }}","{{ $row->id }}" )' class="btn btn-danger">-</button>
                                  </div>
                            </div>
                        @endforeach

                        <!-- </div> -->
                      </div>
                       <div  id="dynamic_field">
                         <div class="row">

                         </div>
                    </div>










                            <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                            </div>
                        </div>
        </div>
              <!-- /.card -->
                </form>
           </div>

        </div>
              </div>


@endsection
@section('scripts')
<script src="{{ asset('admin_panel/dist/js/validate.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
<script src="{{ asset('admin_panel/plugins/select2/js/select2.full.min.js') }}"></script>
<script>
  $(function () {
    var i=1;
     $('.select2').select2()
      $('.is_free').click(function(){
            if($(this).prop("checked") == true){
               $('.display_total_price').prop('disabled',true);
            }
            else if($(this).prop("checked") == false){
                $('.display_total_price').prop('disabled',false);

            }
        });
      $('.is_lifetime_access').click(function(){
            if($(this).prop("checked") == true){
               $('.validity_in_days').prop('disabled',true);
            }
            else if($(this).prop("checked") == false){
                $('.validity_in_days').prop('disabled',false);

            }
        });
  $('#add').click(function(){

   i++;
  var data=`<div class="row" id="row${i}">
                            <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="exampleInputEmail2">Supplier Name</label>
                                        <input type="text" name="supplier_name_s[]" required class="form-control"  value="{{old('supplier_name_s')}}"  >
                                          @error('supplier_name')
                                          <div class="alert alert-danger">{{ $message }}</div>
                                          @enderror
                                     </div>
                            </div>
                            <div class="col-md-5">
                               <div class="form-group">
                                 <label for="exampleInputEmail1">Whats app Number</label>
                                 <input type="number" name="mobile_s[]" required class="form-control"  value="{{ old('mobile_s') }}">
                                 @error('mobile')
                                 <div class="alert alert-danger">{{ $message }}</div>
                                 @enderror
                                </div>
                              </div>
                            <div class="col-md-2">
                                    <div class="form-group">
                                        <button type="button" id="${i}" class="btn btn-danger btn_remove">-</button>
                                      </div>
                            </div>

                </div>`;


                 $('#dynamic_field').append(data);
  });

$(document).on('click', '.btn_remove', function(){
      var button_id = $(this).attr("id");
  console.log(button_id);
      $('#row'+button_id+'').remove();
});

  });
// $("#form_validation").validate();


function delete_supplier_group(supplier_id,supplier_group_id)
{

       swal({
                title: "Are you sure?",
                text: "Do you want delete this person information",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Verify it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: true,
                showLoaderOnConfirm: true

            },
          function(isConfirm){
              if (isConfirm) {
                        $.ajax({
                                url:"{{ url('admin/delete_supplier_group_member') }}",
                                method:"POST",
                                data: {supplier_id:supplier_id,supplier_group_id:supplier_group_id, _token: "{{ csrf_token() }}"},
                                // contentType: false,
                                cache:false,
                                // processData: false,
                                // dataType:"json",
                                success:function(data)
                                {
                                    console.log(data);

                                    if(data==1)
                                    {
                                      location.reload();
                                    }
                                    else
                                    {
                                      swal(data);
                                    }
                                },
                            });

                        }
          });
    }


</script>

@endsection
