<?php

use App\Count;
use App\Course;
use App\Lesson;
use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            "title"                         => "Reselling bussiness mastery course",
            "description"                   => "In this course, we will teach you how to create your own online business without any Investment.In India there are more than 50 Lakh Resellers who have created their selling Products in Whatsapp and Instagram. Follow the step by step lesson in this course and start your own online Selling Business.",
            "total_price"                   => "2000.00",
            "discount_price"                => "1800.00",
            "is_certificate"                => 1,
            "is_free"                       => 1,
            "what_will_you_get"             => "<ul><li>13 से ज़्यादा मॉड्यूल्स</li><li> 50+ टेक्स्ट टॉपिक्स</li><li> 10+ वीडियो लेक्चर्स</li><li> स्टॉक अनालिसिस टेम्पलेट</li><li> स्पैशल रिपोर्ट</li><li> टेस्ट्स</li><li> सर्टिफिकेट</li></ul>",
            "what_you_learn"                => "<ul><li>शेयर मार्केट की पूरी तकनीकी शब्दावली &ndash; उसका अर्थ और उपयोग</li><li>किसी भी कम्पनी की ऐन्यूअल रिपोर्ट को समझने और आँकने पर चैप्टर्स &nbsp;</li><li>स्टॉक्स के सही मूल्य का निर्धारण कैसे करें</li><li>स्टॉक मार्केट के दिग्गजों के निवेश के सिद्धांत &nbsp;</li><li>कम्पनी चुन्ने की सही पद्धति और वो तरीक़े जिनसे कभी कोई कम्पनी चुनी नहीं जानी चाहिए</li><li>स्टॉक मार्केट में प्रचलित मिथों का सच</li><li>सेक्टर के आधार पर मार्केट का विश्लेषण</li></ul>",
            "how_this_course_will_help_you" => "<p>क्या आप बेहतर भविष्य के लिए निवेश करना चाह रहे हैं पर स्टॉक्स में निवेश करने से डरते हैं? क्या आप मार्केट में प्रचलित मिथों के चलते ख़ुद को रोक रहे हैं?</p>

                        <p>चिंता ना करें, हम लाए हैं ये कोर्स जो स्टॉक मार्केट में आपकी सफलता की सीढ़ी बनेगा। इस कोर्स की मदद से स्टॉक मार्केट की सिद्धांतिक और वास्तविक ख़ूबियाँ और ख़ामियाँ जान कर आप निवेश से जुड़े कठोर और सही निर्णय लेने में सक्षम होंगे।</p>

                        <p>ये कोर्स आपका पर्चय कराएगा वॉरन बफे, बेंजामिन ग्राहम, आदि जैसे महान निवेशकों की रणनीतियों से।</p>

                        <p>ये कोर्स लिखा गया है दोनो नए और पुराने निवेशकों को ध्यान में रखते हुए जिसके लिए आपको पहले से किसी डिग्री, सर्टिफ़िकेट या जानकारी की आवश्यकता नहीं है।</p>",
            "image"                         => "socialseller/images/c1.jpg",
            "course_url"                    => "https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0",
            "langauge"                      => 1,
            "category"                      => 1,
            "course_for"                    => "1,2",

        );
        $course     = Course::create($data);
        $lessonData = array(
            array("course_id" => $course->id,
                "title"           => "introduction",
                "description"     => "In this course, we will teach you how to create your own online business without any Investment.In India there are more than 50 Lakh Resellers who have created their selling Products in Whatsapp and Instagram. Follow the step by step lesson in this course and start your own online Selling Business",
                "url"             => "https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0",

            ),
            array("course_id" => $course->id,
                "title"           => "introduction 2",
                "description"     => "In this course, we will teach you how to create your own online business without any Investment.In India there are more than 50 Lakh Resellers who have created their selling Products in Whatsapp and Instagram. Follow the step by step lesson in this course and start your own online Selling Business",
                "url"             => "https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0",

            ),
        );

        Lesson::insert($lessonData);
        $data = array(
            "title"                         => "Mind mastery course",
            "description"                   => "In this course, we will teach you how to create your own online business without any Investment.In India there are more than 50 Lakh Resellers who have created their selling Products in Whatsapp and Instagram. Follow the step by step lesson in this course and start your own online Selling Business.",
            "total_price"                   => "2100.00",
            "discount_price"                => "1900.00",
            "is_certificate"                => 1,
            "is_free"                       => 1,
            "what_will_you_get"             => "<ul><li>13 से ज़्यादा मॉड्यूल्स</li><li> 50+ टेक्स्ट टॉपिक्स</li><li> 10+ वीडियो लेक्चर्स</li><li> स्टॉक अनालिसिस टेम्पलेट</li><li> स्पैशल रिपोर्ट</li><li> टेस्ट्स</li><li> सर्टिफिकेट</li></ul>",
            "what_you_learn"                => "<ul><li>शेयर मार्केट की पूरी तकनीकी शब्दावली &ndash; उसका अर्थ और उपयोग</li><li>किसी भी कम्पनी की ऐन्यूअल रिपोर्ट को समझने और आँकने पर चैप्टर्स &nbsp;</li><li>स्टॉक्स के सही मूल्य का निर्धारण कैसे करें</li><li>स्टॉक मार्केट के दिग्गजों के निवेश के सिद्धांत &nbsp;</li><li>कम्पनी चुन्ने की सही पद्धति और वो तरीक़े जिनसे कभी कोई कम्पनी चुनी नहीं जानी चाहिए</li><li>स्टॉक मार्केट में प्रचलित मिथों का सच</li><li>सेक्टर के आधार पर मार्केट का विश्लेषण</li></ul>",
            "how_this_course_will_help_you" => "<p>क्या आप बेहतर भविष्य के लिए निवेश करना चाह रहे हैं पर स्टॉक्स में निवेश करने से डरते हैं? क्या आप मार्केट में प्रचलित मिथों के चलते ख़ुद को रोक रहे हैं?</p>

                        <p>चिंता ना करें, हम लाए हैं ये कोर्स जो स्टॉक मार्केट में आपकी सफलता की सीढ़ी बनेगा। इस कोर्स की मदद से स्टॉक मार्केट की सिद्धांतिक और वास्तविक ख़ूबियाँ और ख़ामियाँ जान कर आप निवेश से जुड़े कठोर और सही निर्णय लेने में सक्षम होंगे।</p>

                        <p>ये कोर्स आपका पर्चय कराएगा वॉरन बफे, बेंजामिन ग्राहम, आदि जैसे महान निवेशकों की रणनीतियों से।</p>

                        <p>ये कोर्स लिखा गया है दोनो नए और पुराने निवेशकों को ध्यान में रखते हुए जिसके लिए आपको पहले से किसी डिग्री, सर्टिफ़िकेट या जानकारी की आवश्यकता नहीं है।</p>",
            "image"                         => "socialseller/images/c1.jpg",
            "course_url"                    => "https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0",
            "langauge"                      => 1,
            "category"                      => 2,
            "course_for"                    => "1,2",

        );
        $course     = Course::create($data);
        $lessonData = array(
            array("course_id" => $course->id,
                "title"           => "introduction",
                "description"     => "In this course, we will teach you how to create your own online business without any Investment.In India there are more than 50 Lakh Resellers who have created their selling Products in Whatsapp and Instagram. Follow the step by step lesson in this course and start your own online Selling Business",
                "url"             => "https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0",

            ),
            array("course_id" => $course->id,
                "title"           => "introduction 2",
                "description"     => "In this course, we will teach you how to create your own online business without any Investment.In India there are more than 50 Lakh Resellers who have created their selling Products in Whatsapp and Instagram. Follow the step by step lesson in this course and start your own online Selling Business",
                "url"             => "https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0",

            ),
        );

        Lesson::insert($lessonData);

        $a = array(
            'resellers'           => '16',
            'connects'            => '16',
            'suppliers'           => '18',
            'students'            => '16',
            'views'               => '16',
            'subscriber'          => '16',
            'followers'           => '16',
            'shares'              => '12',
            'satisfied_customers' => '16',
            'complete_projects'   => '16',
            'cup_of_coffee'       => '11',
            'award_won'           => '16',
        );
        Count::insert($a);
    }
}
