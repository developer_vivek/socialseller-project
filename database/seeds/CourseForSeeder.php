<?php

use App\CourseFor;
use Illuminate\Database\Seeder;

class CourseForSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
                'name'  => 'Housewife',
                "image" => 'socialseller/house.jpg',
            ),
            array(
                'name'  => 'Children',
                "image" => 'socialseller/house.jpg',
            ),
            array(
                'name'  => 'Professional',
                "image" => 'socialseller/house.jpg',
            ),
            array(
                'name'  => 'Teacher',
                "image" => 'socialseller/house.jpg',
            ),

        );
        CourseFor::insert($data);
    }
}
