<?php

use App\AdminUser;
use App\Category;
use App\Langauge;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CourseSeeder::class);
        $this->call(CourseForSeeder::class);

        $adminUser = AdminUser::create([
            'name'     => 'admin',
            'email'    => 'vivek.et1993@gmail.com',
            "role_id"  => 1,
            'password' => Hash::make('12345'),

        ]);
        $adminUser = AdminUser::create([
            'name'     => 'admin',
            'email'    => 'admin@gmail.com',
            "role_id"  => 1,
            'password' => Hash::make('12345'),

        ]);

        $data = array(
            array('name' => 'Business'),
            array('name' => 'Finance'),
            array('name' => 'Share Market'),
            //...
        );

        Category::insert($data);
        $langaugeData = array(
            array("name" => "Hindi"),
            array("name" => "English"),
            array("name" => "Urdu"),
        );
        Langauge::insert($langaugeData);
    }

}
