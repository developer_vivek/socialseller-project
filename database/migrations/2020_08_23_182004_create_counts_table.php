<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('counts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('resellers')->nullable();
            $table->string('connects')->nullable();
            $table->string('suppliers')->nullable();
            $table->string('students')->nullable();
            $table->string('views')->nullable();
            $table->string('subscriber')->nullable();
            $table->string('followers')->nullable();
            $table->string('shares')->nullable();
            $table->string('satisfied_customers')->nullable();
            $table->string('complete_projects')->nullable();
            $table->string('cup_of_coffee')->nullable();
            $table->string('award_won')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counts');
    }
}
