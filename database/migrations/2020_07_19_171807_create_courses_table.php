<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->bigInteger('langauge')->unsigned()->comment('id of langauge')->nullable();

            $table->bigInteger('category')->unsigned()->comment('id of category');
            $table->string('image')->nullable();
            $table->string('course_url')->nullable();
            $table->string('video_thumbnail')->nullable();
            $table->string('total_hours')->nullable();
            $table->text('course_for')->nullable();
            $table->string('author')->nullable();
            // $table->bigInteger('seller_id')->unsigned()->nullable();
            $table->text('what_will_you_get')->nullable();
            $table->text('how_this_course_will_help_you')->nullable();
            $table->text('what_you_learn')->nullable();

            $table->string('total_enrollment')->nullable()->default(0);
            $table->string('total_lesson')->nullable()->default(0);
            // $table->text('who_can_learn')->nullable()->comment("csv format");
            // $table->string('total_modules')->nullable()->default(0);
            $table->string('total_price')->nullable();
            $table->string('ratings')->nullable()->default(0);
            $table->string('discount_price')->nullable();
            $table->string('validity_in_days')->nullable();
            $table->tinyInteger('is_certificate')->nullable()->default(0);
            $table->tinyInteger('is_lifetime_access')->nullable()->default(0);
            $table->tinyInteger('is_free')->nullable()->default(0);
             $table->enum('status', ['Active', 'Inactive'])->default('Active');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
