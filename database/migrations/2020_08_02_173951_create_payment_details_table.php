<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('course_id')->unsigned()->nullable();
            $table->bigInteger('appointment_id')->unsigned()->nullable();
            $table->string('razorpay_order_id')->nullable();
            $table->string('razorpay_signature')->nullable();
            $table->string('razorpay_payment_id')->nullable();
            $table->string('razorpay_status')->nullable();
            $table->string('razorpay_reciept')->nullable();
            $table->string('razorpay_attempt')->nullable();
            $table->string('razorpay_payment_capture')->nullable();
            $table->enum('type', ['course', 'supplier', 'appointment'])->nullable();

            $table->string('amount')->nullable()->default(0);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_details');
    }
}
