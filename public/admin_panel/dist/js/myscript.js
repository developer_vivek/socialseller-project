function blockUser(id) {
    swal({
        title: "Are you sure?",
        text: "Do you want to block this user!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Block it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
        function (isConfirm) {
            if (isConfirm) {
                $.getJSON('user/block_user/' + id + '/3', function (res) {
                    swal("Success!", "User has successfully blocked!", "success");
                    location.reload();
                });
            } else {
                swal("Cancelled", "Your User Status is safe :)", "error");
            }
        });
}
function unblockUser(id) {
    swal({
        title: "Are you sure?",
        text: "Do you want to Unblock this user!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, Unblock it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
        function (isConfirm) {
            if (isConfirm) {
                $.getJSON('user/block_user/' + id + '/2', function (res) {
                    console.log(res);
                    swal("Success!", "User has successfully Unblocked!", "success");
                    location.reload();
                });
            } else {
                swal("Cancelled", "Your User Status is safe :)", "error");
            }
        });
}

function deleteToken(id) {
    swal({
        title: "Are you sure?",
        text: "Do you want to Delete token of this user!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
        function (isConfirm) {
            if (isConfirm) {
                $.getJSON('user/delete_token/' + id, function (res) {
                    swal("Success!", "Token has successfully deleted!", "success");
                });
            } else {
                swal("Cancelled", "Your token is safe :)", "error");
            }
        });
}