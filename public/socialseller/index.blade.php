<!DOCTYPE html>
<html>
   <head>
      <title>Socialseller</title>
      @include('head');
   </head>
   <body>

      @include('partial.header');
      <section class="header" style="background:url(images/homebg.png);background-size: 100% 100%!important;background-repeat: no-repeat;
         background-size: cover;">
         <div class="clearfix"></div>
         <div class="container-fluid" style="background: #02042b;">
            <div class="row banner no-gutters justify-content-center">

               <div class="col-12 col-md-5 text-center imgdiv  order-0 order-md-0">
                  <img data-aos="zoom-in" data-aos-delay="500" data-aos-duration="500" src="images/Graphic.png"
                     class="img-fluid bannerimg" />
               </div>
               <div class="col-12 col-md-7 textdiv order-1 order-md-1">
                  <h2>Courses that help you <br /><strong>Create your Online Business</strong>
                  </h2>
                  <ul class="checklist">
                     <li class="text-white">Drafted By Lakshit Sethiya.</li>
                     <li class="text-white">Explained in Hindi + English</li>
                     <li class="text-white">24X7 Lifetime access </li>
                  </ul>
                  <h6 class="ml-3 mt-3 text-white">Get free Courses & Lectures on
                     <br/><span style="color: #1ebd94;">Whats App</span> <img src="images/wht-w.png" style="width: 20px;">
                     <strong>Signup Now!</strong>
                  </h6>
                  <div class="input-group pt-2 ">
                     <input name="txtEmail" id="txtEmail" class="form-control" placeholder="Enter your number"
                        type="number" />
                  </div>
                  <div class="input-group ">
                     <a id="" class="btn text-white ml-0" href="" style="    width: 100%;background: #1ebd94 !important;">Get free Courses
                     </a>
                  </div>
                  <a href="#" class="small ml-3 text-white">OR View Our Courses</a>
               </div>

            </div>

         </div>

      </section>
      <section class="header2">
         <div class="container ">
            <div class="container">
               <div class="row no-gutters text-cente justify-content-center">
                  <div class="sectionheading text-center pt-5">
                     <h3 class="dark-blue-text pt-5">Trusted by Thousands of Online Sellers</h3>

                  </div>
               </div>
            </div>
            <div class="row no-gutters justify-content-center">
               <div class="col-6 col-md-3 text-center " data-aos="fade-up" data-aos-delay='0'>
               	 <div class="columns2 hidden-xs hidden-sm"> </div>
               	<div class="box123">
                  <img src="images/Icon_1.png" class="text-center icon11">
                  <h3 class="text-white text-center pt-4">1000+</h3>
                  <p class="text-white text-center">Enrolled Lectures</p>
                  </div>
               </div>
               <div class="col-6 col-md-3 text-center " data-aos="fade-up" data-aos-delay='0'>
               		<div class="box123">
                  <img src="images/Icon_2.png" class="text-center icon11">
                  <h3 class="text-white text-center pt-4">10,000</h3>
                  <p class="text-white text-center">Enrolled Lectures</p>
              </div>
               </div>
               <div class="col-6 col-md-3 text-center" data-aos="fade-up" data-aos-delay='0'>
               	<div class="box123">
                  <img src="images/Icon_3.png" class="text-center icon11">
                  <h3 class="text-white text-center pt-4">1000+</h3>
                  <p class="text-white text-center">Enrolled Lectures</p>
              </div>
               </div>
               <div class="col-6 col-md-3 text-center " data-aos="fade-up" data-aos-delay='0'>
               	<div class="box123 ">
                  <img src="images/Icon_4.png" class="text-center icon11">
                  <h3 class="text-white text-center pt-4">1000+</h3>
                  <p class="text-white text-center">Enrolled Lectures</p>
                  <div class="columns1 hidden-xs hidden-sm">

                  </div>
              </div>
               </div>
            </div>
         </div>
      </section>

      <section class="courses ">
      	<section class="pt-5 text-center">
         <div class="container">
            <div class="row no-gutters justify-content-center">

               	<div class="sectionheading pt-5 text-center">
                  <h3 class="dark-blue-text">Best Online Courses</h3>
                  <p class="subheading">These Courses will help you Grow your Online Business</p>
               </div>

            </div>
         </div>
      </section>
         <div class="container">
            <div class="row no-gutters justify-content-center">
               <div class="col-12 col-md-3" data-aos="fade-up" data-aos-delay='0'>

                  <div class="coursecard">
                     <div class="courseimg">
                        <img class="img-fluid" src='images/c1.jpg' />
                        <span class="badge badge-warning"> Mastery</span>
                     </div>
                     <div class="p-2 coursedetails">
                        <span class="coursename">रीसेलिंग बिज़नेस मास्टरी कोर्स</span>
                        <div class="clearfix"></div>
                        <div class="coursehighlights">
                           <img src="images/Icon_5.png" style="width: 20px;">
                           <p>11 Lessons</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="images/Icon_6.png" style="width: 20px;">
                           <p>Certificate</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="images/Icon_7.png" style="width: 20px;">
                           <p> Hours</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <a href='courses_details.php' class="enrollbtn"><i class="fas fa-user-plus"></i>Enroll Now</a>
                  </div>
               </div>
               <div class="col-12 col-md-3" data-aos="fade-up" data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg">
                        <img class="img-fluid" src='images/c1.jpg' />
                        <span class="badge badge-warning"> Mastery</span>
                     </div>
                     <div class="p-2 coursedetails">
                        <span class="coursename">रीसेलिंग बिज़नेस मास्टरी कोर्स</span>
                        <div class="clearfix"></div>
                        <div class="coursehighlights">
                           <img src="images/Icon_5.png" style="width: 20px;">
                           <p>11 Lessons</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="images/Icon_6.png" style="width: 20px;">
                           <p>Certificate</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="images/Icon_7.png" style="width: 20px;">
                           <p> Hours</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <a href='courses_details.php' class="enrollbtn"><i class="fas fa-user-plus"></i>Enroll Now</a>
                  </div>
               </div>
               <div class="col-12 col-md-3" data-aos="fade-up" data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg">
                        <img class="img-fluid" src='images/c1.jpg' />
                        <span class="badge badge-warning"> Mastery</span>
                     </div>
                    <div class="p-2 coursedetails">
                        <span class="coursename">रीसेलिंग बिज़नेस मास्टरी कोर्स</span>
                        <div class="clearfix"></div>
                        <div class="coursehighlights">
                           <img src="images/Icon_5.png" style="width: 20px;">
                           <p>11 Lessons</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="images/Icon_6.png" style="width: 20px;">
                           <p>Certificate</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="images/Icon_7.png" style="width: 20px;">
                           <p> Hours</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <a href='courses_details.php' class="enrollbtn"><i class="fas fa-user-plus"></i>Enroll Now</a>
                  </div>
               </div>
               <div class="col-12 col-md-3" data-aos="fade-up" data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg">
                        <img class="img-fluid" src='images/c1.jpg' />
                        <span class="badge badge-warning"> Mastery</span>
                     </div>
                    <div class="p-2 coursedetails">
                        <span class="coursename">रीसेलिंग बिज़नेस मास्टरी कोर्स</span>
                        <div class="clearfix"></div>
                        <div class="coursehighlights">
                           <img src="images/Icon_5.png" style="width: 20px;">
                           <p>11 Lessons</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="images/Icon_6.png" style="width: 20px;">
                           <p>Certificate</p>
                        </div>
                        <div class="coursehighlights">
                           <img src="images/Icon_7.png" style="width: 20px;">
                           <p> Hours</p>
                        </div>
                        <div class="clearfix"></div>
                     </div>
                     <a href='courses_details.php' class="enrollbtn"><i class="fas fa-user-plus"></i>Enroll Now</a>
                  </div>
               </div>
            </div>

         </div>
      </section>
      <section class="courses ">
         <div class="container">
         	<div class="sectionheading pt-5 text-center">
                  <h3 class="dark-blue-text">Frequently Asked Questions</h3>
                  <p class="subheading">For all those little doubts on your mind</p>
               </div>

            <div class="faq row  no-gutters  mb-10">
               <div class="col-12 col-md-6 ">

                     <iframe src="https://www.youtube.com/embed/Z1RWgMlwcxU?controls=0" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="" class="videothumb" id="fitvid6"
                        width="100%" height="315"></iframe>

               </div>
            </div>
            <div class="row faq no-gutters mt-5">
               <div class="accordion" id="accordion-tab-1">
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-1">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-1" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-1"><i
                              class="fas fa-plus-circle"></i>&nbsp;What does Finology Learn offer and how will
                           that help me?</button>
                        </h5>
                     </div>
                     <div class="collapse show" id="accordion-tab-1-content-1"
                        aria-labelledby="accordion-tab-1-heading-1" data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>Finology offers various courses on law, finance and entrepreneurship to help everyone
                              learn more and grow more. Our tech enabled and user friendly platform helps you in
                              your quest to learn something new, every day.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-2">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-2" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-2"><i
                              class="fas fa-plus-circle"></i>&nbsp;Who can sign-up for the courses, is there
                           any restriction or age limit?</button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-2" aria-labelledby="accordion-tab-1-heading-2"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>Not at all ! Finology aims to remove barriers & boundaries in learning. Our learning
                              tools are designed to help everyone learn more and grow more.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-3">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-3" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-3"><i
                              class="fas fa-plus-circle"></i>&nbsp;How can I sign-up and access the
                           courses?</button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-3" aria-labelledby="accordion-tab-1-heading-3"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>To subscribe to any course, all you need to do is simply click on the "Enroll now"
                              button, fill a few details and make the payment using your desired payment option.
                              That's it! You now have access to the subscribed course on your user dashboard after
                              logging in.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-4">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-4" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-4"><i
                              class="fas fa-plus-circle"></i>&nbsp;What payment modes are accepted on the
                           website?</button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-4" aria-labelledby="accordion-tab-1-heading-4"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>You can use all major credit and debit cards, you can also use net banking and
                              payment wallets to subscribe.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-5">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-5" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-5"><i
                              class="fas fa-plus-circle"></i>&nbsp;How can I access the courses, are these
                           online only? Can I take print-outs? </button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-5" aria-labelledby="accordion-tab-1-heading-5"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>Yes, the courses are totally online. You will not be able to take print-outs either.
                              At the end of each course, as well as in between, there are various tests and
                              assignments to help the learners assess themselves and track their progress.
                              However, you can read the material across all platforms, browsers and devices. The
                              platform and its content are accessible 24x7 and does not require any app or
                              software downloads.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-6">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-6" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-6"><i
                              class="fas fa-plus-circle"></i>&nbsp;Where can I access all the courses I have
                           enrolled in?</button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-6" aria-labelledby="accordion-tab-1-heading-6"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>Once you sign-up, you are given access to your personal dashboard on the website
                              where you can see all your on-going and completed courses, track your progress on
                              the on-going courses, etc. In addition to that, you can access any other services
                              availed by you on the same dashboard.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="card">
                     <div class="card-header" id="accordion-tab-1-heading-7">
                        <h5>
                           <button class="btn btn-link" type="button" data-toggle="collapse"
                              data-target="#accordion-tab-1-content-7" aria-expanded="false"
                              aria-controls="accordion-tab-1-content-7"><i class="fas fa-plus-circle"></i>&nbsp;Do
                           you offer refunds?</button>
                        </h5>
                     </div>
                     <div class="collapse" id="accordion-tab-1-content-7" aria-labelledby="accordion-tab-1-heading-7"
                        data-parent="#accordion-tab-1">
                        <div class="card-body">
                           <p>Sorry, our team works really hard to make the courses enriching. We offer constant
                              improvements but not refunds.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php include 'enrollinside.php';?>
      <?php include 'feedback.php';?>
       <?php include 'seller-testimonial.php';?>
      <?php include 'blog.php';?>
      <section style="padding:4rem 0 4rem;background: #f3f9fd;">
         <div class="sectionheading text-center">
            <h3 class="dark-blue-text">Connect with
               the Best Suppliers for<br /> Reselling Business
            </h3>
            <p class="subheading">Subscribe and Get the
               Database of Best Online Suppliers
            </p>
         </div>
         <!--div class="container">
            <div class="mt-5">
               <div class="row justify-content-center">
               	<div class="col-md-10">
               		<div class="row">
	                  <span style="padding: 0px 10px;
	                  background: linear-gradient(179deg, rgba(14,83,177,1) 19%, rgba(35,119,201,1) 98%) !important;
	                   border-radius: 50%;    z-index: 1;
	                   color: #ffff;    margin-bottom: -7px;">1</span>
                   </div>
               <div style="background: #f3fafd;
                    box-shadow: 0px 0px 13px 5px #f6f3f3f7; ">
                  <div class="row no-gutters justify-content-center">
                     <div class="col-md-7">
                           <h4><img class="img-circle" alt="" src="http://socialseller.co.in/assets/images/test5.png"
                           style="width: 50px;">
                           <b style="color: #09256d;">S.M Business</b> <span class="subheading">|</span> <span class="subheading"> Wholesellers</span>
                           </h4>
                         <div class="row no-gutters">
                             <div class="col-md-3 col-xs-6 col-6"><img src="images/product.jpg" class="img-fluid border-radius"> </div>
                             <div class="col-md-3 col-xs-6 col-6"><img src="images/product.jpg" class="img-fluid border-radius"> </div>
                             <div class="col-md-3 col-xs-6 col-6"><img src="images/product.jpg" class="img-fluid border-radius"> </div>
                             <div class="col-md-3 col-xs-6 col-6"><img src="images/product.jpg" class="img-fluid border-radius"> </div>

                         </div>
                     </div>
                     <div class="col-md-5">
                        <div  class="mt-2 text-center">
                           <h6><i>Services</i></h6>
                          <div class="row pt-2" >
                               <div class="col-md-6 col-6 text-center">
                             <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                              <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                              <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                               <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>

                          </div>
                          <div class="col-md-6 col-6">
                             <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                               <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                              <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                                <label class="container">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                              </label>
                          </div>

                          </div>
                        </div>
                        <div class="col-md-12  p-1 pt-3">
                             <a href="#" class=" green-btn btn text-white ml-0" style=" border-radius: 5px !important; width: 100% !important"><img src="images/wht-w.png" width="20px;"> Connect</a>
                         </div>
                     </div>
                  </div>
               </div>
               	</div>
               </div>
            </div>



         </div-->
         <div class="container">
            <div class="row no-gutters justify-content-center">
             <div class="col-12 col-md-4" data-aos="fade-up"        data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg1 text-center">
                        <img class="img-fluid" src='images/product.jpg' />
                        <span class="badge badge-warning"> Category 1</span>
                     </div>
                     <div class="p-1 coursedetails">
                        <span class="coursename1">S.M Business | Wholesellers</span>

                        <div class="row justify-content-center">
                        	<div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                           <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                        </div>

                     </div>
                     <a href='courses_details.php' class=" green-btn btn text-white ml-0"><img src="images/wht-w.png" width="20px;"> Connect</a>
                  </div>
             </div>
             <div class="col-12 col-md-4" data-aos="fade-up"        data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg1 text-center">
                        <img class="img-fluid" src='images/product.jpg' />
                        <span class="badge badge-warning"> Category 1</span>
                     </div>
                     <div class="p-1 coursedetails">
                        <span class="coursename1">S.M Business | Wholesellers</span>

                        <div class="row justify-content-center">
                        	<div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                           <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                        </div>

                     </div>
                     <a href='courses_details.php' class=" green-btn btn text-white ml-0"><img src="images/wht-w.png" width="20px;"> Connect</a>
                  </div>
             </div>
             <div class="col-12 col-md-4" data-aos="fade-up"        data-aos-delay='0'>
                  <div class="coursecard">
                     <div class="courseimg1 text-center">
                        <img class="img-fluid" src='images/product.jpg' />
                        <span class="badge badge-warning"> Category 1</span>
                     </div>
                     <div class="p-1 coursedetails">
                        <span class="coursename1">S.M Business | Wholesellers</span>

                        <div class="row justify-content-center">
                        	<div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                           <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                            <div class=" col-md-6 col-xs-6 col-6">
                                <input type="checkbox" checked="checked">Subscribe
                                <span class="checkmark"></span>
                            </div>
                        </div>

                     </div>
                     <a href='courses_details.php' class=" green-btn btn text-white ml-0"><img src="images/wht-w.png" width="20px;"> Connect</a>
                  </div>
             </div>

            </div>
            <div class="row justify-content-center">

                  <div class="col-md-6  p-4 text-center">
                          <a href="#" class=" green-btn btn text-white ml-0" style=" border-radius: 5px !important;font-weight: 600 !important;font-size: 1.2rem !important; width:80% !important;"> GET SUPPLIERS DETAILS</a>
                 </div>

            </div>
         </div>
      </section>
   @include ('helping');
   @include ('trust');
   @include ('consultation');
   @include ('footer');
      <div class="bsnav-mobile">
         <div class="bsnav-mobile-overlay">
         <div class="navbar">
            <div class="masterbarmobile"></div>
         </div>
      </div>

      @include ('footer-scripts');
   </body>
</html>