<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="ThemeMascot" />

<!-- Page Title -->
<title></title>


<!-- Stylesheet -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">
<link href="css/css-plugin-collections.css" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="css/menuzord-skins/menuzord-rounded-boxed.css" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="css/style-main.css" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="css/preloader.css" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="css/custom-bootstrap-margin-padding.css" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="css/responsive.css" rel="stylesheet" type="text/css">

<link  href="js/revolution-slider/css/settings.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/layers.css" rel="stylesheet" type="text/css"/>
<link  href="js/revolution-slider/css/navigation.css" rel="stylesheet" type="text/css"/>
<link href="css/colors/theme-skin-orange.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/jquery-plugin-collection.js"></script>

<script src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
<style type="text/css">
  .bg-red
  {
    background: #990000 !important;
  }
  .background-gray
  {
    background:#ededed !important;
  }
  .border-10
  {
    border-radius: 10px;
  }
  .font-Opensans{
  font-family: 'Open Sans', sans-serif;
}
.tab11 a
{
  background: #990000 !important;
  border: none !important;
  color: #fff !important;
 border-radius: 20px !important;
  font-size: 22px !important;
   font-family: 'Open Sans', sans-serif;
}
.Marquee {
  background: -webkit-linear-gradient(-135deg, #990000, #890909);
  background: -moz-linear-gradient(-135deg, #990000, #890909);
  background: -o-linear-gradient(-135deg, #990000, #890909);
  background: -ms-linear-gradient(-135deg, #990000, #890909);
  background: linear-gradient(-135deg, #990000, #890909);
  width: 100%;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  padding: 1em;
  color: #fff;
  font-weight: 200;
  display: -webkit-box;
  display: -moz-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: box;
  display: flex;
  -webkit-box-align: center;
  -moz-box-align: center;
  -o-box-align: center;
  -ms-flex-align: center;
  -webkit-align-items: center;
  align-items: center;
  overflow: hidden;
}
.Marquee-content {
  display: -webkit-box;
  display: -moz-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display: box;
  display: flex;
  -webkit-animation: marquee 10s linear infinite running;
  -moz-animation: marquee 10s linear infinite running;
  -o-animation: marquee 10s linear infinite running;
  -ms-animation: marquee 10s linear infinite running;
  animation: marquee 10s linear infinite running;
}
.Marquee-content:hover {
  -webkit-animation-play-state: paused;
  -moz-animation-play-state: paused;
  -o-animation-play-state: paused;
  -ms-animation-play-state: paused;
  animation-play-state: paused;
}
.Marquee-tag {
  width: 200px;
  margin: 0 0.5em;
  padding: 0.5em;
  background: rgba(255,255,255,0.1);
  display: -webkit-inline-box;
  display: -moz-inline-box;
  display: -webkit-inline-flex;
  display: -ms-inline-flexbox;
  display: inline-box;
  display: inline-flex;
  -webkit-box-align: center;
  -moz-box-align: center;
  -o-box-align: center;
  -ms-flex-align: center;
  -webkit-align-items: center;
  align-items: center;
  -webkit-box-pack: center;
  -moz-box-pack: center;
  -o-box-pack: center;
  -ms-flex-pack: center;
  -webkit-justify-content: center;
  justify-content: center;
  -webkit-transition: all 0.2s ease;
  -moz-transition: all 0.2s ease;
  -o-transition: all 0.2s ease;
  -ms-transition: all 0.2s ease;
  transition: all 0.2s ease;
}
.Marquee-tag:hover {
  background: rgba(255,255,255,0.5);
  -webkit-transform: scale(1.1);
  -moz-transform: scale(1.1);
  -o-transform: scale(1.1);
  -ms-transform: scale(1.1);
  transform: scale(1.1);
  cursor: pointer;
}
@-moz-keyframes marquee {
  0% {
    -webkit-transform: translateX(0);
    -moz-transform: translateX(0);
    -o-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
  100% {
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
    -ms-transform: translate(-50%);
    transform: translate(-50%);
  }
}
@-webkit-keyframes marquee {
  0% {
    -webkit-transform: translateX(0);
    -moz-transform: translateX(0);
    -o-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
  100% {
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
    -ms-transform: translate(-50%);
    transform: translate(-50%);
  }
}
@-o-keyframes marquee {
  0% {
    -webkit-transform: translateX(0);
    -moz-transform: translateX(0);
    -o-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
  100% {
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
    -ms-transform: translate(-50%);
    transform: translate(-50%);
  }
}
@keyframes marquee {
  0% {
    -webkit-transform: translateX(0);
    -moz-transform: translateX(0);
    -o-transform: translateX(0);
    -ms-transform: translateX(0);
    transform: translateX(0);
  }
  100% {
    -webkit-transform: translate(-50%);
    -moz-transform: translate(-50%);
    -o-transform: translate(-50%);
    -ms-transform: translate(-50%);
    transform: translate(-50%);
  }
}
</style>
</head>
<body class="">
<div id="wrapper" class="clearfix">
 <header class="header">
  
     <div class="header-middle p-0 bg-red xs-text-center">
      <div class="container pt-0 pb-0">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <div class="text-center mt-20 mb-20">
                <a class="menuzord-brand pull-center flip xs-pull-center wow fadeInDownBig" data-wow-duration="2.5s" data-wow-offset="10" href="#"><img src="img/garmef_logo.png" alt=""></a>            
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav">
      <div class="header-nav-wrapper navbar-scrolltofixed bg-red">
        <div class="container text-center">
          <nav id="menuzord" class="menuzord wow fadeInLeftBig default bg-white" data-wow-duration="1.5s" data-wow-offset="10" style="border-radius: 10px;margin-bottom: -20px;
             z-index: 10;">
            <ul class="menuzord-menu text-center">
              <li class=""><a href="#home">Home</a></li>
              <li class=""><a href="#home">Company</a></li>
              <li class=""><a href="#home">Products</a></li>
              <li class=""><a href="#home">About Us</a></li>
              <li class=""><a href="#home">CSR</a></li>
              <li class=""><a href="#home">Certificate</a></li>
              <li class=""><a href="#home">Contact</a></li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
               
  </header>
  <div class="main-content">
    <section id="home">
      <div class="container-fluid p-0">
        <div class="rev_slider_wrapper">
          <div class="rev_slider rev_slider_fullscreen" data-version="5.0">
            <ul>

              <!-- SLIDE 1 -->
              <li data-index="rs-1" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="img/sli1.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide 3" data-description="">
                <!-- MAIN IMAGE -->
                <img src="img/sli1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent pr-20 pl-20"
                  id="rs-1-layer-1"

                  data-x="['right']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-90']" 
                  data-fontsize="['64']"
                  data-lineheight="['72']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:600;"><span class="">Help</span> The Poor
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway"
                  id="rs-1-layer-2"

                  data-x="['right']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['-25']" 
                  data-fontsize="['32']"
                  data-lineheight="['54']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:600;">For Their Better Future 
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme text-white text-right" 
                  id="rs-1-layer-3"

                  data-x="['right']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['30']"
                  data-fontsize="['16','18',24']"
                  data-lineheight="['28']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1400" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">Every day we bring hope to millions of children in the world's<br>  hardest places as a sign of God's unconditional love.
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption tp-resizeme" 
                  id="rs-1-layer-4"

                  data-x="['right']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['95']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                  data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                  data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1400" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg btn-flat btn-theme-colored pl-20 pr-20" href="#">Donate Now</a> 
                </div>
              </li>

              <!-- SLIDE 2 -->
              <li data-index="rs-2" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="img/sli2.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide 2" data-description="">
                <!-- MAIN IMAGE -->
                <img src="img/sli2.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway"
                  id="rs-2-layer-1"

                  data-x="['left']"
                  data-hoffset="['30']"
                  data-y="['middle']"
                  data-voffset="['-110']" 
                  data-fontsize="['110']"
                  data-lineheight="['120']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:700;">Donate
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-uppercase text-white font-raleway bg-theme-colored-transparent pl-20 pr-20"
                  id="rs-2-layer-2"

                  data-x="['left']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['-25']" 
                  data-fontsize="['35']"
                  data-lineheight="['54']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:600;">For the poor children 
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme text-white" 
                  id="rs-2-layer-3"

                  data-x="['left']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['35','35','40']"
                  data-fontsize="['16','18',24']"
                  data-lineheight="['28']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1400" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">Every day we bring hope to millions of children in the world's<br>  hardest places as a sign of God's unconditional love.
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption tp-resizeme" 
                  id="rs-2-layer-4"

                  data-x="['left']"
                  data-hoffset="['35']"
                  data-y="['middle']"
                  data-voffset="['95','105','110']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                  data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                  data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1400" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-colored btn-lg btn-theme-colored pl-20 pr-20" href="#">Donate Now</a> 
                </div>
              </li>

              <!-- SLIDE 3 -->
              <li data-index="rs-3" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="img/sli3.jpg" data-rotate="0" data-saveperformance="off" data-title="Slide 1" data-description="">
                <!-- MAIN IMAGE -->
                <img src="img/sli3.jpg"  alt=""  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10" data-no-retina>
                <!-- LAYERS -->

                <!-- LAYER NR. 1 -->
                <div class="tp-caption tp-resizeme text-uppercase  bg-dark-transparent text-white font-raleway pl-30 pr-30"
                  id="rs-3-layer-1"
                
                  data-x="['center']"
                  data-hoffset="['0']"
                  data-y="['middle']"
                  data-voffset="['-90']" 
                  data-fontsize="['28']"
                  data-lineheight="['54']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:400; border-radius: 30px;">For the poor children 
                </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption tp-resizeme text-uppercase bg-theme-colored-transparent text-white font-raleway pl-30 pr-30"
                  id="rs-3-layer-2"

                  data-x="['center']"
                  data-hoffset="['0']"
                  data-y="['middle']"
                  data-voffset="['-20']"
                  data-fontsize="['48']"
                  data-lineheight="['70']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1000" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 7; white-space: nowrap; font-weight:700; border-radius: 30px;">raise your helping hand 
                </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption tp-resizeme text-white text-center" 
                  id="rs-3-layer-3"

                  data-x="['center']"
                  data-hoffset="['0']"
                  data-y="['middle']"
                  data-voffset="['50']"
                  data-fontsize="['16','18',24']"
                  data-lineheight="['28']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;s:500"
                  data-transform_in="y:100;scaleX:1;scaleY:1;opacity:0;"
                  data-transform_out="x:left(R);s:1000;e:Power3.easeIn;s:1000;e:Power3.easeIn;"
                  data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1400" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:0px; font-weight:400;">Every day we bring hope to millions of children in the world's<br>  hardest places as a sign of God's unconditional love.
                </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption tp-resizeme" 
                  id="rs-3-layer-4"

                  data-x="['center']"
                  data-hoffset="['0']"
                  data-y="['middle']"
                  data-voffset="['115']"
                  data-width="none"
                  data-height="none"
                  data-whitespace="nowrap"
                  data-transform_idle="o:1;"
                  data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                  data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                  data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                  data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                  data-start="1400" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-responsive_offset="on"
                  style="z-index: 5; white-space: nowrap; letter-spacing:1px;"><a class="btn btn-default btn-circled btn-theme-colored pl-20 pr-20" href="#">Donate Now</a> 
                </div>
              </li>

            </ul>
          </div>
          <!-- end .rev_slider -->
        </div>
        <!--  Revolution slider scriopt -->
        <script>
          $(document).ready(function(e) {
            $(".rev_slider_fullscreen").revolution({
              sliderType:"standard",
              sliderLayout: "fullscreen",
              dottedOverlay: "none",
              delay: 5000,
              navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                touch: {
                  touchenabled: "on",
                  swipe_threshold: 75,
                  swipe_min_touches: 1,
                  swipe_direction: "horizontal",
                  drag_block_vertical: false
                },
                arrows: {
                  style:"zeus",
                  enable:true,
                  hide_onmobile:true,
                  hide_under:600,
                  hide_onleave:true,
                  hide_delay:200,
                  hide_delay_mobile:1200,
                  tmp:'<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                  left: {
                    h_align:"left",
                    v_align:"center",
                    h_offset:30,
                    v_offset:0
                  },
                  right: {
                    h_align:"right",
                    v_align:"center",
                    h_offset:30,
                    v_offset:0
                  }
                },
                bullets: {
                  enable:true,
                  hide_onmobile:true,
                  hide_under:600,
                  style:"metis",
                  hide_onleave:true,
                  hide_delay:200,
                  hide_delay_mobile:1200,
                  direction:"horizontal",
                  h_align:"center",
                  v_align:"bottom",
                  h_offset:0,
                  v_offset:30,
                  space:5,
                  tmp:'<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span><span class="tp-bullet-title">{{title}}</span>'
                }
              },
              responsiveLevels: [1240, 1024, 778],
              visibilityLevels: [1240, 1024, 778],
              gridwidth: [1170, 1024, 778, 480],
              gridheight: [600, 768, 960, 720],
              lazyType: "none",
              parallax: {
                origo: "slidercenter",
                speed: 1000,
                levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                type: "scroll"
              },
              shadow: 0,
              spinner: "off",
              stopLoop: "on",
              stopAfterLoops: 0,
              stopAtSlide: -1,
              shuffle: "off",
              autoHeight: "off",
              fullScreenAutoWidth: "off",
              fullScreenAlignForce: "off",
              fullScreenOffsetContainer: "",
              fullScreenOffset: "0",
              hideThumbsOnMobile: "off",
              hideSliderAtLimit: 0,
              hideCaptionAtLimit: 0,
              hideAllCaptionAtLilmit: 0,
              debugMode: false,
              fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
              }
            });
          });
        </script>
        <!-- Slider Revolution Ends -->
      </div>
    </section>

  <section class="clients bg-red">
      <div class="container pt-0 pb-0">
        <div class="row">
          <div class="col-md-12">
            <!-- Section: Clients -->
            <div class="owl-carousel-6col clients-logo transparent text-center">
              <div class="item"> <a href="#"><img src="images/clients/w1.png" alt=""></a></div>
              <div class="item"> <a href="#"><img src="images/clients/w2.png" alt=""></a></div>
              <div class="item"> <a href="#"><img src="images/clients/w3.png" alt=""></a></div>
              <div class="item"> <a href="#"><img src="images/clients/w4.png" alt=""></a></div>
              <div class="item"> <a href="#"><img src="images/clients/w5.png" alt=""></a></div>
              <div class="item"> <a href="#"><img src="images/clients/w6.png" alt=""></a></div>
              <div class="item"> <a href="#"><img src="images/clients/w3.png" alt=""></a></div>
              <div class="item"> <a href="#"><img src="images/clients/w4.png" alt=""></a></div>
              <div class="item"> <a href="#"><img src="images/clients/w5.png" alt=""></a></div>
              <div class="item"> <a href="#"><img src="images/clients/w6.png" alt=""></a></div>
            </div>
          </div>
        </div>
      </div>
  </section>
   

   
    <section class="bg-silver-light">
      <div class="container pb-0">
        <div class="section-content">
          <div class="row">
            
            <div class="col-md-6 wow fadeInLeftBig" data-wow-duration="2.5s" data-wow-offset="10">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/c8g0wImRS64?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="border-radius: 10px;"></iframe>
            </div>
            <div class="col-md-6 wow fadeInRightBig" data-wow-duration="2.5s" data-wow-offset="10">
              <h2 class="text-uppercase font-weight-600 mt-0 font-28 line-bottom wow fadeInRightBig font-Opensans">Serving Brands Since 1950</h2>
              <p class="font-18 font-Opensans">As a member of Hasozgen Group, Zeria Textile is founded 2012 in Istanbul –
              Turkey. The Company has gained a good reputation with providing the
              excellent quality and service. The company has a dynamic organization where
              sampling is quick and production is effective. A highly skilled workforce with
              high experience Model department working to increase efficiency and
              competitiveness. The company produce all kinds of woven fabric & woven
              garment (women and children) for well-known labels in Europe.
              </p>
              <a class="btn btn-theme-colored btn-flat btn-lg mt-10 mb-sm-30 border-10" href="#">Read More →</a>
            </div>
          </div>
        </div>

      </div>
      <img src="img/footer.png" class="img-fullwidth wow fadeInUp" data-wow-duration="3s" data-wow-offset="10">
    </section>

    <!-- divider: Emergency Services -->
    <section class="divider parallax  " data-stellar-background-ratio="0.2"  data-bg-img="img/bg1.jpg">
      <div class="container">
        <div class="section-content text-center text-white">
          <div class="row">
           <h2 class="text-white font-weight-600 font-32">We Help grow your Brands</h2>
           <p class="text-white font-22">he Company has gained a good reputation with providing the excellent quality and service.</p>
          </div>
        </div>
      </div>      
    </section>
   <section class="background-gray">
     <div class="container">
          <div class="row" data-margin-top="-140px">
            <div class="col-sm-12 col-md-4">
              <div class="icon-box p-40 iconbox-theme-colored bg-white border-1px border-10 text-center hvr-bob  wow fadeInLeftBig" data-wow-duration="3.5s" data-wow-offset="10">
                <a class="icon" href="#">
                  <i class="flaticon-charity-world-in-your-hands font-48 font-weight-100"></i>
                </a>
                <h4 class="text-uppercase mt-0 font-Opensans">Expert Engineers</h4>
                <p class="font-Opensans">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              </div>
            </div>
            <div class="col-sm-12 col-md-4">
              <div class="icon-box border-10 p-40 iconbox-theme-colored bg-white border-1px text-center hvr-bob wow fadeInLeftBig" data-wow-duration="4.5s" data-wow-offset="10">
                <a class="icon" href="#">
                  <i class="flaticon-charity-shaking-hands-inside-a-heart font-48 font-weight-100"></i>
                </a>
                <h4 class="text-uppercase mt-0 font-Opensans">Latest Tools</h4>
                <p class="font-Opensans">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              </div>
            </div>
            <div class="col-sm-12 col-md-4">
              <div class="icon-box border-10 p-40 iconbox-theme-colored bg-white border-1px text-center hvr-bob wow fadeInLeftBig" data-wow-duration="5.5s" data-wow-offset="10">
                <a class="icon" href="#">
                  <i class="flaticon-charity-child-hand-on-adult-hand font-48 font-weight-100"></i>
                </a>
                <h4 class="text-uppercase mt-0 font-Opensans">Quick Delivery</h4>
                <p class="font-Opensans">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              </div>
            </div>
          </div>
     </div>
   </section>
  <section class="background-gray">
     <div class="container">
        <div class="row text-center">
           <h1 class="font-Opensans text-uppercase font-weight-800">Clothing Manufacturer For Your Brand</h1>
            <p class="font-Opensans">An Apparel Manufacturing Company – Zeria Textile is a Trusted clothing manufacturer for your brand. All processes from yarn entry to final product referral are examined, inspected and developed within the framework of ISO quality management system.</p>
        </div>
          <div class="row mt-20">
            
            <div class="col-md-6 wow fadeInLeftBig hvr-bob" data-wow-duration="2.5s" data-wow-offset="10">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/c8g0wImRS64?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="border-radius: 10px;"></iframe>
            </div>
            <div class="col-md-6 wow fadeInRightBig" data-wow-duration="2.5s" data-wow-offset="10">
              <h1 class="text-uppercase font-Opensans font-weight-800 mt-0 font-28  wow fadeInRightBig">Quality Control</h1>
              <p class="font-18 font-Opensans">As a member of Hasozgen Group, Zeria Textile is founded 2012 in Istanbul –
              Turkey. The Company has gained a good reputation with providing the
              excellent quality and service. The company has a dynamic organization where
              sampling is quick and production is effective. A highly skilled workforce with
              high experience Model department working to increase efficiency and
              competitiveness. The company produce all kinds of woven fabric & woven
              garment (women and children) for well-known labels in Europe.
              </p>
              <a class="btn btn-theme-colored btn-flat btn-lg mt-10 mb-sm-30 border-10" href="#">Read More →</a>
            </div>
          </div>
     </div>
   </section>
   <section>
     <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
           <h4 class="font-Opensans font-weight-800 font-32">PRODUCTS SPECIALIZATION</h4>
        </div>
      </div>
       <div class="row">
          <div class="col-md-12">
            <div class="Marquee">
  <div class="Marquee-content">
    <div class="Marquee-tag">Product 1</div>
    <div class="Marquee-tag">Product 2</div>
    <div class="Marquee-tag">Product 3</div>
    <div class="Marquee-tag">Product 1</div>
    <div class="Marquee-tag">Product 2</div>
    <div class="Marquee-tag">Products 3</div>
    <div class="Marquee-tag">Product 1</div>
    <div class="Marquee-tag">Product 2</div>
    <div class="Marquee-tag">Products 3</div>
  </div>
</div>
          </div>
       </div>
       <div class="row text-center pt-20">
          <div class="col-md-12">
             <div class="owl-carousel-6col">
              <div class="item"><img src="http://placehold.it/320x400" alt=""></div>
              <div class="item"><img src="http://placehold.it/320x400" alt=""></div>
              <div class="item"><img src="http://placehold.it/320x400" alt=""></div>
              <div class="item"><img src="http://placehold.it/320x400" alt=""></div>
              <div class="item"><img src="http://placehold.it/320x400" alt=""></div>
              <div class="item"><img src="http://placehold.it/320x400" alt=""></div>
              <div class="item"><img src="http://placehold.it/320x400" alt=""></div>
              <div class="item"><img src="http://placehold.it/320x400" alt=""></div>
              <div class="item"><img src="http://placehold.it/320x400" alt=""></div>
            </div>
          </div>
         <div class="col-md-12">
            <h4 class="pt-20"><a href="">View All</a></h4>
         </div>
       </div>
     </div>
   </section>
   <section>
     <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4">
              <div class="icon-box p-40 iconbox-theme-colored bg-white  text-center hvr-bob  wow fadeInLeftBig" data-wow-duration="3.5s" data-wow-offset="10">
                <a class="icon" href="#">
                  <i class="flaticon-charity-world-in-your-hands font-48 font-weight-100"></i>
                </a>
                <h4 class="text-uppercase mt-0 font-Opensans">Completed Projects</h4>
                <p class="font-Opensans">We help our client’s company’s Apparel Manufacturing projects complete successfully in a smoother way possible.</p>
              </div>
            </div>
            <div class="col-sm-12 col-md-4">
              <div class="icon-box border-10 p-40 iconbox-theme-colored  text-center hvr-bob wow fadeInLeftBig" data-wow-duration="4.5s" data-wow-offset="10">
                <a class="icon" href="#">
                  <i class="flaticon-charity-shaking-hands-inside-a-heart font-48 font-weight-100"></i>
                </a>
                <h4 class="text-uppercase mt-0 font-Opensans">Latest Tools</h4>
                <p class="font-Opensans">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              </div>
            </div>
            <div class="col-sm-12 col-md-4">
              <div class="icon-box border-10 p-40 iconbox-theme-colored bg-white  text-center hvr-bob wow fadeInLeftBig" data-wow-duration="5.5s" data-wow-offset="10">
                <a class="icon" href="#">
                  <i class="flaticon-charity-child-hand-on-adult-hand font-48 font-weight-100"></i>
                </a>
                <h4 class="text-uppercase mt-0 font-Opensans">Quick Delivery</h4>
                <p class="font-Opensans">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias non nulla placeat, odio, qui dicta alias.</p>
              </div>
            </div>
          </div>
     </div>
   </section>
   <section>
     <div class="container pb-0 text-center">
        <div class="row">
           <div class="col-md-12">
              <h2 class="font-weight-800 font-Opensans">FACTORY TOUR</h2>
              <p class="font-Opensans">Planning ability with high sell-through rates. Well-established work flow of design lead Product
              planning that matches customer’s need with high sell-through rates, backed by cutting-edge information aggregated to the design office in Istanbul - Turkey.</p>
           </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">
              <div class="col-md-12">
              <div id="carousel-custom" class="carousel slide" data-ride="carousel">
                <div class="carousel-outer"> 
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <div class="item active"> <img src="http://placehold.it/760x350&text=slide1" alt="" /> </div>
                    <div class="item"> <img src="http://placehold.it/760x350&text=slide2" alt="" /> </div>
                    <div class="item"> <img src="http://placehold.it/760x350&text=slide3" alt="" /> </div>
                    <div class="item"> <img src="http://placehold.it/760x350&text=slide4" alt="" /> </div>
                    <div class="item"> <img src="http://placehold.it/760x350&text=slide5" alt="" /> </div>
                    <div class="item"> <img src="http://placehold.it/760x350&text=slide6" alt="" /> </div>
                    <div class="item"> <img src="http://placehold.it/760x350&text=slide7" alt="" /> </div>
                    <div class="item"> <img src="http://placehold.it/760x350&text=slide8" alt="" /> </div>
                    <div class="item"> <img src="http://placehold.it/760x350&text=slide9" alt="" /> </div>
                  </div>
                
                  <!-- Controls --> 
                  <a class="left carousel-control" href="#carousel-custom" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a> <a class="right carousel-control" href="#carousel-custom" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> 
                  </a> 
                </div>
                  <!-- Indicators -->
                  <ol class="carousel-indicators mCustomScrollbar">
                    <li data-target="#carousel-custom" data-slide-to="0" class="active"><img src="http://placehold.it/100x50&text=slide1" alt="" /></li>
                    <li data-target="#carousel-custom" data-slide-to="1"><img src="http://placehold.it/100x50&text=slide2" alt="" /></li>
                    <li data-target="#carousel-custom" data-slide-to="2"><img src="http://placehold.it/100x50&text=slide3" alt="" /></li>
                    <li data-target="#carousel-custom" data-slide-to="3"><img src="http://placehold.it/100x50&text=slide4" alt="" /></li>
                    <li data-target="#carousel-custom" data-slide-to="4"><img src="http://placehold.it/100x50&text=slide5" alt="" /></li>
                    <li data-target="#carousel-custom" data-slide-to="5"><img src="http://placehold.it/100x50&text=slide6" alt="" /></li>
                    <li data-target="#carousel-custom" data-slide-to="6"><img src="http://placehold.it/100x50&text=slide7" alt="" /></li>
                    <li data-target="#carousel-custom" data-slide-to="7"><img src="http://placehold.it/100x50&text=slide8" alt="" /></li>
                    <li data-target="#carousel-custom" data-slide-to="8"><img src="http://placehold.it/100x50&text=slide9" alt="" /></li>
                  </ol>
                  <style>
                    #carousel-example-generic {
                          margin: 20px auto;
                          width: 100%;
                      }

                      #carousel-custom {
                          margin: 20px auto;
                          width: 100%;
                      }
                      #carousel-custom .carousel-indicators {
                          margin: 10px 0 0;
                          overflow: auto;
                          position: static;
                          text-align: center;
                          white-space: nowrap;
                          width: 100%;
                      }
                      #carousel-custom .carousel-indicators li {
                          background-color: transparent;
                          -webkit-border-radius: 0;
                          border-radius: 0;
                          display: inline-block;
                          height: auto;
                          margin: 0 !important;
                          width: auto;
                      }
                      #carousel-custom .carousel-indicators li img {
                          display: block;
                          opacity: 0.5;
                      }
                      #carousel-custom .carousel-indicators li.active img {
                          opacity: 1;
                      }
                      #carousel-custom .carousel-indicators li:hover img {
                          opacity: 0.75;
                      }
                      #carousel-custom .carousel-outer {
                          position: relative;
                      }
                  </style>
              </div>
          </div>
        </div>
     </div>
   </div>
     <img src="img/footer.png" class="img-fullwidth wow fadeInUp" data-wow-duration="3s" data-wow-offset="10" >
   </section>
   <section class="divider parallax" data-stellar-background-ratio="0.2" data-bg-img="img/bg2.jpg" >
      <div class="container">
        <div class="section-content text-center">
          <div class="row">
            <div class="col-md-6">
            </div>
            <div class="col-md-6 text-justify" style="background: #716a63;    padding: 10px;">
              <h2 class="mt-0 text-white font-Opensans font-weight-800">ABOUT US</h2>
              <hr/>
              <h4 class="text-white font-Opensans ">Planning ability with high sell-through rates. Wellestablished work flow of design lead Product planning
that matches customer’s need with high sell-through
rates, backed by cutting-edge information aggregated to
the design office in Istanbul - Turkey.<br/>
Planning ability with high sell-through rates. Wellestablished work flow of design lead Product planning
that matches customer’s need with high sell-through
rates, backed by cutting-edge information aggregated to
the design office in Istanbul - Turkey.</h4>
<a class="btn btn-theme-colored btn-flat btn-lg mt-10 mb-sm-30 border-10" href="#">Read More →</a>

            </div>
          </div>
        </div>
      </div>      
    </section>
    <section>
      <div class="container">
         <div class="row">
            <div class="col-md-6 pt-20">
              <div class="row">
                 <div class="col-md-6">
                    <img src="http://placehold.it/320x200" alt="">
                 </div>
                 <div class="col-md-6">
                    <img src="http://placehold.it/320x200" alt="">
                 </div>
              </div>
              <div class="row mt-5">
                 <div class="col-md-6">
                    <img src="http://placehold.it/320x200" alt="">
                 </div>
                 <div class="col-md-6">
                    <img src="http://placehold.it/320x200" alt="">
                 </div>
              </div>
            </div>
             <div class="col-md-6">
              <h2 class="font-32 font-weight-800 font-Opensans">SOCIAL RESPONSIBILITY</h2>
              <h4 class="font-Opensans">Planning ability with high sell-through rates. Wellestablished work flow of design lead Product planning
        that matches customer’s need with high sell-through
        rates, backed by cutting-edge information aggregated to
        the design office in Istanbul - Turkey.<br/><br/>
        Planning ability with high sell-through rates. Wellestablished work flow of design lead Product planning
        that matches customer’s need with high sell-through
        rates, backed by cutting-edge information aggregated to
        the design office in Istanbul - Turkey.</h4>
            </div>
         </div>
      </div>
    </section>
    <section>
      <div class="container">
         <div class="row">
           <div class="col-md-12 text-center">
            <h2 class="font-32 font-weight-800 font-Opensans">CERTIFICATIONS</h2>
              <p class="font-Opensans font-22">Planning ability with high sell-through rates. Well-established work flow of design
              lead Product planning that matches customer’s need with high sell-through rates
              </p>
           </div>
         </div>
         <div class="row">
          <div class="col-md-12">
             <div class="col-md-4">
                <img src="http://placehold.it/320x400" alt="">
             </div>
             <div class="col-md-4">
                <img src="http://placehold.it/320x400" alt="">
             </div>
             <div class="col-md-4">
                <img src="http://placehold.it/320x400" alt="">
             </div>
          </div>
         </div>
      </div>
      <img src="img/footer.png" class="img-fullwidth wow fadeInUp" data-wow-duration="3s" data-wow-offset="10" >
    </section>
    <section>
      <div class="container">
         <div class="row">
            <div class="col-md-12">
            <h2 class="mt-0 mb-30 font-32 text-uppercase font-weight-800 text-center">CONTACT US</h2>
            <form action="#" method="post">
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="form_name">Name <small>*</small></label>
                    <input id="form_name" name="form_name" class="form-control" type="text" placeholder="Enter Name" required="">
                  </div>
                </div>
                  <div class="col-sm-4">
                  <div class="form-group">
                    <label for="form_email">Email <small>*</small></label>
                    <input id="form_email" name="form_email" class="form-control required email" type="email" placeholder="Enter Email">
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group">
                    <label for="form_email">Mobile <small>*</small></label>
                    <input id="form_email" name="form_email" class="form-control required " type="number" placeholder="Enter Mobile Number">
                  </div>
                </div>
              </div>
              <div class="row">
                 <div class="col-md-12">
                   <div class="form-group">
                    <label for="form_name">Message</label>
                    <textarea id="form_message" name="form_message" class="form-control " rows="5" placeholder="Enter Message" autocomplete="off"></textarea>
                  </div>
                 </div>
                  <div class="col-md-12">
                    <div class="form-group">
                    <input id="form_botcheck" name="form_botcheck" class="form-control" type="hidden" value="">
                    <button type="submit" class="btn btn-dark btn-theme-colored btn-flat mr-5" data-loading-text="Please wait..." name="send_mail">Send your message</button>
                  </div>
                  </div>
              </div>
              </form>
                </div>
              </div>
            </div>
    </section>
  </div>
  <!-- end main-content -->
  
  <!-- Footer -->
  <footer id="footer" class="footer" data-bg-img="images/footer-bg.png" data-bg-color="#25272e">
    <div class="container pt-70 pb-40">
      <div class="row border-bottom-black">
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <img class="mt-10 mb-20" alt="" src="img/garmef_logo.png">
            <p>203, Envato Labs, Behind Alis Steet, Melbourne, Australia.</p>
            <ul class="list-inline mt-5">
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-phone text-theme-colored mr-5"></i> <a class="text-gray" href="#">123-456-789</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-envelope-o text-theme-colored mr-5"></i> <a class="text-gray" href="#">contact@yourdomain.com</a> </li>
              <li class="m-0 pl-10 pr-10"> <i class="fa fa-globe text-theme-colored mr-5"></i> <a class="text-gray" href="#">www.yourdomain.com</a> </li>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Latest News</h5>
            <div class="latest-posts">
              <article class="post media-post clearfix pb-0 mb-10">
                <a href="#" class="post-thumb"><img alt="" src="http://placehold.it/80x55"></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="#">Sustainable Construction</a></h5>
                  <p class="post-date mb-0 font-12">Mar 08, 2015</p>
                </div>
              </article>
              <article class="post media-post clearfix pb-0 mb-10">
                <a href="#" class="post-thumb"><img alt="" src="http://placehold.it/80x55"></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="#">Industrial Coatings</a></h5>
                  <p class="post-date mb-0 font-12">Mar 08, 2015</p>
                </div>
              </article>
              <article class="post media-post clearfix pb-0 mb-10">
                <a href="#" class="post-thumb"><img alt="" src="http://placehold.it/80x55"></a>
                <div class="post-right">
                  <h5 class="post-title mt-0 mb-5"><a href="#">Storefront Installations</a></h5>
                  <p class="post-date mb-0 font-12">Mar 08, 2015</p>
                </div>
              </article>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Useful Links</h5>
            <ul class="list angle-double-right list-border">
              <li><a href="#">Body Building</a></li>
              <li><a href="#">Fitness Classes</a></li>
              <li><a href="#">Weight lifting</a></li>
              <li><a href="#">Yoga Courses</a></li>
              <li><a href="#">Training</a></li>              
            </ul>
          </div>
        </div>
        <div class="col-sm-6 col-md-3">
          <div class="widget dark">
            <h5 class="widget-title line-bottom">Opening Hours</h5>
            <div class="opening-hours">
              <ul class="list-border">
                <li class="clearfix"> <span> Mon - Tues :  </span>
                  <div class="value pull-right"> 6.00 am - 10.00 pm </div>
                </li>
                <li class="clearfix"> <span> Wednes - Thurs :</span>
                  <div class="value pull-right"> 8.00 am - 6.00 pm </div>
                </li>
                <li class="clearfix"> <span> Fri : </span>
                  <div class="value pull-right"> 3.00 pm - 8.00 pm </div>
                </li>
                <li class="clearfix"> <span> Sun : </span>
                  <div class="value pull-right"> Colosed </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-10">
        <div class="col-md-5">
          <div class="widget dark">
            <h5 class="widget-title mb-10">Subscribe Us</h5>
            <!-- Mailchimp Subscription Form Starts Here -->
            <form id="mailchimp-subscription-form-footer" class="newsletter-form">
              <div class="input-group">
                <input type="email" value="" name="EMAIL" placeholder="Your Email" class="form-control input-lg font-16" data-height="45px" id="mce-EMAIL-footer" style="height: 45px;">
                <span class="input-group-btn">
                  <button data-height="45px" class="btn btn-colored btn-theme-colored btn-xs m-0 font-14" type="submit">Subscribe</button>
                </span>
              </div>
            </form>
            <!-- Mailchimp Subscription Form Validation-->
            <script type="text/javascript">
              $('#mailchimp-subscription-form-footer').ajaxChimp({
                  callback: mailChimpCallBack,
                  url: '//thememascot.us9.list-manage.com/subscribe/post?u=a01f440178e35febc8cf4e51f&amp;id=49d6d30e1e'
              });

              function mailChimpCallBack(resp) {
                  // Hide any previous response text
                  var $mailchimpform = $('#mailchimp-subscription-form-footer'),
                      $response = '';
                  $mailchimpform.children(".alert").remove();
                  console.log(resp);
                  if (resp.result === 'success') {
                      $response = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  } else if (resp.result === 'error') {
                      $response = '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + resp.msg + '</div>';
                  }
                  $mailchimpform.prepend($response);
              }
            </script>
            <!-- Mailchimp Subscription Form Ends Here -->
          </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <div class="widget dark">
            <h5 class="widget-title mb-10">Call Us Now</h5>
            <div class="text-gray">
              +61 3 1234 5678 <br>
              +12 3 1234 5678
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="widget dark">
            <h5 class="widget-title mb-10">Connect With Us</h5>
            <ul class="styled-icons icon-dark icon-theme-colored icon-circled icon-sm">
              <li><a href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-skype"></i></a></li>
              <li><a href="#"><i class="fa fa-youtube"></i></a></li>
              <li><a href="#"><i class="fa fa-instagram"></i></a></li>
              <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom bg-black-333">
      <div class="container pt-15 pb-10">
        <div class="row">
          <div class="col-md-6">
            <p class="font-11 text-black-777 m-0">Copyright &copy;2022 GARMEF. All Rights Reserved</p>
          </div>
          <div class="col-md-6 text-right">
            <div class="widget no-border m-0">
              <ul class="list-inline sm-text-center mt-5 font-12">
                <li>
                  <a href="#">FAQ</a>
                </li>
                <li>|</li>
                <li>
                  <a href="#">Help Desk</a>
                </li>
                <li>|</li>
                <li>
                  <a href="#">Support</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>

<script src="js/custom.js"></script>
<script type="text/javascript">
  
</script>
</body>

</html>