<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierGroup extends Model
{
    protected $guarded = [];
}
