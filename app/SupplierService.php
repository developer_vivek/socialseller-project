<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierService extends Model
{
    protected $guarded = [];
    public function getImageAttribute($value)
    {
        return url($value);
    }

}
