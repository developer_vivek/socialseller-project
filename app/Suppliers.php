<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suppliers extends Model
{
    protected $guarded = [];
    public function services()
    {
        return $this->belongsToMany('App\SupplierService', 'supplier_services');
    }
    public function supplier_images()
    {
        return $this->hasMany('App\SupplierImage');
    }
}
