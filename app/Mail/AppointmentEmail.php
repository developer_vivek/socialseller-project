<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AppointmentEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $mobile;
    public $name;
    public $time;
    public function __construct($name, $mobile, $time)
    {
        $this->mobile = $mobile;
        $this->name   = $name;
        $this->time   = $time;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('website.email_template');
    }
}
