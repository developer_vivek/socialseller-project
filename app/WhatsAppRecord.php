<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsAppRecord extends Model
{
    protected $guarded = [];
}
