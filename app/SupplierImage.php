<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierImage extends Model
{
    protected $guarded = [];
    public function getImageAttribute($value)
    {
        if ($value == null) {
            return $value;
        } else {
            return url($value);
        }

    }

}
