<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CheckUserLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $student_id = Session::get('student_id');
        if (!$student_id) {
            return redirect('login');
        }

        return $next($request);

    }
}
