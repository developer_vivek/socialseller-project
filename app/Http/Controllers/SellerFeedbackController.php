<?php

namespace App\Http\Controllers;

use App\SellerFeedback;
use DB;
use Illuminate\Http\Request;

class SellerFeedbackController extends Controller
{
    public function index()
    {
        $seller_feedback = SellerFeedback::all();
        return view('admin_panel/seller_feedback/index', compact('seller_feedback'));
    }
    // public function add_supplier()
    // {
    //     $services   = Service::where('status', 'active')->get();
    //     $categories = Category::where('status', 'active')->get();
    //     return view('admin_panel/suppliers/add', compact('services', 'categories'));

    // }
    public function store_seller_feedback(Request $request)
    {
        request()->validate([

            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:3048',

        ]);
        DB::beginTransaction();
        $image = $request->image;

        $dir = 'seller_feedback';

        if ($request->image) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path() . '/storage/' . $dir, $imageName);
            $data['image'] = '/storage/' . $dir . '/' . $imageName;
        }

        SellerFeedback::insert(['image' => $data['image']]);
        DB::commit();
        session("message", "successfully suppliers created");
        return redirect('admin/seller_feedbacks');
    }

    public function seller_feedback_delete(Request $request)
    {
        $id = $request->id;
        SellerFeedback::where('id', $id)->delete();
        return "Successfully deleted";
    }
}
