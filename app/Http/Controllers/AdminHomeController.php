<?php

namespace App\Http\Controllers;

use App\Course;
use App\CourseFor;
use App\PaymentDetail;
use App\User;
use App\WhatsAppRecord;
use DB;
use Illuminate\Http\Request;

class AdminHomeController extends Controller
{
    public function dashboard()
    {

        // $data['enroll_student_count']   = EnrollUser::selectRaw(' count("id") as count ')->groupBy('student_id')->get();
        $data['student_count']                      = User::count();
        $data['course_count']                       = Course::count();
        $data['razorpay_payment_count']             = PaymentDetail::where(['razorpay_status' => 'paid'])->sum('amount');
        $data['razorpay_payment_count_supplier']    = PaymentDetail::where(['razorpay_status' => 'paid'])->where('supplier_id', '!=', null)->sum('amount');
        $data['razorpay_payment_count_course']      = PaymentDetail::where(['razorpay_status' => 'paid'])->where('course_id', '!=', null)->sum('amount');
        $data['razorpay_payment_count_appointment'] = PaymentDetail::where(['razorpay_status' => 'paid'])->where('appointment_id', '!=', null)->sum('amount');

        // dd($data);
        return view('admin_panel.home.dashboard', $data);

    }
    public function who_can_learn_create()
    {
        return view('admin_panel.course_for.create');
    }
    public function who_can_learn_store(Request $request)
    {
        // $photo_file_path = $request->file('image_file')->storePublicly(
        //     'images/artworks/gallery-images/'.uniqid()
        //   );
        // $request->file('image')->storePublicly('my-file', 's3');
        // die;
        request()->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'  => 'required',

        ]);
        $dir          = 'course_for';
        $data['name'] = $request->input('name');

        if ($request->image) {
            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path() . '/storage/' . $dir, $imageName);
            $data['image'] = '/storage/' . $dir . '/' . $imageName;
        }
        // $data['status'] = 1;
        CourseFor::create($data);
        session()->flash('message', 'Successfully created');
        return redirect('admin/who_can_learn');

    }

    public function who_can_learn_list()
    {
        $course_for = CourseFor::all();
        return view('admin_panel.course_for.index', ['course_for' => $course_for]);
    }
    public function counts_setting()
    {
        $count = Count::where('id', 1)->first();
        return view('admin_panel.course_for.index', compact('count'));

    }
    public function add_whatsapp_mobile(Request $request)
    {
        $mobile = $request->mobile;
        WhatsAppRecord::create(['mobile' => $mobile]);
        return 1;
    }
    public function whatsapp_mobile_list()
    {
        // $user             = [];
        // $name             = "Vivek";
        // $mobile           = "7828161459";
        // $appointment_time = "2021-10-05 12.35";
        // echo date(' h:i A | l | d-m-Y', strtotime($appointment_time));
        // die;
        // $time = "12 nov 2019";
        // Mail::to("vivek.et1993@gmail.com")->send(new AppointmentEmail($mobile, $name, $time));

        // Mail::send('website.email_template', ['user' => $user], function ($m) use ($user) {
        //     // $m->from('hello@app.com', 'Your Application');

        //     $m->to("vivek.et1993@gmail.com")->subject('Your Reminder!');
        // });

        // // $details = array("name" => "vivek");
        // // dispatch(new SendEmailJob($details));
        // die;
        $data = WhatsAppRecord::orderBy('created_at', 'desc')->get();

        return view('admin_panel.whatsappData', compact('data'));

    }
    public function export_csv(Request $request)
    {

        if ($request->input('date_range')) {
            // print_r($request->input('date_range'));
            $date_range = explode('-', $request->input('date_range'));
            if (!empty($date_range[0])) {
                $exp_date = explode('/', $date_range[0]);
                if (sizeof($exp_date) > 1) {
                    $start_date = trim(trim($exp_date[2]) . '-' . trim($exp_date[1]) . '-' . trim($exp_date[0]));
                }

            }
            // print_r($start_date);

            if ($date_range[1]) {
                $exp_date2 = explode('/', $date_range[1]);
                // print_r($exp_date2);
                if (sizeof($exp_date2) > 1) {
                    $end_date = trim($exp_date2[2] . '-' . trim($exp_date2[1]) . '-' . trim($exp_date2[0]));
                }

            }
            // dd($start_date . ' ' . $end_date);
            $result = WhatsAppRecord::select(DB::raw('DISTINCT(mobile)'), 'created_at')
                ->whereBetween('created_at', [$start_date, $end_date])->orderBy('created_at', 'desc')
                ->get();
            // dd(array_unique($result));
            // echo '<pre>';
            // print_r($result);
            // die;
        } else {
            // get data
            // echo "without get";
            // $result = WhatsAppRecord::orderBy('created_at', 'desc')->get(['mobile', 'created_at']);
            $result = DB::select(DB::raw("select  distinct mobile from whats_app_records"));
            // echo count($result);
            // die;

        }
        // print_r($result);
        $filename = 'whatsAppDb_' . date('Ymd') . '.xls';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/xls;charset=utf-8");
        // die;
        $data = "<table><tr><th>Number</th></tr>";
        foreach ($result as $row) {
            $data .= "<tr>" . $row->mobile . "</tr>";
            // if (!$flag) {
            //     // display field/column names as first row
            //     echo implode("\t", array_keys($row)) . "\r\n";
            //     $flag = true;
            // }
            // array_walk($row, __NAMESPACE__ . '\cleanData');
            // echo implode("\t", array_values($row)) . "\r\n";
        }
        echo $data;
        exit;
        die;

        // file creation
        $file = fopen('php://output', 'w');
        // die;
        $header = array("id", "mobile", "created_at");
        fputcsv($file, $header);
        foreach ($result as $key => $line) {
            fputcsv($file, $line);
        }
        fclose($file);
        exit;
    }
    // public function export_csv(Request $request)
    // {

    //     if ($request->input('date_range')) {
    //         // print_r($request->input('date_range'));
    //         $date_range = explode('-', $request->input('date_range'));
    //         if (!empty($date_range[0])) {
    //             $exp_date = explode('/', $date_range[0]);
    //             if (sizeof($exp_date) > 1) {
    //                 $start_date = trim(trim($exp_date[2]) . '-' . trim($exp_date[1]) . '-' . trim($exp_date[0]));
    //             }

    //         }
    //         // print_r($start_date);

    //         if ($date_range[1]) {
    //             $exp_date2 = explode('/', $date_range[1]);
    //             // print_r($exp_date2);
    //             if (sizeof($exp_date2) > 1) {
    //                 $end_date = trim($exp_date2[2] . '-' . trim($exp_date2[1]) . '-' . trim($exp_date2[0]));
    //             }

    //         }
    //         // dd($start_date . ' ' . $end_date);
    //         $result = WhatsAppRecord::select(DB::raw('DISTINCT(mobile)'), 'created_at')
    //             ->whereBetween('created_at', [$start_date, $end_date])->orderBy('created_at', 'desc')
    //             ->get();
    //         // dd(array_unique($result));
    //         // echo '<pre>';
    //         // print_r($result);
    //         // die;
    //     } else {
    //         // get data
    //         // echo "without get";
    //         // $result = WhatsAppRecord::orderBy('created_at', 'desc')->get(['mobile', 'created_at']);
    //         $result = DB::select(DB::raw("select  distinct mobile from whats_app_records"));
    //         // echo count($result);
    //         // die;

    //     }
    //     // print_r($result);
    //     $filename = 'whatsAppDb_' . date('Ymd') . '.xls';
    //     header("Content-Description: File Transfer");
    //     header("Content-Disposition: attachment; filename=$filename");
    //     header("Content-Type: application/xls;charset=utf-8");
    //     // die;
    //     $data = "<table><tr><th>Number</th></tr>";
    //     foreach ($result as $row) {
    //         $data .= "<tr>" . $row->mobile . "</tr>";
    //         // if (!$flag) {
    //         //     // display field/column names as first row
    //         //     echo implode("\t", array_keys($row)) . "\r\n";
    //         //     $flag = true;
    //         // }
    //         // array_walk($row, __NAMESPACE__ . '\cleanData');
    //         // echo implode("\t", array_values($row)) . "\r\n";
    //     }
    //     echo $data;
    //     exit;
    //     die;

    //     // file creation
    //     $file = fopen('php://output', 'w');
    //     // die;
    //     $header = array("id", "mobile", "created_at");
    //     fputcsv($file, $header);
    //     foreach ($result as $key => $line) {
    //         fputcsv($file, $line);
    //     }
    //     fclose($file);
    //     exit;
    // }
    public function export_csv_emails(Request $request)
    {
        $result = DB::select(DB::raw("select  distinct email from users"));

        $filename = 'usersEmails_' . date('Ymd') . '.xls';
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$filename");
        header("Content-Type: application/xls;charset=utf-8");
        // die;
        $data = "<table><tr><th>Email</th></tr>";
        foreach ($result as $row) {
            $data .= "<tr>" . $row->email . "</tr>";
            // if (!$flag) {
            //     // display field/column names as first row
            //     echo implode("\t", array_keys($row)) . "\r\n";
            //     $flag = true;
            // }
            // array_walk($row, __NAMESPACE__ . '\cleanData');
            // echo implode("\t", array_values($row)) . "\r\n";
        }
        echo $data;
        exit;
        die;

        // file creation
        $file = fopen('php://output', 'w');
        // die;
        $header = array("id", "mobile", "created_at");
        fputcsv($file, $header);
        foreach ($result as $key => $line) {
            fputcsv($file, $line);
        }
        fclose($file);
        exit;
    }

}
