<?php

namespace App\Http\Controllers;

use App\AdminUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;
use Validator;

class AdminLoginController extends Controller
{
    public function admin_login()
    {
        // Session::forget('path');

        // dd(Session::get('path'));
        if (Session::get('social_user_id')) {
            return redirect('admin/dashboard');
        }
        return view('admin_panel.login');
    }

    public function login_auth(Request $request)
    {
        $data = $request->validate([
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        $admin = AdminUser::where(['email' => $data['email']])->first();
        if (!$admin) {
            session()->flash('error', 'Invalid Credentials');
            return redirect('auth')->withInput();
        }
        if (Hash::check($data['password'], $admin->password)) {
            // dd($admin . 'hiii');
            Session::put('social_user_id', $admin->id);

            Session::put('social_user_email', $admin->email);
            Session::put('social_user_name', $admin->name);
            Session::put('social_role_id', $admin->role_id);
            // Session::put('gonext_user_role', $admin->role_id);
            Session::save();
            return redirect('admin/dashboard');
        }
        session()->flash('error', 'Invalid Credentials');
        return redirect('admin/login');
    }
    public function forgot_password()
    {
        return view('admin_panel/forgot_password');
    }
    public function send_mail_to_id(Request $request)
    {
        request()->validate([
            'email' => 'required|email',

        ]);

        $email = $request->input('email');
        ##check email exist or not
        $user = AdminUser::where('email', $email)->first();
        // dd($user);
        if (!$user) {
            // return Redirect::back()->withErrors('msg', 'This Email is not exist');
            session()->flash('error', 'This Email is not exist');

            return back()
                ->withInput();

        }
        $token = md5(rand(1, 10) . microtime());
        AdminUser::where('id', $user->id)->update(['token' => $token]);

        $link = url("/reset_password/" . $token);
        Mail::to($user->email)->send(new ForgotPassword($user->name, $link));
        // echo "send email";
        session()->flash('error', 'Email is sucessfully send ');

        return back()
            ->withInput();

    }
    public function reset_password_page($token = '')
    {
        if ($token == '' && $token == null) {
            return "Invalid Token";
        }
        return view('reset_password.index', ['token' => $token]);

    }
    public function submit_reset_password(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
        ]);
        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate)->withInput();
        }

        $token = $request->input('token');
        $user  = AdminUser::where('token', $token)->first();

        if ($user) {
            $password = Hash::make($request->input('password'));

            $user->token    = null;
            $user->password = $password;
            $user->save();

            return Redirect::back()->with('success', 'Password has successfully changed');

        }
        return Redirect::back()->with('error', 'Your Token is expired');

    }

    public function logout()
    {
        Session::flush();
        Session::save();
        return redirect('admin/login');
    }
}
