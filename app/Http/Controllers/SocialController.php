<?php

namespace App\Http\Controllers;

// use Socialite;
use App\Mail\NewUserRegistration;
use App\Social;
use App\User;
use Laravel\Socialite\Facades\Socialite;
use Mail;
use Session;

class SocialController extends Controller
{
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function redirectTofacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function handleFacebookCallback()
    {
        $user = Socialite::driver('facebook')->user();

        $condition = array('provider' => 'Facebook', 'provider_id' => $user->id);
        $social    = Social::select('provider', 'provider_id', 'id', 'user_id')->where($condition)->first();
        if ($social) {
            ##check user email exist in db users table
            $student = User::where('email', $user->email)->first();
            ##case 1: social credential and user credential are exists
            if ($student) {
                $student->name = $user->name;
                $student->save();
            }
            ##case 2 :social credential exists but user credentials are not exists
            else {
                $student                    = new User;
                $student->email             = $user->email;
                $student->name              = $user->name;
                $student->email_verified_at = date('Y-m-d H:i:s');
                $student->save();

                // $social->user_id = $student->id;
                // $social->save();
            }
        }

##case 3: social credential not exists but users exists
        else {
            $student = User::where('email', $user->email)->first();
            if ($student) {
                $social              = new Social;
                $social->user_id     = $student->id;
                $social->provider    = 'Facebook';
                $social->provider_id = $user->id;
                $social->save();
                //$student->image = $request->image;
                $student->name = $user->name;
                // $student->status            = '1';
                $student->email_verified_at = date('Y-m-d H:i:s');
                $student->save();
            }
            ##case 4: social credential not exists and users also not exists
            else {
                $student        = new User;
                $student->email = $user->email;

                $student->name = $user->name;

                // $student->status            = '1';
                // $student->email_verified_at = date('Y-m-d H:i:s');
                // $student->categories=$request->input('categories');

                $student->save();
                $social              = new Social;
                $social->user_id     = $student->id;
                $social->provider    = 'Facebook';
                $social->provider_id = $user->id;
                $social->save();
                Mail::to($user->email)->queue(new NewUserRegistration($user->name));

            }
        }

        Session::put('student_id', $student->id);
        $path = Session::get('path');
        if (isset($path)) {
            Session::forget('path');
            return redirect($path);
        }

        return redirect('dashboard');

    }
    public function handleGoogleCallback()
    {

        $user = Socialite::driver('google')->user();
        // dd($user);
        $condition = array('provider' => 'Google', 'provider_id' => $user->id);
        $social    = Social::select('provider', 'provider_id', 'id', 'user_id')->where($condition)->first();
        if ($social) {
            ##check user email exist in db users table
            $student = User::where('email', $user->email)->first();
            ##case 1: social credential and user credential are exists
            if ($student) {
                $student->name = $user->name;
                $student->save();
            }
            ##case 2 :social credential exists but user credentials are not exists
            else {
                $student                    = new User;
                $student->email             = $user->email;
                $student->name              = $user->name;
                $student->email_verified_at = date('Y-m-d H:i:s');
                $student->save();

                // $social->user_id = $student->id;
                // $social->save();
            }
        }

        ##case 3: social credential not exists but users exists
        else {
            $student = User::where('email', $user->email)->first();
            if ($student) {
                $social              = new Social;
                $social->user_id     = $student->id;
                $social->provider    = 'Google';
                $social->provider_id = $user->id;
                $social->save();
                //$student->image = $request->image;
                $student->name = $user->name;
                // $student->status            = '1';
                $student->email_verified_at = date('Y-m-d H:i:s');
                $student->save();
            }
            ##case 4: social credential not exists and users also not exists
            else {
                $student        = new User;
                $student->email = $user->email;

                $student->name = $user->name;

                // $student->status            = '1';
                // $student->email_verified_at = date('Y-m-d H:i:s');
                // $student->categories=$request->input('categories');

                $student->save();
                $social              = new Social;
                $social->user_id     = $student->id;
                $social->provider    = 'Google';
                $social->provider_id = $user->id;
                $social->save();
            }
        }

        Session::put('student_id', $student->id);
        $path = Session::get('path');
        if (isset($path)) {
            Session::forget('path');
            return redirect($path);
        }

        return redirect('dashboard');
        // $studentExist = User::where('email', $user->email)->first();
        // if (!$studentExist) {
        //     $student        = new User();
        //     $student->email = $user->email;
        //     $student->save();

        // }

    }

}
