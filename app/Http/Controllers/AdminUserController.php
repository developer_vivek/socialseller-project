<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Course;
use App\EnrollUser;
use App\SupplierAssign;
use App\User;
use DB;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    public function registered_users()
    {
        $users = User::orderBy('created_at', 'desc')->get();
        return view('admin_panel/users/index', compact('users'));
    }
    public function enrolled_users()
    {
        $users = EnrollUser::select('enroll_users.student_id', 'users.name', 'users.mobile', 'users.gender', DB::Raw('count("enroll_users.course_id") course_count'))->join('users', 'enroll_users.student_id', '=', 'users.id')->groupBy('enroll_users.student_id')->get();
        // dd($users);
        // foreach ($users as $row) {
        //     $row->course_count = DB::table('enroll_users')->where('enroll_users.student_id', $row->student_id)->count();
        // }
        // $users = EnrollUser::selectRaw('courses.title as course_name,enroll_users.*,users.name,users.email,users.mobile,users.gender')->join('users', 'enroll_users.student_id', '=', 'users.id')->join('courses', 'enroll_users.course_id', '=', 'courses.id')->get();
        return view('admin_panel/users/enroll_users', compact('users'));
    }

    public function user_details($user_id)
    {
        $data['user']            = User::where('id', $user_id)->first();
        $data['course_enroll']   = EnrollUser::selectRaw('courses.title as course_name,enroll_users.course_id')->where('enroll_users.student_id', $user_id)->join('courses', 'enroll_users.course_id', '=', 'courses.id')->get();
        $data['supplier_assign'] = SupplierAssign::where('user_id', $user_id)->join('suppliers', 'supplier_assigns.supplier_id', '=', 'suppliers.id')->get();
        $data['appointments']    = Appointment::where('user_id', $user_id)->get();

        $course_ids     = EnrollUser::where('student_id', $user_id)->pluck('course_id')->toArray();
        $data['course'] = Course::select('id', 'title')->where('status', 'active')->whereNotIn('id', $course_ids)->get();
        // echo '<pre>';
        // print_r($data);
        // die;
        return view('admin_panel/users/show', compact('data'));

    }

    public function assign_course_to_student(Request $request)
    {
        $student_id = $request->student_id;
        $course_id  = $request->course_id;
        $exist      = EnrollUser::where(['student_id' => $student_id, 'course_id' => $course_id])->first();
        if ($exist) {
            return response()->json(array('message' => "This course is already exist in dashboard", "status" => 0));
        }
        EnrollUser::create(['student_id' => $student_id, 'course_id' => $course_id]);
        return response()->json(array('message' => "Successfully Course Assigned ", "status" => 1));

    }
}
