<?php

namespace App\Http\Controllers;

use App\EnrollUser;
use App\SupplierAssign;
use App\SupplierGroup;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;

class UserController extends Controller
{

    public function dashboard()
    {
        $student_id = Session::get('student_id');

        $student = User::where('id', $student_id)->first();
        $courses = EnrollUser::where('student_id', $student_id)
            ->join('courses', 'enroll_users.course_id', '=', 'courses.id')
// ->join('lession', 'courses_id', '=', 'lessions.course.id')
            ->get();
        foreach ($courses as $row) {
            $row->image         = url($row->image);
            $row->category_name = DB::table('categories')->where('id', $row->category)->pluck('name')->first();
        }
        // dd($courses);
        return view('website/dashboard', compact('student', 'courses'));

    }

    public function profile()
    {
        $student_id = Session::get('student_id');

        $student = User::where('id', $student_id)->first();
        // dd($student);
        return view('website/profile', compact('student'));
    }

    public function profile_update(Request $request)
    {
        request()->validate([
            'image'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // 'email'  => 'required|email',
            'name'   => 'required',
            'gender' => 'required',
        ]);

        // $data['email']  = $request->email;
        // $data['mobile'] = $request->mobile;
        $data['name']   = $request->name;
        $data['gender'] = $request->gender;
        $dir            = 'Student';
        if ($request->image) {
            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path() . '/storage/' . $dir, $imageName);
            $data['profile_image'] = '/storage/' . $dir . '/' . $imageName;
        }

        $student_id = Session::get('student_id');
        if ($request->password) {
            $data['password'] = Hash::make($request->password);
        }
        User::where('id', $student_id)->update($data);
        session()->flash('message', 'Successfully profile update');

        return redirect('profile');
    }
    public function my_courses()
    {
        $student_id = Session::get('student_id');

        $courses = EnrollUser::where('student_id', $student_id)
            ->join('courses', 'enroll_users.course_id', '=', 'courses.id')
        // ->join('lession', 'courses_id', '=', 'lessions.course.id')
            ->get();
        // dd($courses);
        return view('website/course', compact('courses'));

    }
    public function my_suppliers()
    {

        $student_id = Session::get('student_id');
        // $student_id = 1;
        $suppliers = SupplierAssign::selectRaw('suppliers.*,supplier_categories.name as category_name')->where(['user_id' => $student_id])
            ->join('suppliers', 'supplier_assigns.supplier_id', '=', 'suppliers.id')
            ->join('supplier_categories', 'suppliers.category', '=', 'supplier_categories.id')
            ->get();
        // $comments = Supplier::find(12);/
        // dd($comments->services);
        foreach ($suppliers as $row) {
            $row->services = DB::table('supplier_services')->select('services.name')->where('supplier_id', $row->id)->join('services', 'supplier_services.service_id', '=', 'services.id')->get();
            // $row->supplier_image = SupplierImage::where('supplier_id', $row->id)->get();
        }
        // echo 1;
        // dd($supplier);

        // foreach()
        return view('website/suppliers', compact('suppliers'));

    }
    public function supplier_detail(Request $request, $supplier_id)
    {

        $supplierGroup = SupplierGroup::where('supplier_id', $supplier_id)->get();
        return view('website/supplier_detail', compact('supplierGroup'));

    }

}
