<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use App\CourseFor;
use App\EnrollUser;
use App\Langauge;
use App\Lesson;
use App\User;
use DB;
use Illuminate\Http\Request;
use PDF;
use Session;

class CourseController extends Controller
{

    public function index()
    {
        $courses = Course::selectRaw('courses.*,categories.name,langauges.name as langauge_name')->join('categories', 'courses.category', '=', 'categories.id')->join('langauges', 'courses.langauge', '=', 'langauges.id')->get();
        return view('admin_panel/courses/index', compact('courses'));
    }
    public function add_course()
    {
        $langauges  = Langauge::all();
        $coursefor  = CourseFor::all();
        $categories = Category::where('status', 'active')->get();
        return view('admin_panel/courses/create', compact('langauges', 'categories', 'coursefor'));
    }
    public function edit_course($course_id)
    {
        $langauges          = Langauge::all();
        $coursefor          = CourseFor::all();
        $categories         = Category::where('status', 'active')->get();
        $course             = Course::where('id', $course_id)->first();
        $course->course_for = explode(',', $course->course_for);
        return view('admin_panel/courses/edit', compact('langauges', 'categories', 'coursefor', 'course'));
    }
    public function update_course(Request $request)
    {
        request()->validate([
            'course_image'             => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title'                    => 'required',
            'description'              => 'required',
            'discount_price'           => 'required',
            'total_price'              => 'required',
            'course_url'               => 'required',
            'how_this_course_help_you' => 'required',
            'what_will_you_get'        => 'required',
            'what_you_learn'           => 'required',

            // 'drop_add.*.drop_longitude' => 'required',

        ]);

        DB::beginTransaction();
        $dir = 'course';
        if ($request->course_image) {
            $imageName = time() . '.' . request()->course_image->getClientOriginalExtension();
            request()->course_image->move(public_path() . '/storage/' . $dir, $imageName);
            $data['image'] = '/storage/' . $dir . '/' . $imageName;
        }
        $dir = 'course_thumbnail';
        if ($request->video_thumbnail) {
            $imageName = time() . '.' . request()->video_thumbnail->getClientOriginalExtension();
            request()->video_thumbnail->move(public_path() . '/storage/' . $dir, $imageName);
            $data['video_thumbnail'] = '/storage/' . $dir . '/' . $imageName;
        }
        $course_id = $request->course_id;
// dd($request);
        $data['title']                         = $request->title;
        $data['description']                   = $request->description;
        $data['how_this_course_will_help_you'] = $request->how_this_course_help_you;
        $data['what_you_learn']                = $request->what_you_learn;
        $data['what_will_you_get']             = $request->what_will_you_get;
        $data['total_price']                   = $request->total_price;
        $data['discount_price']                = $request->discount_price;
        $data['course_url']                    = $request->course_url;
        $data['is_certificate']                = $request->is_certificate ? $request->is_certificate : 0;
        $data['is_free']                       = $request->is_free ? $request->is_free : 0;
        $data['author']                        = $request->author;
        $data['discount_price']                = $request->discount_price;

        $data['is_lifetime_access'] = $request->is_lifetime_access ? $request->is_lifetime_access : 0;
        $data['validity_in_days']   = $request->validity_in_days;
        $data['total_hours']        = $request->total_hours;
        // $data['ratings']            = $request->ratings;
        $data['total_lesson']     = $request->total_lesson;
        $data['total_enrollment'] = $request->total_enrollment;
        $data['course_for']       = implode(',', $request->who_can_learn_from_this_course);

// dd($data);
        $data['category'] = $request->category;
        $data['langauge'] = $request->langauge;
        $courseInsert     = Course::where('id', $course_id)->update($data);

        DB::commit();
        session()->flash('message', 'Course update succesfully');

        return redirect('admin/courses');

    }
    public function store_course(Request $request)
    {
        request()->validate([
            'course_image'             => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'title'                    => 'required',
            'description'              => 'required',
            'discount_price'           => 'required',
            'total_price'              => 'required',
            'course_url'               => 'required',
            'how_this_course_help_you' => 'required',
            'what_will_you_get'        => 'required',
            'what_you_learn'           => 'required',

            // 'drop_add.*.drop_longitude' => 'required',

        ]);

        DB::beginTransaction();
        $dir = 'course';
        if ($request->course_image) {
            $imageName = time() . '.' . request()->course_image->getClientOriginalExtension();
            request()->course_image->move(public_path() . '/storage/' . $dir, $imageName);
            $data['image'] = '/storage/' . $dir . '/' . $imageName;
        }
        $dir = 'course_thumbnail';
        if ($request->video_thumbnail) {
            $imageName = time() . '.' . request()->video_thumbnail->getClientOriginalExtension();
            request()->video_thumbnail->move(public_path() . '/storage/' . $dir, $imageName);
            $data['video_thumbnail'] = '/storage/' . $dir . '/' . $imageName;
        }

        // dd($request);
        $data['title']                         = $request->title;
        $data['description']                   = $request->description;
        $data['how_this_course_will_help_you'] = $request->how_this_course_help_you;
        $data['what_you_learn']                = $request->what_you_learn;
        $data['what_will_you_get']             = $request->what_will_you_get;
        $data['total_price']                   = $request->total_price;
        $data['discount_price']                = $request->discount_price;
        $data['course_url']                    = $request->course_url;
        $data['is_certificate']                = $request->is_certificate ? $request->is_certificate : 0;
        $data['is_free']                       = $request->is_free ? $request->is_free : 0;
        $data['author']                        = $request->author;
        $data['discount_price']                = $request->discount_price;

        $data['is_lifetime_access'] = $request->is_lifetime_access ? $request->is_lifetime_access : 0;
        $data['validity_in_days']   = $request->validity_in_days;
        $data['total_hours']        = $request->total_hours;
        $data['ratings']            = $request->ratings;
        $data['total_lesson']       = $request->total_lesson;
        $data['total_enrollment']   = $request->total_enrollment;
        $data['course_for']         = implode(',', $request->who_can_learn_from_this_course);

        // dd($data);
        $data['category'] = $request->category;
        $data['langauge'] = $request->langauge;
        $courseInsert     = Course::create($data);

        DB::commit();
        session()->flash('message', 'Course created succesfully');

        return redirect('admin/courses');

    }
    public function change_status_course(Request $request)
    {
        $course_id = $request->course_id;
        $status    = $request->status;
        Course::where('id', $course_id)->update(['status' => $status]);
        return response()->json(["message" => "Sucessfully course status changed", "status" => 1]);
    }

    public function view_lesson($id)
    {
        // $lessons = array();
        $course_id = $id;
        $lessons   = Lesson::where('course_id', $id)->get();
        return view('admin_panel/courses/lesson', compact('lessons', 'course_id'));
    }
    public function add_lesson(Request $request)
    {
        request()->validate([
            'course_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'course_id'    => 'required',

            // 'l_url'         => 'required|array|min:1',
            // 'l_title'       => 'required|array|min:1',
            // 'l_description' => 'required|array|min:1',
            // 'l_url.*'         => 'required',
            // 'l_title.*'       => 'required',
            // 'l_description.*' => 'required',
            // 'drop_add.*.drop_longitude' => 'required',

        ]);

        DB::beginTransaction();

        $l_id = $request->l_id;
        $dir  = 'lesson';
        if ($l_id) {
            request()->validate([
                'l_url'           => 'required|array|min:1',
                'l_title'         => 'required|array|min:1',
                'l_description'   => 'required|array|min:1',
                'l_url.*'         => 'required',
                'l_title.*'       => 'required',
                'l_description.*' => 'required',

            ]);

            $l_title          = $request->l_title;
            $l_description    = $request->l_description;
            $l_url            = $request->l_url;
            $length_lesson_id = count($l_id);
            for ($j = 0; $j < $length_lesson_id; $j++) {
                $data['title']       = $l_title[$j];
                $data['description'] = $l_description[$j];
                $data['url']         = $l_url[$j];
                $data                = array('title' => $l_title[$j], 'description' => $l_description[$j], 'url' => $l_url[$j]);
                if (isset($lesson_image[$j])) {
                    $imageName = time() . '.' . $lesson_image[$j]->getClientOriginalExtension();
                    $lesson_image[$j]->move(public_path() . '/storage/' . $dir, $imageName);
                    $image = '/storage/' . $dir . '/' . $imageName;
                    $data  = array('title' => $l_title[$j], 'description' => $l_description[$j], 'url' => $l_url[$j], 'image' => $image);

                }
                Lesson::where('id', $l_id[$j])->update($data);
                // $data['image'] = '';
                // echo $data['image'];
            }

        }
        $course_id      = $request->course_id;
        $n_title        = $request->n_title;
        $lesson_image   = $request->lesson_image;
        $n_lesson_image = $request->n_lesson_image;
        // print_r($request->lesson_image);
        // die;
        if ($n_title) {
            $n_description = $request->n_description;
            $n_url         = $request->n_url;
            $length        = count($n_title);
            for ($i = 0; $i < $length; $i++) {
                $n_data = array('course_id' => $course_id, 'title' => $n_title[$i], 'description' => $n_description[$i], 'url' => $n_url[$i]);
                if (isset($n_lesson_image[$i])) {
                    $imageName = time() . '.' . $n_lesson_image[$i]->getClientOriginalExtension();
                    $n_lesson_image[$i]->move(public_path() . '/storage/' . $dir, $imageName);
                    $n_image = '/storage/' . $dir . '/' . $imageName;
                    $n_data  = array('course_id' => $course_id, 'title' => $n_title[$i], 'description' => $n_description[$i], 'url' => $n_url[$i], 'image' => $n_image);

                }

                Lesson::create($n_data);
            }
        }

        DB::commit();
        return redirect('admin/lessons' . '/' . $course_id);
    }
    public function certificate_download(Request $request)
    {

        $course_id = $request->course_id;
        // $course_id = 4;
        ##course exist or not check
        $courseCheck = Course::where('id', $course_id)->first();
        if (!$courseCheck) {
            return 'Course not found';
        }
        $student_id           = Session::get('student_id');
        $student              = User::where('id', $student_id)->first();
        $data['student_name'] = $student->name;
        $data['course_name']  = $courseCheck->title;

        // dd($data);
        ##check student is assign to this course or not
        $enrollUser = EnrollUser::where(['student_id' => $student_id, 'course_id' => $course_id])->first();
        if (!$enrollUser) {
            return "You are Not Enroll this course you can not Download certificate";
        }

        if ($enrollUser->completed_at == null) {
            $enrollUser->completed_at = now();
            $enrollUser->save();
        }
        $enrollUser           = EnrollUser::where(['student_id' => $student_id, 'course_id' => $course_id])->first();
        $data['completed_at'] = date('d M Y', strtotime($enrollUser->completed_at));
        // return view('website.pdf.certificate', $data);
        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('website.pdf.certificate', $data)->setPaper('a4', 'portrait');
        // return $pdf->stream();

        return $pdf->download('certificate' . now() . '.pdf');

    }

}
