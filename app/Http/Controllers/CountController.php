<?php

namespace App\Http\Controllers;

use App\Count;
use DB;
use Illuminate\Http\Request;

class CountController extends Controller
{
    public function settings()
    {
        // $counts = Count::where('id', 1)->first();
        $counts = Count::find(1);
        return view('admin_panel/counts_setting', compact('counts'));

    }
    public function update_settings(Request $request)
    {
        $data = request()->validate([
            'resellers'           => 'required',
            'connects'            => 'required',
            'suppliers'           => 'required',
            'students'            => 'required',
            'views'               => 'required',
            'subscriber'          => 'required',
            'followers'           => 'required',
            'shares'              => 'required',
            'satisfied_customers' => 'required',
            'complete_projects'   => 'required',
            'cup_of_coffee'       => 'required',
            'award_won'           => 'required',

        ]);
        // dd($data);
        DB::beginTransaction();
        Count::where('id', 1)->update($data);
        DB::commit();
        session("message", "Successfully update");
        return redirect('admin/settings');
    }
}
