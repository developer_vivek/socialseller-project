<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $blogs = Blog::get();
        return view('admin_panel/blogs/index', compact('blogs'));
    }

    public function add_blog(Request $request)
    {
        request()->validate([
            'url'   => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $data['url'] = ucfirst($request->url);
        $dir         = 'blogs';
        if ($request->image) {
            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path() . '/storage/' . $dir, $imageName);
            $data['image'] = '/storage/' . $dir . '/' . $imageName;
        }

        Blog::create($data);
        session()->flash('message', 'Blog created succesfully');

        return redirect('admin/blogs');
    }
    public function edit_Blog(Request $request)
    {
        request()->validate([
            'url' => 'required',
        ]);

        $name = ucfirst($request->url);
        $id   = $request->id;
        Blog::where('id', $id)->update(['url' => $name]);
        return 'Succefully update';
    }
    public function delete_blog(Request $request)
    {
        $id        = $request->id;
        $blogExist = Blog::where('id', $id)->first();
        if (!$blogExist) {
            return "Blog Not Exist";
        }
        $filename = $blogExist->getOriginal('image');

        if (file_exists(public_path() . $filename)) {
            \File::delete($blogExist->image);
        }
        Blog::where('id', $id)->delete();
        return 1;
    }
    public function change_status(Request $request)
    {
        $status = $request->status;
        $id     = $request->id;
        Blog::where('id', $id)->update(['status' => $status]);
        return 1;
    }
}
