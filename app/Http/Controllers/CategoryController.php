<?php

namespace App\Http\Controllers;

use App\Category;
use App\SupplierCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::where(['status' => 'active'])->get();
        return view('admin_panel/categories/index', compact('categories'));
    }
    // public function add_category_view()
    // {
    //     return view('admin_panel/categories/add_category');

    // }
    public function add_category(Request $request)
    {
        request()->validate([
            'name' => 'required',
        ]);

        $name = ucfirst($request->name);
        Category::create(['name' => $name]);
        return redirect('admin/categories');
    }
    public function edit_category(Request $request)
    {
        request()->validate([
            'name' => 'required',
        ]);

        $name = ucfirst($request->name);
        $id   = $request->id;
        Category::where('id', $id)->update(['name' => $name]);
        return 'Succefully update';
    }
    public function supplier_category_list()
    {
        $categories = SupplierCategory::where(['status' => 'active'])->get();
        return view('admin_panel/supplier_categories/index', compact('categories'));
    }
    // public function add_category_view()
    // {
    //     return view('admin_panel/categories/add_category');

    // }
    public function add_supplier_category(Request $request)
    {
        request()->validate([
            'name' => 'required',
        ]);

        $name = ucfirst($request->name);
        SupplierCategory::create(['name' => $name]);
        return redirect('admin/supplier_categories');
    }
    public function edit_supplier_category(Request $request)
    {
        request()->validate([
            'name' => 'required',
        ]);

        $name = ucfirst($request->name);
        $id   = $request->id;
        SupplierCategory::where('id', $id)->update(['name' => $name]);
        return 'Succefully update';
    }
    public function change_status(Request $request)
    {
        $status = $request->status;
        $id     = $request->id;
        Category::where('id', $id)->update(['status' => $status]);
        return 1;
    }
}
