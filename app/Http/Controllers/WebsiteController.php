<?php

namespace App\Http\Controllers;

use App\AppointmentVideo;
use App\Blog;
use App\Count;
use App\Course;
use App\CourseFor;
use App\EnrollUser;
use App\Lesson;
use App\Supplier;
use App\SupplierCategory;
use App\SupplierService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;

class WebsiteController extends Controller
{
    public function login($path = '', $id = '')
    {
        $student_id = Session::get('student_id');
        if (isset($student_id)) {
            return redirect('dashboard');

        } else {
            return view('website/login', compact('path', 'id'));
        }
    }
    public function home()
    {
        $courses = Course::selectRaw('courses.*,categories.name')->join('categories', 'courses.category', '=', 'categories.id')->where('courses.status', 'Active')->get();
        // dd($courses);
        $free_courses = Course::selectRaw('courses.*,categories.name')->where('is_free', 1)->join('categories', 'courses.category', '=', 'categories.id')->where('courses.status', 'Active')->get();
        $blogs        = Blog::all();
        $suppliers    = Supplier::selectRaw('suppliers.*,supplier_categories.name as category_name')->join('supplier_categories', 'suppliers.category', '=', 'supplier_categories.id')->where('suppliers.status', 'Active')->get();
        foreach ($suppliers as $row) {
            $row->supplier_services = SupplierService::select('services.name as service_name')->where('supplier_id', $row->id)->join('services', 'supplier_services.service_id', '=', 'services.id')->get();

        }
        $count = Count::where('id', 1)->first();
        $msg   = "Thankyou For Connecting with us 😀
Save our Whatsapp Number +916260276922

Here are the list of Free Videos from Lakshit Sir.

How to sell Products on Youtube ?
https://www.youtube.com/watch?v=Egmt3JGvA8w

How to sell products on whatsapp?
https://www.youtube.com/watch?v=m9SUQxMIe2Q&t=489s


How to Create Ecommerce website for Online Business ?
https://www.youtube.com/watch?v=IntwQOam42g&t=29s


How to sell products on Instagram ?
https://www.youtube.com/watch?v=VeyyNPXpq8I&t=33s


How to sell products on Facebook ?
https://www.youtube.com/watch?v=Q-SLSs5wSWE&t=90s

How to do Instagram Marketing ?
https://www.youtube.com/watch?v=0ocRHZkM2OM

How to Deal with Customers in Whatsapp ?
https://www.youtube.com/watch?v=ZsdF4sjfgfA&t=12s

Manage Customers in Whatsapp ?
https://www.youtube.com/watch?v=-_AmMG7qtyE&t=8s

How to do online Courier in COD ?
https://www.youtube.com/watch?v=IntwQOam42g&t=29s";

        // $msg = rawurlencode($msg);
        $msg = rawurlencode($msg);
        // dd($msg);
        return view('website/index', compact('courses', 'free_courses', 'blogs', 'suppliers', 'count', 'msg'));
    }
    public function login_with_password()
    {
        return view('website/login_with_password');
    }
    public function signup()
    {
        return view('website/signup');
    }
    public function enroll_inside()
    {
        return view('website/enrollinside');
    }
    public function user_course_details($course_id)
    {
        $student_id = Session::get('student_id');

        $UserEnroll = EnrollUser::where(['course_id' => $course_id, 'student_id' => $student_id])->first();
        if (!$UserEnroll) {
            abort(401, 'Not Permitted');
        }

        $course = Course::selectRaw('courses.*,langauges.name as langauge_name')->where('courses.id', $course_id)->join('langauges', 'courses.langauge', '=', 'langauges.id')->first();
        $lesson = Lesson::where('course_id', $course_id)->get();
        return view('website/courses', compact('course', 'lesson'));

    }
    public function course_details($id)
    {
        $count        = Count::where('id', 1)->first();
        $course       = Course::selectRaw('courses.*,langauges.name as langauge_name')->where('courses.id', $id)->join('langauges', 'courses.langauge', '=', 'langauges.id')->first();
        $course_views = $course->viewer;
        // dd($course_views);
        Course::where('id', $id)->update(['viewer' => $course_views + 1]);
        $blogs     = Blog::all();
        $courseFor = explode(',', $course->course_for);
// $data['course_for'] = array();
        foreach ($courseFor as $value) {
            $course_for[] = CourseFor::selectRaw('id,name,image')->where('id', $value)->first();
        }
        $course_structure = Lesson::where('course_id', $id)->get();
        $student_id       = Session::get('student_id');
        $is_enroll        = 0;
        if ($student_id) {
            $UserEnroll = EnrollUser::where(['course_id' => $id, 'student_id' => $student_id])->first();
            if ($UserEnroll) {
                $is_enroll = 1;
            }
        }

// dd($course_for);
        return view('website/courses_details', compact('course', 'course_for', 'blogs', 'count', 'is_enroll', 'course_structure'));

    }
    public function all_suppliers()
    {
        $blogs               = Blog::all();
        $count               = Count::where('id', 1)->first();
        $supplier_categories = SupplierCategory::where('status', 'Active')->get();
        foreach ($supplier_categories as $row) {
            $row->supplier = Supplier::where('category', $row->id)->where('status', 'Active')->get();
            foreach ($row->supplier as $row2) {
                $row2->supplier_service = SupplierService::select('services.name as service_name')->where('supplier_id', $row2->id)->join('services', 'supplier_services.service_id', '=', 'services.id')->get();
            }
        }
        // dd($supplier_categories);
        return view('website/supplier_landing', compact('blogs', 'count', 'supplier_categories'));

    }
    public function free_courses()
    {
        $course = Course::where(['is_free' => 1])->where('status', 'Active')->get();
        return view('website/free_courses', compact('course'));

    }
    public function courses()
    {
        $courses = Course::where(['is_free' => 0])->where('status', 'Active')->get();
        return view('website/all_courses', compact('courses'));

    }
    public function free_videos()
    {
        // $courses = Course::where(['is_free' => 0])->get();
        $blogs = Blog::all();
        return view('website/free_videos', compact('blogs'));

    }
    public function appointment()
    {
        $appointment_videos = AppointmentVideo::all();
        return view('website/appointment', compact("appointment_videos"));

    }
    public function forgot_password()
    {
        return view('website/forgot_password');

    }
    public function reset_password(Request $request)
    {
        $mobile = $request->mobile;
        $otp    = rand(1000, 9999);

        $user = User::where('mobile', $mobile)->first();
        if (!$user) {
            session()->flash('error', 'Mobile Number not found');

            return redirect('forgot_password')->withInput($request->all());

        }
        User::where('mobile', $mobile)->update(['otp' => $otp]);
        ##send sms
        $message = 'Your Four Digit social seller Otp is ' . $otp;
        SendSmsController::Send_message($mobile, $message);
        return view('website/reset_password', compact('mobile'));

    }
    public function set_new_password(Request $request)
    {
        request()->validate([

            'mobile'       => 'required',
            'otp'          => 'required',
            'new_password' => 'required',

        ]);

        $mobile           = $request->mobile;
        $otp              = $request->otp;
        $new_password     = $request->new_password;
        $confirm_password = $request->confirm_password;
        $user             = User::where(['mobile' => $mobile])->first();
        if (!$user) {
            session()->flash('error', 'Mobile Number not found');
            return redirect('reset_password')->withInput($request->all());

        }
        $otpMatch = User::where(['mobile' => $mobile, 'otp' => $otp])->first();
        if (!$otpMatch) {
            session()->flash('error', 'Otp is not correct');
            // return back()->withInput();
            return redirect('reset_password')->withInput($request->all());

        }
        $password = Hash::make($new_password);
        User::where(['mobile' => $mobile, 'otp' => $otp])->update(['password' => $password]);
        return redirect('login');
    }
    public function privacy_policy()
    {
        return view('website/privacy_policy');
    }
    public function data_deletion_url(Request $request)
    {
        return response()->json(['url' => 'socialseller.in'], 200);
    }
    public function feedback()
    {
        return view('website/feedback_page');
    }

    public function course_enroll()
    {
        return view('website/course_enroll');

    }
    public function terms_condition()
    {
        return view('website/terms&condition');

    }

}
