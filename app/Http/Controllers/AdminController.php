<?php

namespace App\Http\Controllers;

use App\AdminUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;

class AdminController extends Controller
{

    public function user_profile()
    {

        if (Session::get('social_role_id') != 1) {
            abort('403', 'You have not permission to access this');
        }

        $user_id = Session::get('social_user_id');

        $adminUser = AdminUser::where('id', $user_id)->first();
        return view('admin_panel/user_profile', compact('adminUser'));
    }
    public function check_password(Request $request)
    {
        $old_password = $request->old_password;
        $user_id      = Session::get('social_user_id');
        $user         = AdminUser::find($user_id);
        if (Hash::check($old_password, $user->password)) {
            echo 1;
        } else {
            echo 0;
        }

    }
    public function update_password(Request $request)
    {
        $new_password   = $request->new_password;
        $user_id        = Session::get('social_user_id');
        $user           = AdminUser::find($user_id);
        $user->password = Hash::make($new_password);
        $user->save();
        return 1;
    }

}
