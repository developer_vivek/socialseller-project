<?php

namespace App\Http\Controllers;

use App\AppointmentVideo;
use Illuminate\Http\Request;

class AppointmentVideoController extends Controller
{
    public function index()
    {
        $appointment_videos = AppointmentVideo::get();
        return view('admin_panel/appointment_video/index', compact('appointment_videos'));
    }

    public function add_appointment_video(Request $request)
    {
        request()->validate([
            'url'   => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $data['url'] = ucfirst($request->url);
        $dir         = 'appointment_videos';
        if ($request->image) {
            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path() . '/storage/' . $dir, $imageName);
            $data['image'] = '/storage/' . $dir . '/' . $imageName;
        }

        AppointmentVideo::create($data);
        session()->flash('message', 'Appointment Video created succesfully');

        return redirect('admin/appointment_video');
    }
    public function edit_appointment_video(Request $request)
    {
        request()->validate([
            'url' => 'required',
        ]);

        $name = ucfirst($request->url);
        $id   = $request->id;
        AppointmentVideo::where('id', $id)->update(['url' => $name]);
        return 'Succesfully update';
    }
    public function delete_appointment_video(Request $request)
    {
        $id                     = $request->id;
        $appointment_videoExist = AppointmentVideo::where('id', $id)->first();
        if (!$appointment_videoExist) {
            return "Appointment video Not Exist";
        }
        $filename = $appointment_videoExist->getOriginal('image');

        if (file_exists(public_path() . $filename)) {
            \File::delete($appointment_videoExist->image);
        }
        AppointmentVideo::where('id', $id)->delete();
        return 1;
    }
    public function change_status(Request $request)
    {
        $status = $request->status;
        $id     = $request->id;
        AppointmentVideo::where('id', $id)->update(['status' => $status]);
        return 1;
    }
}
