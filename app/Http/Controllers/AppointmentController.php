<?php

namespace App\Http\Controllers;

use App\Appointment;

class AppointmentController extends Controller
{

    public function index()
    {
        $appointment = Appointment::orderBy('created_at', 'desc')->where('status', 'booked')->get();
        return view('admin_panel/appointment', compact('appointment'));
    }

    public function mark_completed(Request $request)
    {
        $appointment_id = $request->appointment_id;
        Appointment::where('id', $appointment_id)->update(['status' => 'completed']);
        return 1;
    }
}
