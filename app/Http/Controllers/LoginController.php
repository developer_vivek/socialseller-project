<?php

namespace App\Http\Controllers;

use App\Mail\NewUserRegistration;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mail;
use Session;

class LoginController extends Controller
{
    public function signup_process(Request $request)
    {
        request()->validate([
            'mobile'    => 'required|unique:users',
            'email'     => 'required|unique:users',
            'full_name' => 'required',
            'password'  => 'required',
        ]);

        $data['mobile']   = $request->mobile;
        $data['email']    = $request->email;
        $data['name']     = $request->full_name;
        $data['password'] = Hash::make($request->password);

        User::create($data);
        Mail::to($data['email'])->queue(new NewUserRegistration($data['name']));

        return redirect('login')->with('message', 'Successfully signup');

    }
    public function signin(Request $request)
    {
        request()->validate([
            'email'    => 'email|required',
            'password' => 'required',
        ]);
        // dd($request);
        $email    = $request->email;
        $password = $request->password;

        $user = User::where('email', $email)->first();
        if (!$user) {
            session()->flash('error', 'Invalid Credentials');
            return redirect('login')->withInput();

        }

        if (is_null($user->password)) {
            session()->flash('error', 'Password is not set for this email id');
            return redirect('login')->withInput();

        }
        if (Hash::check($password, $user->password)) {

            Session::put('student_id', $user->id);
            $path = Session::get('path');
            if (isset($path)) {
                Session::forget('path');
                return redirect($path);
            }

            return redirect('dashboard');
        } else {
            session()->flash('error', 'Invalid Credentials');
            return redirect('login')->withInput();

        }

    }
    public function forgot_password(Request $request)
    {
        $mobile    = $request->mobile;
        $userExist = User::where('mobile', $mobile)->first();
        if (!$userExist) {

        }
        $otp            = rand(1000, 9999);
        $message        = 'Your Social seller 4 digit otp is ' . $otp;
        $userExist->otp = $otp;
        $userExist->save();
        SmsController::send_message($message, $mobile);
        redirect('authenticate/reset_passowrd', compact('mobile'));
    }
    public function reset_password(Request $request)
    {
        $otp         = $request->otp;
        $password    = $request->password;
        $c_password  = $request->c_password;
        $mobile      = $request->mobile;
        $mobileExist = User::where('mobile', $mobile)->first();
        if (!$mobileExist) {
            return "Something went wrong try again";
        }
        if ($mobileExist->otp != $otp) {
            return "The otp you entered is not correct";

        }
        if ($password != $c_password) {
            return "New password and confirm password not matched";
        }
        $password              = Hash::make($password);
        $mobileExist->password = $password;
        $mobileExist->save();
        return 1;

    }
    public function logout()
    {
        Session::flush();
        Session::save();
        return redirect('login');
    }
}
