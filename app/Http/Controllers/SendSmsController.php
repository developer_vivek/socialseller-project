<?php

namespace App\Http\Controllers;

class SendSmsController extends Controller
{

    public static function send_message($mobile, $message)
    {

        $curl               = curl_init();
        $authentication_key = "275588AIHmHWVyjtu5cd18c59";
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://api.msg91.com/api/v2/sendsms",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => "{ \"sender\": \"GONEXT\", \"route\": \"4\", \"country\": \"91\", \"sms\": [ { \"message\": \"$message\", \"to\": [ \"$mobile\"] } ] }",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER     => array(
                "authkey: $authentication_key",
                "content-type: application/json",
            ),
        ));
        // echo '<pre>';
        // print_r($mobile);
        // print_r($curl);
        $response = curl_exec($curl);
        $err      = curl_error($curl);
        // print_r()
        curl_close($curl);
        return $response;
        // if ($err) {
        //     echo "cURL Error #:" . $err;
        // } else {
        //     echo $response;
        // }

    }

}
