<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function index()
    {
        $services = Service::where(['status' => 'active'])->get();
        return view('admin_panel/services/index', compact('services'));
    }
    public function add_service_view()
    {
        return view('admin_panel/services/add_service');

    }
    public function add_service(Request $request)
    {
        request()->validate([
            'name' => 'required',
        ]);

        $name = ucfirst($request->name);
        Service::create(['name' => $name]);
        session()->flash('message', 'service created succesfully');

        return redirect('admin/services');
    }
    public function edit_service(Request $request)
    {
        request()->validate([
            'name' => 'required',
        ]);

        $name = ucfirst($request->name);
        $id   = $request->id;
        Service::where('id', $id)->update(['name' => $name]);
        return 'Succefully update';
    }
    public function change_status(Request $request)
    {
        $status = $request->status;
        $id     = $request->id;
        Service::where('id', $id)->update(['status' => $status]);
        return 1;
    }
}
