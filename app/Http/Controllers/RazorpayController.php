<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Course;
use App\EnrollUser;
use App\Mail\AppointmentEmail;
use App\Mail\BuyCourse;
use App\Mail\ConsultBooking;
use App\PaymentDetail;
use App\User;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Mail;
use Razorpay\Api\Api;
use Session;
use Validator;

class RazorpayController extends Controller
{
    public function payWithRazorpay()
    {
        return view('admin_panel/captain/show');
    }

    public function create_order_id_razorpay(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [

                'amount'    => 'required',
                'course_id' => 'required',
            ]
        );
        if ($validate->fails()) {
            return response()->json(array('message' => $validate->errors()->first(), 'errors' => $validate->errors()), 422);
        }
        // $data['amount'] = $request->amount;
        $course_id    = $request->course_id;
        $courseDetail = Course::where('id', $course_id)->first();
        if (!$courseDetail) {
            return "Course Not Found";
        }
        $data['amount'] = $courseDetail->discount_price;
        $api_key        = 'rzp_live_15L1bNIExnbURu';
        $api_secret     = 'OOd4lTTRKXXTpLG8qeMBflbU';
        $api            = new Api($api_key, $api_secret);
        $faker          = Faker::create();
        $UUID           = $faker->uuid();

        if ($api) {
            $order = $api->order->create([
                'receipt'         => $UUID,
                'amount'          => $data['amount'] * 100,
                'payment_capture' => 1,
                'currency'        => 'INR',
            ]);

        }
        $student_id   = Session::get('student_id');
        $create_order = PaymentDetail::create([

            'razorpay_order_id' => $order['id'],
            'amount'            => ($data['amount']),
            // 'amount_paid'       =>  0,
            // 'amount_due'        =>  ($order['amount']), / by 100 /
            // 'currency'          =>  $order['currency'],

            'razorpay_reciept'  => $UUID,
            'razorpay_status'   => $order['status'],
            'razorpay_attempt'  => $order['attempts'],
            'created_at'        => $order['created_at'],
            // 'user_id'           => $data['user_id'],
            'user_id'           => $student_id,
            'course_id'         => $course_id,

        ]);

        $response = [
            'orderId'        => $order['id'],
            'razorpayId'     => $api_key,
            'amount'         => $data['amount'] * 100,
            'name'           => $request->name,
            'currency'       => 'INR',
            'email'          => $request->email,
            'contactNumber'  => $request->contactNumber,
            // 'address'       => $request->address,
            'description'    => 'Social seller',
            "course_id"      => $course_id,
            "student_id"     => $student_id,
            "appointment_id" => "",
            "supplier_id"    => "",

            "url"            => url('/payment_complete'),
        ];

        // Let's checkout payment page is it working
        return view('admin_panel/coupan/payment_page', compact('response'));

    }

    public function add_payment_request(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                "razorpay_signature"  => 'required',
                "razorpay_payment_id" => 'required',
                "razorpay_order_id"   => 'required',
                'amount'              => 'required',
            ]
        );
        if ($validate->fails()) {
            return response()->json(array('message' => $validate->errors()->first(), 'errors' => $validate->errors()), 422);
        }
        ##test
        // $api_key    = 'rzp_test_jQLluNm4V5xleO';
        // $api_secret = '2Umlbhg0VtuAn0qioeklOYQi';
        ##live
        $api_key    = 'rzp_live_15L1bNIExnbURu';
        $api_secret = 'OOd4lTTRKXXTpLG8qeMBflbU';

        $student_id = $request->student_id;
        $course_id  = $request->course_id;
        $api        = new Api($api_key, $api_secret);
        // $data['user_id'] = $request->user()->id;
        $data['amount'] = $request->input('amount');
        EnrollUser::create(['student_id' => $student_id, 'course_id' => $course_id]);
        $passbook   = PaymentDetail::where(['user_id' => $student_id, 'razorpay_order_id' => $request->razorpay_order_id])->first();
        $attributes = array(
            'razorpay_signature'  => $request->razorpay_signature,
            'razorpay_payment_id' => $request->razorpay_payment_id,
            'razorpay_order_id'   => $request->razorpay_order_id,
        );

        try {
            $order = $api->utility->verifyPaymentSignature($attributes);
            // dd($order);
        } catch (\Exception $e) {
            $passbook->razorpay_signature  = $request->razorpay_signature;
            $passbook->razorpay_payment_id = $request->razorpay_payment_id;
            $passbook->razorpay_attempt    = $request->razorpay_attempt + 1;
            $passbook->razorpay_status     = 'attempted';
            $passbook->save();

            return response()->json([

                'message' => 'Your Payment Request is failed',
            ], 422);
        }

        ##passbook update
        $passbook->razorpay_signature  = $request->razorpay_signature;
        $passbook->razorpay_payment_id = $request->razorpay_payment_id;
        // $passbook->razorpay_attempt    = $request->razorpay_attempt + 1;
        $passbook->razorpay_status = 'paid';
        // $passbook->amount          = ($data['amount'] / 100);
        $passbook->save();

        $userDetails = User::where('id', $student_id)->first();
        if ($userDetails) {
            $courseDetail = Course::select('title')->where('id', $course_id)->first();
            if ($courseDetail) {
                Mail::to($userDetails->email)->queue(new BuyCourse($userDetails->name, $courseDetail->title));
            }
        }

        session()->flash('message', 'Successfully Purchase');
        return redirect('success_payment');

        // return redirect('dashboard');

    }
    public function payment_details($num = null)
    {
        if ($num == null) {
            abort('404');
        }
        if (Session::get('social_role_id') != 1) {
            abort('403', 'You have not permission to access this');
        }
        ##for course paid
        if ($num == 1) {
            $payment_details = PaymentDetail::selectRaw('payment_details.*,courses.title as course_name,users.name as user_name')
                ->where(['razorpay_status' => 'paid'])
                ->where('course_id', '!=', null)
            // ->orWhere(['razorpay_status' => 'created'])
                ->join('users', 'payment_details.user_id', '=', 'users.id')
                ->leftjoin('courses', 'payment_details.course_id', '=', 'courses.id')
                ->orderBy('payment_details.created_at', 'desc')
                ->get();
        }
        ##for supplier paid
        if ($num == 2) {
            $payment_details = PaymentDetail::selectRaw('payment_details.*,suppliers.name as course_name,users.name as user_name')
                ->where(['razorpay_status' => 'paid'])
                ->where('supplier_id', '!=', null)
            // ->orWhere(['razorpay_status' => 'created'])
                ->join('users', 'payment_details.user_id', '=', 'users.id')
                ->leftjoin('suppliers', 'payment_details.supplier_id', '=', 'suppliers.id')
                ->orderBy('payment_details.created_at', 'desc')
                ->get();
        }
        ##for appointment paid
        if ($num == 3) {
            $payment_details = PaymentDetail::selectRaw('payment_details.*,courses.title as course_name,users.name as user_name')
                ->where(['razorpay_status' => 'paid'])
                ->where('appointment_id', '!=', null)
            // ->orWhere(['razorpay_status' => 'created'])
                ->join('users', 'payment_details.user_id', '=', 'users.id')
                ->leftjoin('courses', 'payment_details.course_id', '=', 'courses.id')
                ->orderBy('payment_details.created_at', 'desc')
                ->get();
        }
        ## for all
        if ($num == 0) {
            $payment_details = PaymentDetail::selectRaw('payment_details.*,courses.title as course_name,users.name as user_name')
                ->where(['razorpay_status' => 'paid'])
            // ->orWhere(['razorpay_status' => 'created'])
                ->join('users', 'payment_details.user_id', '=', 'users.id')
                ->leftjoin('courses', 'payment_details.course_id', '=', 'courses.id')
                ->orderBy('payment_details.created_at', 'desc')
                ->get();

        }
        return view('admin_panel/payment_details', compact('payment_details'));

    }
    public function generate_appointment(Request $request)
    {
        request()->validate([
            'mobile'           => 'required',
            'name'             => 'required',
            'appointment_time' => 'required',

        ]);

        $student_id = session::get('student_id');
        if (!$student_id) {
            return redirect()->back()->withErrors(['message' => 'please login first for booking appointment'])->withInput();

            // return false;
        }
        $name             = $request->name;
        $mobile           = $request->mobile;
        $appointment_time = $request->appointment_time;
        // echo ($appointment_time);
        // echo '<br>';
        $appointment_time = date('Y-m-d h:i:s', strtotime($appointment_time));
        // dd($r);
        if (strtotime(date('Y-m-d h:i:s')) > strtotime($appointment_time)) {
            session()->flash('error', 'You can not book Appointment in past time');
            return redirect()->back()->withErrors(['message' => 'You can not book Appointment in past time'])->withInput();

        }
        // echo $appointment_time;
        // die;
        $data['amount'] = 2000;
        $booked         = Appointment::create(['user_id' => $student_id, 'mobile' => $mobile, 'appointment_time' => $appointment_time, 'name' => $name]);
        // $api_key        = 'rzp_test_jQLluNm4V5xleO';
        // $api_secret     = '2Umlbhg0VtuAn0qioeklOYQi';

        ##live
        // $api_key    = 'rzp_live_15L1bNIExnbURu';
        // $api_secret = 'OOd4lTTRKXXTpLG8qeMBflbU';
        $student_id = Session::get('student_id');

        if ($student_id == 97) {
            ##test
            $api_key    = 'rzp_test_jQLluNm4V5xleO';
            $api_secret = '2Umlbhg0VtuAn0qioeklOYQi';
        } else {
            ##live
            $api_key    = 'rzp_live_15L1bNIExnbURu';
            $api_secret = 'OOd4lTTRKXXTpLG8qeMBflbU';

        }

        $api   = new Api($api_key, $api_secret);
        $faker = Faker::create();
        $UUID  = $faker->uuid();

        if ($api) {
            $order = $api->order->create([
                'receipt'         => $UUID,
                'amount'          => $data['amount'] * 100,
                'payment_capture' => 1,
                'currency'        => 'INR',
            ]);

        }

        $create_order = PaymentDetail::create([

            'razorpay_order_id' => $order['id'],
            'amount'            => ($data['amount']),
            'razorpay_reciept'  => $UUID,
            'razorpay_status'   => $order['status'],
            'razorpay_attempt'  => $order['attempts'],
            'created_at'        => $order['created_at'],
            'user_id'           => $student_id,
            'appointment_id'    => $booked->id,

            'type'              => 'appointment',

        ]);

        $response = [
            'orderId'        => $order['id'],
            'razorpayId'     => $api_key,
            'amount'         => $data['amount'] * 100,
            'name'           => $request->name,
            'currency'       => 'INR',
            'email'          => $request->email,
            'contactNumber'  => $request->contactNumber,
            'description'    => 'Social seller',
            "course_id"      => '',
            "student_id"     => $student_id,
            "appointment_id" => $booked->id,
            "supplier_id"    => "",
            "url"            => url('/add_payment_request_for_appointment'),
        ];

// Let's checkout payment page is it working
        return view('admin_panel/coupan/payment_page', compact('response'));

    }
    public function add_payment_request_for_appointment(Request $request)
    {

        request()->validate(
            [
                "razorpay_signature"  => 'required',
                "razorpay_payment_id" => 'required',
                "razorpay_order_id"   => 'required',
                'amount'              => 'required',
                'appointment_id'      => 'required',
            ]
        );

        // ##test
        // $api_key    = 'rzp_test_jQLluNm4V5xleO';
        // $api_secret = '2Umlbhg0VtuAn0qioeklOYQi';
        ##live
        // $api_key    = 'rzp_live_15L1bNIExnbURu';
        // $api_secret = 'OOd4lTTRKXXTpLG8qeMBflbU';
        $student_id = Session::get('student_id');

        if ($student_id == 97) {
            ##test
            $api_key    = 'rzp_test_jQLluNm4V5xleO';
            $api_secret = '2Umlbhg0VtuAn0qioeklOYQi';
        } else {
            ##live
            $api_key    = 'rzp_live_15L1bNIExnbURu';
            $api_secret = 'OOd4lTTRKXXTpLG8qeMBflbU';

        }

        $appointment_id = $request->appointment_id;
        // $course_id  = $request->course_id;
        $api = new Api($api_key, $api_secret);
        // $data['user_id'] = $request->user()->id;
        $data['amount'] = $request->input('amount');
        // EnrollUser::create(['student_id' => $student_id, 'course_id' => $course_id]);
        $passbook   = PaymentDetail::where(['razorpay_order_id' => $request->razorpay_order_id])->first();
        $attributes = array(
            'razorpay_signature'  => $request->razorpay_signature,
            'razorpay_payment_id' => $request->razorpay_payment_id,
            'razorpay_order_id'   => $request->razorpay_order_id,
        );

        try {
            $order = $api->utility->verifyPaymentSignature($attributes);
            // dd($order);
        } catch (\Exception $e) {
            $passbook->razorpay_signature  = $request->razorpay_signature;
            $passbook->razorpay_payment_id = $request->razorpay_payment_id;
            $passbook->razorpay_attempt    = $request->razorpay_attempt + 1;
            $passbook->razorpay_status     = 'attempted';
            $passbook->save();

            return response()->json([

                'message' => 'Your Payment Request is failed',
            ], 422);
        }

        ##passbook update
        $passbook->razorpay_signature  = $request->razorpay_signature;
        $passbook->razorpay_payment_id = $request->razorpay_payment_id;
        // $passbook->razorpay_attempt    = $request->razorpay_attempt + 1;
        $passbook->razorpay_status = 'paid';
        // $passbook->amount          = ($data['amount'] / 100);
        $passbook->save();
        Appointment::where('id', $appointment_id)->update(['status' => 'booked']);
        $appointmentDetail = Appointment::where('id', $appointment_id)->first();
        $name              = $appointmentDetail->name;
        $mobile            = $appointmentDetail->mobile;
        $time              = $appointmentDetail->appointment_time;
        $time              = date(' h:i A | l | d-m-Y', strtotime($time));
        // die;
        // $time = "12 nov 2019";
        // Mail::to("vivek.et1993@gmail.com")->queue(new AppointmentEmail($name, $mobile, $time));
        Mail::to("socialselleracademy@gmail.com")->queue(new AppointmentEmail($name, $mobile, $time));

        $userDetails = User::where('id', $student_id)->first();
        if ($userDetails) {
            Mail::to($userDetails->email)->queue(new ConsultBooking($userDetails->name));

        }

        session()->flash('message', 'Your Appointment is booked Succesfully');

        return redirect('appointment');
    }

    public function success_page($message = '')
    {
        $message = $message;
        // $message=Your ;
        return view("website.success_payment", compact('message'));
    }

}
