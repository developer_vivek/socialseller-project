<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\UnblockSupplier;
use App\PaymentDetail;
use App\Service;
use App\Supplier;
use App\SupplierAssign;
use App\SupplierCategory;
use App\SupplierGroup;
use App\SupplierService;
use App\User;
use DB;
use Faker\Factory as Faker;
use Illuminate\Http\Request;
use Mail;
use Razorpay\Api\Api;
use Session;
use Validator;

class SupplierController extends Controller
{
    public function index()
    {
        // $otp     = 1234;
        // $message = 'hiii';
        // $mobile  = "9148725074";
        // SendSmsController::Send_message($mobile, $message);
        // die;
        // // $message = rawurlencode($message);
        // echo $message;
        // SendSmsController::Send_message($mobile, $message);
        // print_r(SendSmsController::send_message("9148725074", $message));

        $suppliers = Supplier::get();
        // $servi     = array();
        // foreach ($suppliers as $row) {
        //     $services = explode(',', $row->services);
        //     print_r($services);
        //     foreach ($services as $key => $value) {
        //         // echo $value;
        //         $servi[] = Service::select('name')->where('id', $value)->first();

        //     }
        //     $row->services = $servi;

        // }
        // // dd($survicess);
        // dd($suppliers);
        return view('admin_panel/suppliers/index', compact('suppliers'));
    }
    public function change_status_supplier(Request $request)
    {
        $supplier_id = $request->supplier_id;
        $status      = $request->status;
        Supplier::where('id', $supplier_id)->update(['status' => $status]);
        return response()->json(["message" => "Sucessfully Supplier status changed", "status" => 1]);

    }
    public function add_supplier()
    {
        $services   = Service::where('status', 'active')->get();
        $categories = SupplierCategory::where('status', 'active')->get();
        return view('admin_panel/suppliers/add', compact('services', 'categories'));

    }
    public function store_supplier(Request $request)
    {
        request()->validate([
            'name'          => 'required',
            // 'mobile'   => 'required',
            // 'email'    => 'email',
            'image'         => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'services'      => 'required|array|min:1',
            'amount'        => 'required',
            'supplier_name' => 'required|array|min:1',
            'mobile'        => 'required|array|min:1',
        ]);
        DB::beginTransaction();

        // echo '<pre>';
        // print_r($request->input());
        // die;
        // $mul_images = $request->mul_images;
        // dd($mul_images[0]);
        // $length = count($request->mul_images);

        $dir = 'suppliers';
        // $length = count($request->mul_images);

        if ($request->image) {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path() . '/storage/' . $dir, $imageName);
            $data['image'] = '/storage/' . $dir . '/' . $imageName;
        }

        $data['name']     = $request->name;
        $data['video_id'] = $request->video_id;
        // $data['email']              = $request->email;
        // $data['mobile']             = $request->mobile;
        $data['category'] = $request->category;
        $data['is_free']  = $request->is_free ? $request->is_free : 0;
        // $data['is_lifetime_access'] = $request->is_lifetime_access ? $request->is_lifetime_access : 0;
        $data['amount'] = $request->amount ? $request->amount : 0;

        // $data['validity_in_days'] = $request->validity_in_days;
        $supplierId           = Supplier::create($data);
        $supplier_name        = $request->supplier_name;
        $mobile               = $request->mobile;
        $supplier_group_count = count($request->supplier_name);
        for ($j = 0; $j < $supplier_group_count; $j++) {
            SupplierGroup::create(['supplier_id' => $supplierId->id, 'name' => $supplier_name[$j], 'mobile' => $mobile[$j]]);

        }

        // for ($i = 0; $i < $length; $i++) {

        //     $imageName = time() . $i . '.' . $mul_images[$i]->getClientOriginalExtension();

        //     $mul_images[$i]->move(public_path() . '/storage/' . $dir, $imageName);
        //     $image = '/storage/' . $dir . '/' . $imageName;

        //     SupplierImage::create(['image' => $image, 'supplier_id' => $supplierId->id]);

        // }
        foreach ($request->services as $row) {
            SupplierService::create(['service_id' => $row, 'supplier_id' => $supplierId->id]);
        }

        DB::commit();
        session("message", "successfully suppliers created");
        return redirect('admin/suppliers');
    }

    public function edit_supplier(Request $request, $supplier_id)
    {
        $suppliers         = Supplier::where('id', $supplier_id)->first();
        $services          = Service::get();
        $supplier_services = SupplierService::where('supplier_id', $supplier_id)->pluck('service_id')->toArray();

        $supplier_person = SupplierGroup::where('supplier_id', $supplier_id)->get();
        // dd($supplier_services);
        $categories = SupplierCategory::get();

        return view('admin_panel/suppliers/edit', compact('suppliers', 'services', 'categories', 'supplier_services', 'supplier_person'));
    }
    public function update_supplier(Request $request)
    {
        DB::beginTransaction();
        $supplier_id = $request->supplier_id;
        $suppliers   = Supplier::where('id', $supplier_id)->first();
        if (!$suppliers) {
            return "Supplier Not Found";
        }
        $data['name']     = $request->name;
        $data['amount']   = $request->amount;
        $data['video_id'] = $request->video_id;

        // $data['email']    = $request->email;
        // $data['mobile']   = $request->mobile;
        $data['category'] = $request->category;
        $dir              = 'suppliers';

        if ($request->image) {
            DB::commit();
            $imageName = time() . '.' . request()->image->getClientOriginalExtension();
            request()->image->move(public_path() . '/storage/' . $dir, $imageName);
            $data['image'] = '/storage/' . $dir . '/' . $imageName;
        }

        Supplier::where('id', $supplier_id)->update($data);
        SupplierService::where(['supplier_id' => $supplier_id])->delete();

        foreach ($request->services as $row) {
            SupplierService::create(['service_id' => $row, 'supplier_id' => $supplier_id]);
        }

        $supplier_group_count = count($request->supplier_groups_id);
        ##old update
        // dd($request->supplier_groups_id);
        for ($i = 0; $i < $supplier_group_count; $i++) {
            SupplierGroup::where(['id' => $request->supplier_groups_id[$i], 'supplier_id' => $supplier_id])->update(['name' => $request->supplier_name[$i], 'mobile' => $request->mobile[$i]]);
        }
        ## new add if
        if ($request->supplier_name_s) {
            $supplier_group_count_new = count($request->supplier_name_s);
            for ($j = 0; $j < $supplier_group_count_new; $j++) {
                SupplierGroup::create(['supplier_id' => $supplier_id, 'name' => $request->supplier_name_s[$j], 'mobile' => $request->mobile_s[$j]]);

            }
        }

        DB::commit();
        return redirect('admin/suppliers');
    }

    public function buy_suppliers_details(Request $request)
    {
        ##top 5 supplier details buy
        ##if he pays 300 rupee through razorpay then
        ##first check what he buy previously then not supply him reapeat detail
        $validate = Validator::make(
            $request->all(),
            [
                "razorpay_signature"  => 'required',
                "razorpay_payment_id" => 'required',
                "razorpay_order_id"   => 'required',
                'amount'              => 'required',
                'supplier_id'         => 'required',
            ]
        );
        if ($validate->fails()) {
            return response()->json(array('message' => $validate->errors()->first(), 'errors' => $validate->errors()), 422);
        }
        // ##test
        // $api_key     = 'rzp_test_jQLluNm4V5xleO';
        // $api_secret  = '2Umlbhg0VtuAn0qioeklOYQi';
        ##live
        // $api_key    = 'rzp_live_15L1bNIExnbURu';
        // $api_secret = 'OOd4lTTRKXXTpLG8qeMBflbU';
        $student_id = $request->student_id;
        if ($student_id == 97) {
            ##test
            $api_key    = 'rzp_test_jQLluNm4V5xleO';
            $api_secret = '2Umlbhg0VtuAn0qioeklOYQi';
        } else {
            ##live
            $api_key    = 'rzp_live_15L1bNIExnbURu';
            $api_secret = 'OOd4lTTRKXXTpLG8qeMBflbU';

        }

        $supplier_id = $request->supplier_id;
        $api         = new Api($api_key, $api_secret);
        // $data['user_id'] = $request->user()->id;
        $data['amount'] = $request->input('amount');
        // EnrollUser::create(['student_id' => $student_id, 'course_id' => $course_id]);
        $passbook   = PaymentDetail::where(['user_id' => $student_id, 'supplier_id' => $supplier_id, 'razorpay_order_id' => $request->razorpay_order_id])->first();
        $attributes = array(
            'razorpay_signature'  => $request->razorpay_signature,
            'razorpay_payment_id' => $request->razorpay_payment_id,
            'razorpay_order_id'   => $request->razorpay_order_id,
        );

        try {
            $order = $api->utility->verifyPaymentSignature($attributes);
            // dd($order);
        } catch (\Exception $e) {
            $passbook->razorpay_signature  = $request->razorpay_signature;
            $passbook->razorpay_payment_id = $request->razorpay_payment_id;
            $passbook->razorpay_attempt    = $request->razorpay_attempt + 1;
            $passbook->razorpay_status     = 'attempted';
            $passbook->save();

            return response()->json([

                'message' => 'Your Payment Request is failed',
            ], 422);
        }

##passbook update
        $passbook->razorpay_signature  = $request->razorpay_signature;
        $passbook->razorpay_payment_id = $request->razorpay_payment_id;
// $passbook->razorpay_attempt    = $request->razorpay_attempt + 1;
        $passbook->razorpay_status = 'paid';
        // $passbook->amount          = ($data['amount'] / 100);
        $passbook->save();

        $student_id = Session::get('student_id');
        // $student_id     = 1;
        $SupplierAssign = SupplierAssign::create(['user_id' => $student_id, 'supplier_id' => $supplier_id]);
        $userDetails    = User::where('id', $student_id)->first();
        if ($userDetails) {
            Mail::to($userDetails->email)->queue(new UnblockSupplier($userDetails->name));

        }

        return redirect('success_payment');

    }
    public function buy_suppliers_details_request_generate(Request $request, $supplier_id)
    {

        if (!isset($supplier_id)) {

            return back()->with('error', 'Invalid Supplier');

        }
        $supplierDetails = Supplier::where('id', $supplier_id)->first();
        if (!$supplierDetails) {

            return back()->with('error', 'Supllier not found');

        }
        $student_id = Session::get('student_id');
        if (!isset($student_id)) {

            return back()->with('error', 'Please login first before purchase');

        }

        $supplierAssignCheck = SupplierAssign::where(['user_id' => $student_id, 'supplier_id' => $supplier_id])->first();
        if ($supplierAssignCheck) {
            // session()->flash('error', 'You have already purchased this supplier');
            // return redirect()->back();
            return back()->with('error', 'You have already purchased this supplier');

        }

        $data['amount'] = $supplierDetails->amount;

        if ($student_id == 4) {
            ##test
            $api_key    = 'rzp_test_jQLluNm4V5xleO';
            $api_secret = '2Umlbhg0VtuAn0qioeklOYQi';
        } else {
            ##live
            $api_key    = 'rzp_live_15L1bNIExnbURu';
            $api_secret = 'OOd4lTTRKXXTpLG8qeMBflbU';

        }

        $api   = new Api($api_key, $api_secret);
        $faker = Faker::create();
        $UUID  = $faker->uuid();

        if ($api) {
            $order = $api->order->create([
                'receipt'         => $UUID,
                'amount'          => $data['amount'] * 100,
                'payment_capture' => 1,
                'currency'        => 'INR',
            ]);

        }
        if (!isset($student_id)) {
            return "500";

        }
        $create_order = PaymentDetail::create([

            'razorpay_order_id' => $order['id'],
            'amount'            => ($data['amount']),
            'razorpay_reciept'  => $UUID,
            'razorpay_status'   => $order['status'],
            'razorpay_attempt'  => $order['attempts'],
            'created_at'        => $order['created_at'],
            // 'user_id'           => $data['user_id'],
            'user_id'           => $student_id,
            'supplier_id'       => $supplier_id,
            'type'              => 'supplier',

        ]);

        $response = [
            'orderId'        => $order['id'],
            'razorpayId'     => $api_key,
            'amount'         => $request->amount * 100,
            'name'           => $request->name,
            'currency'       => 'INR',
            'email'          => $request->email,
            'contactNumber'  => $request->contactNumber,
            // 'address'       => $request->address,
            'description'    => 'Social seller',
            "course_id"      => '',
            "student_id"     => $student_id,
            "appointment_id" => "",
            "supplier_id"    => $supplier_id,

            "url"            => url('/buy_suppliers_details'),
        ];

// Let's checkout payment page is it working
        return view('admin_panel/coupan/payment_page', compact('response'));

    }

    public function free_supplier_suscribe(Request $request)
    {
        $student_id = Session::get('student_id');
        if (!$student_id) {
            return "please login for get supplier information";
        }
        $supplier_id = $request->id;
        $isExist     = SupplierAssign::where(['user_id' => $student_id, 'supplier_id' => $supplier_id])->first();
        if ($isExist) {
            return "You already subscribe this supplier";

        } else {

            SupplierAssign::create(['user_id' => $student_id, 'supplier_id' => $supplier_id]);
            return 1;
        }

    }
    public function delete_supplier_group_member(Request $request)
    {
        $supplier_id       = $request->supplier_id;
        $supplier_group_id = $request->supplier_group_id;
        $exist             = SupplierGroup::where(['id' => $supplier_group_id, 'supplier_id' => $supplier_id])->first();
        if (!$exist) {
            return "Supplier Information not exist";
        }
        SupplierGroup::where(['id' => $supplier_group_id, 'supplier_id' => $supplier_id])->delete();
        return 1;

    }
}
