<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

use App\Appointment;
// use Mail;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/testDemo', function () {
    // $name        = "vivek";
    // $mobile      = "7828161459";
    // $time        = "12.22";
    // $course_name = "mind power";
    $appointmentDetail = Appointment::where('id', 120)->first();
    $name              = $appointmentDetail->name;
    $mobile            = $appointmentDetail->mobile;
    $time              = $appointmentDetail->appointment_time;
    $time              = date(' h:i A | l | d-m-Y', strtotime($time));
// die;
    // $time = "12 nov 2019";
    // Mail::to("vivek.et1993@gmail.com")->queue(new AppointmentEmail($name, $mobile, $time));
    // Mail::to("socialselleracademy@gmail.com")->queue(new AppointmentEmail($name, $mobile, $time));

    // Mail::to("vivek.et1993@gmail.com")->queue(new AppointmentEmail($name, $mobile, $time));
    // Mail::to("socialselleracademy@gmail.com")->queue(new BuyCourse($name, $course_name));
    // Mail::to("socialselleracademy@gmail.com")->queue(new UnblockSupplier($name));
    // Mail::to("socialselleracademy@gmail.com")->queue(new ConsultBooking($name));
    // Mail::to('vivek.et1993@gmail.com')->send(new NewUserRegistration("vivek"));
    echo "success";

});
Route::get('artisan/clear-all', function () {
    \Artisan::call('config:cache');
});

Route::get('/payment-initiate', function () {
    return view('admin_panel/coupan/create');
});
Route::post('/payment-initiate-request', 'RazorpayController@create_order_id_razorpay');
Route::post('/payment_complete', 'RazorpayController@add_payment_request');

Route::get('admin/login', 'AdminLoginController@admin_login');
Route::post('admin/login', 'AdminLoginController@login_auth');
Route::get('admin/forgot_password', 'AdminLoginController@forgot_password');
Route::post('admin/forgot_password', 'AdminLoginController@send_mail_to_id');
Route::get('admin/reset_password/{token}', 'AdminLoginController@reset_password_page')->name('reset_password_page');
Route::post('admin/submit_reset_password', 'AdminLoginController@submit_reset_password');
Route::get('admin/user_profile', 'AdminController@user_profile');
Route::post('admin/update_password', 'AdminController@update_password');
Route::post('admin/check_password', 'AdminController@check_password');

Route::get('admin/logout', 'LoginController@logout');

Route::prefix('admin')->group(function () {
    Route::get('dashboard', 'AdminHomeController@dashboard');
    Route::get('courses', 'CourseController@index');
    Route::post('add_course', 'CourseController@store_course');
    Route::get('add_course', 'CourseController@add_course');
    Route::post('update_course', 'CourseController@update_course');
    Route::post('change_status_course', 'CourseController@change_status_course');
    Route::get('course/edit/{course_id}', 'CourseController@edit_course');
    Route::get('lessons/{id}', 'CourseController@view_lesson');
    Route::post('modify_lesson', 'CourseController@add_lesson');
    Route::get('registered_users', 'AdminUserController@registered_users');
    Route::get('enrolled_users', 'AdminUserController@enrolled_users');
    Route::get('user_details/{user_id}', 'AdminUserController@user_details');
    Route::post('assign_course_to_student', 'AdminUserController@assign_course_to_student');

    Route::get('supplier_categories', 'CategoryController@supplier_category_list');
    Route::post('add_supplier_category', 'CategoryController@add_supplier_category');
    Route::post('edit_supplier_category', 'CategoryController@edit_supplier_category');

    Route::get('categories', 'CategoryController@index');
    Route::post('add_category', 'CategoryController@add_category');
    Route::post('edit_category', 'CategoryController@edit_category');
    Route::get('services', 'ServiceController@index');

    Route::post('add_service', 'ServiceController@add_service');
    Route::post('edit_service', 'ServiceController@edit_service');
    Route::get('blogs', 'BlogController@index');

    Route::post('add_blog', 'BlogController@add_blog');
    Route::post('edit_blog', 'BlogController@edit_blog');
    Route::post('delete_blog', 'BlogController@delete_blog');

    Route::get('appointment_video', 'AppointmentVideoController@index');

    Route::post('add_appointment_video', 'AppointmentVideoController@add_appointment_video');
    Route::post('edit_appointment_video', 'AppointmentVideoController@edit_appointment_video');
    Route::post('delete_appointment_video', 'AppointmentVideoController@delete_appointment_video');
    Route::get('suppliers', 'SupplierController@index');
    Route::get('edit_supplier/{id}', 'SupplierController@edit_supplier');
    Route::post('update_supplier', 'SupplierController@update_supplier');
    Route::post('delete_supplier_group_member', 'SupplierController@delete_supplier_group_member');
    Route::get('buy_suppliers_details', 'SupplierController@buy_suppliers_details');
    Route::post('add_supplier', 'SupplierController@store_supplier');
    Route::post('edit_supplier', 'SupplierController@edit_supplier');
    Route::get('add_supplier', 'SupplierController@add_supplier');
    Route::post('change_status_supplier', 'SupplierController@change_status_supplier');

    Route::get('payment_details/{num}', 'RazorpayController@payment_details');
    Route::get('settings', 'CountController@settings');
    Route::post('settings', 'CountController@update_settings');
    Route::get('appointments', 'AppointmentController@index');

    #course for
    Route::get('who_can_learn', 'AdminHomeController@who_can_learn_list');
    Route::get('who_can_learn/create', 'AdminHomeController@who_can_learn_create');
    Route::post('who_can_learn/create', 'AdminHomeController@who_can_learn_store');
    Route::get('seller_feedbacks', 'SellerFeedbackController@index');
    Route::post('add_seller_feedbacks', 'SellerFeedbackController@store_seller_feedback');
    Route::post('seller_feedback_delete', 'SellerFeedbackController@seller_feedback_delete');
    Route::get('whatsapp_record', 'AdminHomeController@whatsapp_mobile_list');
    Route::post('export_csv', 'AdminHomeController@export_csv');
    Route::get('export_csv_emails', 'AdminHomeController@export_csv_emails');

    Route::get('logout', 'AdminLoginController@logout');

});
Route::get('buy_suppliers_details_request_generate/{supplier_id}', 'SupplierController@buy_suppliers_details_request_generate');
Route::post('buy_suppliers_details', 'SupplierController@buy_suppliers_details');
Route::post('free_supplier_subscribe', 'SupplierController@free_supplier_suscribe');
Route::post('add_whatsapp_mobile', 'AdminHomeController@add_whatsapp_mobile');

Route::get('auth/google', 'SocialController@redirectToGoogle');
Route::get('auth/google/callback', 'SocialController@handleGoogleCallback');
Route::get('paywithrazorpay', 'RazorpayController@payWithRazorpay')->name('paywithrazorpay');
Route::get('success_payment', 'RazorpayController@success_page');
Route::get('auth/facebook', 'SocialController@redirectToFacebook');
Route::get('auth/facebook/callback', 'SocialController@handleFacebookCallback');

// Post Route For Makw Payment Request
// Route::post('payment', 'RazorpayController@payment')->name('payment');
// Route::post('payment-initiate-request', 'RazorpayController@payment')->name('payment');

###website routes

Route::get('login', 'WebsiteController@login');
Route::post('login', 'LoginController@signin');
// Route::get('login/{path}/{id}', 'WebsiteController@login');
Route::get('feedback', 'WebsiteController@feedback');
Route::get('privacy_policy', 'WebsiteController@privacy_policy');
Route::get('terms_condition', 'WebsiteController@terms_condition');
Route::get('data_deletion_url', 'WebsiteController@data_deletion_url');
Route::get('logout', 'LoginController@logout');
Route::get('my_courses', 'UserController@my_courses');
Route::get('/', 'WebsiteController@home');
Route::get('login_with_password', 'WebsiteController@login_with_password');
Route::get('signup', 'WebsiteController@signup');
Route::post('signup', 'LoginController@signup_process');

Route::get('dashboard', 'UserController@dashboard')->middleware('checkLogin');
Route::get('supplier_detail/{supplier_id}', 'UserController@supplier_detail');
Route::get('suppliers', 'UserController@my_suppliers');
Route::get('forgot_password', 'WebsiteController@forgot_password');
Route::any('reset_password', 'WebsiteController@reset_password');
Route::post('set_new_password', 'WebsiteController@set_new_password');
Route::get('all_suppliers', 'WebsiteController@all_suppliers');
Route::get('profile', 'UserController@profile')->middleware('checkLogin');
Route::post('profile_update', 'UserController@profile_update')->middleware('checkLogin');
Route::get('download_certificate/{course_id}', 'CourseController@certificate_download')->middleware('checkLogin');
Route::get('course_details/{id}', 'WebsiteController@course_details');
Route::get('course_enroll', 'WebsiteController@course_enroll');
Route::get('user_course_details/{id}', 'WebsiteController@user_course_details')->middleware('checkLogin');
Route::get('free_courses', 'WebsiteController@free_courses');
Route::get('free_videos', 'WebsiteController@free_videos');
Route::get('courses', 'WebsiteController@courses');
Route::get('appointment', 'WebsiteController@appointment');
Route::post('appointment', 'RazorpayController@generate_appointment');
Route::post('add_payment_request_for_appointment', 'RazorpayController@add_payment_request_for_appointment');
