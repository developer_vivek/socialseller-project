<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
     */

    'mailgun'  => [
        'domain'   => env('MAILGUN_DOMAIN'),
        'secret'   => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses'      => [
        'key'    => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'google'   => [
        'client_id'     => '931302111053-hc0h4l7r5sbb7f8hegg8jb6nhh9v5oms.apps.googleusercontent.com',
        'client_secret' => '3kqhbXAotCHPXQ8vimBZHKTi',
        'redirect'      => 'https://www.socialseller.in/auth/google/callback',
    ],
    'facebook' => [
        'client_id'     => '348772699841741',
        'client_secret' => '4d10553528332bfc25f1e90e9b95503a',
        'redirect'      => 'https://www.socialseller.in/auth/facebook/callback',
    ],

];
